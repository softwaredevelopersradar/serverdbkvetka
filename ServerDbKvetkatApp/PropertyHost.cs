﻿using System.Runtime.Serialization;

namespace ServerDbKvetkaApp
{
    [DataContract]
    public class PropertyHost
    {
        [DataMember]
        public string IpAddress { get; set; }

        [DataMember]
        public int PortHttp { get; set; }

        public PropertyHost(string ipAddress, int portHttp)
        {
            IpAddress = ipAddress;
            PortHttp = portHttp;
        }
    }
}
