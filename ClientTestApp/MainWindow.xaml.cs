﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using KvetkaModelsDBLib;
using InheritorsEventArgs;
using GrpcClientLib;
using System.Threading;
using System.Collections.ObjectModel;

namespace ClientTestApp
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ServiceClient serviceClient;
        string ip = "127.0.0.1";
        int port = 30051;


        public MainWindow()
        {
            InitializeComponent();
            this.Name = "TestClient";
        }


        public void DispatchIfNecessary(Action action)
        {
            if (!Dispatcher.CheckAccess())
                Dispatcher.Invoke(action);
            else
                action.Invoke();
        }

        private void InitClientDB()
        {
            serviceClient.OnConnect += ClientDB_OnConnect;
            serviceClient.OnDisconnect += HandlerDisconnect;
            serviceClient.OnUpData += HandlerUpData;
            (serviceClient.Tables[NameTable.TableJammer] as ITableUpdate<TableJammer>).OnUpTable += MainWindow_OnUpTable;
            (serviceClient.Tables[NameTable.TempSuppressFF] as ITableUpdate<TempSuppressFF>).OnUpTable += MainWindow_OnUpTable1;
            (serviceClient.Tables[NameTable.TableAnalogResFF] as ITableUpdate<TableAnalogResFF>).OnUpTable += MainWindow_OnUpTable3;
            (serviceClient.Tables[NameTable.TableResFF] as ITableUpdate<TableResFF>).OnUpTable += MainWindow_OnUpTable2; ;
            (serviceClient.Tables[NameTable.TableFHSSExcludedJam] as ITableUpdate<TableFHSSExcludedJam>).OnUpTable += MainWindow_OnUpTable4;
            (serviceClient.Tables[NameTable.TableResFHSSJam] as ITableUpdate<TableResFHSSJam>).OnUpTable += MainWindow_OnUpTable5;
            //clientDB.OnErrorDataBase += HandlerErrorDataBase;
            //(clientDB.Tables[NameTable.TableSource] as ITableUpdate<TableSource>).OnUpTable += TSource_OnUpTable;
            //(clientDB.Tables[NameTable.TableFreqKnown] as ITableUpdate<TableFreqKnown>).OnUpTable += MainWindow_OnUpTable; ;
            //(clientDB.Tables[NameTable.TableSource] as ITableUpRecord<TableSource>).OnChangeRecord += MainWindow_OnChangeRecord;
            //(clientDB.Tables[NameTable.TablePattern] as ITableUpdate<TablePattern>).OnUpTable += TablePattern_OnUpTable;
            //(clientDB.Tables[NameTable.TableOwnUAV] as ITableUpdate<TableOwnUAV>).OnUpTable += MainWindow_OnUpTable1; ;
        }

        private void MainWindow_OnUpTable5(object sender, TableEventArgs<TableResFHSSJam> e)
        {
            var y = 7;
        }

        private void MainWindow_OnUpTable4(object sender, TableEventArgs<TableFHSSExcludedJam> e)
        {
            var t = 9;
        }

        private void MainWindow_OnUpTable3(object sender, TableEventArgs<TableAnalogResFF> e)
        {
            foreach (var rec in e.Table)
            {
                DispatchIfNecessary(() =>
                {
                
                    tbMessage.AppendText($"ResAnalog {rec.Id}, bearing = {rec.BearingOwn} \n");
                });
            }

                DispatchIfNecessary(() =>
                {

                    tbMessage.AppendText($"ResAnalog finished. \n");
                });
        }

        private void MainWindow_OnUpTable2(object sender, TableEventArgs<TableResFF> e)
        {
            foreach (var rec in e.Table)
            {
                DispatchIfNecessary(() =>
                {

                    tbMessage.AppendText($"ResFF {rec.Id}, bearing = {rec.BearingOwn} \n");
                });
            }

            DispatchIfNecessary(() =>
            {

                tbMessage.AppendText($"ResFF finished. \n");
            });
        }

        private void MainWindow_OnUpTable1(object sender, TableEventArgs<TempSuppressFF> e)
        {
            var t = 6;
        }

        private void MainWindow_OnUpTable(object sender, TableEventArgs<TableJammer> e)
        {
            var t = 5;
        }

        private void btnCon_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (serviceClient != null)
                    serviceClient.Disconnect();
                else
                {
                    serviceClient = new ServiceClient(this.Name, ip, port);
                    InitClientDB();
                    serviceClient.Connect();
                }
            }
            catch (ExceptionClient exceptClient)
            {
                tbMessage.Foreground = System.Windows.Media.Brushes.Red;
                tbMessage.AppendText(exceptClient.Message);
            }
        }

        private async void ClientDB_OnConnect(object sender, ClientEventArgs e)
        {
            DispatchIfNecessary(() =>
            {
                btnCon.Content = "Disconnect";
            });
           

            foreach (NameTable table in Enum.GetValues(typeof(NameTable)))
            {
                var tempTable = await serviceClient.Tables[table].LoadAsync<AbstractCommonTable>();
                DispatchIfNecessary(() =>
                {
                    tbMessage.AppendText($"Load data from Db. {table.ToString()} count records - {tempTable.Count} \n");
                });
            }
        }

        void HandlerDisconnect(object obj, ClientEventArgs eventArgs)
        {
            DispatchIfNecessary(() =>
            {
                btnCon.Content = "Connect";
            });
           
            if (eventArgs.GetMessage != "")
            {
                tbMessage.Foreground = System.Windows.Media.Brushes.Red;
                tbMessage.AppendText(eventArgs.GetMessage);
            }
            serviceClient = null;
        }

        void HandlerUpData(object obj, DataEventArgs eventArgs)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                tbMessage.Foreground = System.Windows.Media.Brushes.Black;

                tbMessage.AppendText($"Load data from Db. Tables {eventArgs.Name.ToString()} count records - {eventArgs.AbstractData.ListRecords.Count} \n");
            });
        }

        private void ButDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int nufOfRec = Convert.ToInt32(RecordID.Text);
                object record = null;
                switch (Tables.SelectedItem)
                {

                    case NameTable.TableJammer:
                        record = new TableJammer
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.TableFreqForbidden:
                        record = new TableFreqForbidden
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.TableFreqKnown:
                        record = new TableFreqKnown
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.TableADSB:
                        record = new TableADSB
                        {
                            ICAO = nufOfRec.ToString(),
                        };
                        break;
                    case NameTable.TableResFF:
                        record = new TableResFF
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.TableResFFJam:
                        record = new TableResFFJam
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.TableResFHSS:
                        record = new TableResFHSS
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.TableResFHSSJam:
                        record = new TableResFHSSJam
                        {
                            Id = nufOfRec,
                            NumberASP = 1
                        };
                        break;
                    case NameTable.TempSuppressFF:
                        record = new TempSuppressFF
                        {
                            Id = nufOfRec,
                            NumberASP = 1
                        };
                        break;
                    case NameTable.TempSuppressFHSS:
                        record = new TempSuppressFHSS
                        {
                            Id = nufOfRec,
                            NumberASP = 1
                        };
                        break;
                    case NameTable.TableAnalogResFF:
                        record = new TableAnalogResFF
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.TableDigitalResFF:
                        record = new TableDigitalResFF
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.TableFHSSExcludedJam:
                        record = new TableFHSSExcludedJam
                        {
                            Id = nufOfRec
                        };
                        break;
                    default:
                        break;
                }
                if (record != null)
                    serviceClient?.Tables[(NameTable)Tables.SelectedItem].Delete(record);
            }
            catch (GrpcClientLib.ExceptionClient exceptClient)
            {
                tbMessage.Foreground = System.Windows.Media.Brushes.Red;
                tbMessage.AppendText(exceptClient.Message);
            }
        }

        private void ButClear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                serviceClient?.Tables[(NameTable)Tables.SelectedItem].Clear();
            }
            catch (GrpcClientLib.ExceptionClient exceptClient)
            {
                Foreground = System.Windows.Media.Brushes.Red;
                tbMessage.AppendText(exceptClient.Message);
            }
        }

        private void ButAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int nufOfRec = Convert.ToInt32(RecordID.Text);
                Random rand = new Random();
                object record = null;
                switch (Tables.SelectedItem)
                {

                    case NameTable.TableJammer:
                        record = new TableJammer
                        {
                            Id = nufOfRec,
                            Coordinates = new Coord
                            {
                                Altitude = (float)rand.NextDouble() * rand.Next(0, 360),
                                Latitude = -53.56d,
                                Longitude = -26.365d
                            },
                            CallSign = "topol",
                            Note = "hello local",
                            Role = StationRole.Linked,
                            IsOwn = true
                        };
                        break;
                    case NameTable.TableFreqForbidden:
                        record = new TableFreqForbidden
                        {
                            FreqMaxKHz = rand.Next(150000, 250000),
                            FreqMinKHz = rand.Next(30000, 50000),
                            NumberASP = nufOfRec,
                        };
                        break;
                    case NameTable.TableFreqKnown:
                        record = new TableFreqKnown
                        {
                            FreqMaxKHz = rand.Next(150000, 250000),
                            FreqMinKHz = rand.Next(30000, 50000),
                            NumberASP = nufOfRec
                        };
                        break;
                    case NameTable.TableADSB:
                        record = new TableADSB
                        {
                            ICAO = rand.Next().ToString(),
                            Time = DateTime.Now,
                        };
                        break;
                    case NameTable.TableResFF:
                        record = new TableResFF
                        {
                            FrequencyKHz = rand.Next(),
                            Time = DateTime.Now,
                            //ListTrack = new System.Collections.ObjectModel.ObservableCollection<TableTrackResFF>() { new TableTrackResFF() { BearingOwn = rand.Next() }, new TableTrackResFF() { BearingOwn = rand.Next() } }
                        };
                        break;
                    case NameTable.TableResFFJam:
                        record = new TableResFFJam
                        {
                            NumberASP = nufOfRec,
                            FrequencyKHz = rand.Next(),
                            Threshold = (short)rand.Next(0, 255)
                        };
                        break;
                    case NameTable.TableResFHSS:
                        record = new TableResFHSS
                        {
                            FrequencyMinKHz = rand.Next(),
                            ListSubscribers = new ObservableCollection<TableSubscriberResFHSS>() { 
                                new TableSubscriberResFHSS() { Time = DateTime.Now, Position = new TableTrackSubscriberResFHSS() { BearingOwn = rand.Next() } },
                                new TableSubscriberResFHSS() { Time = DateTime.Now, Position = new TableTrackSubscriberResFHSS() { BearingOwn = rand.Next() } } 
                            } 
                        };
                        break;
                    case NameTable.TableResFHSSJam:
                        record = new TableResFHSSJam
                        {
                            NumberASP = nufOfRec,
                            FrequencyMinKHz = rand.Next(),
                            Threshold = (short)rand.Next(0, 255),
                            Letters = new byte[3] { (byte)rand.Next(0,10), 5,6 }
                        };
                        break;
                    case NameTable.GlobalProperties:
                        record = new GlobalProperties
                        {
                            TimeRadiationFF = rand.Next(), FhssFftResolution = FftResolution.N4096
                        };
                        break;
                    case NameTable.TempSuppressFF:
                        record = new TempSuppressFF
                        {
                            Id = rand.Next(),
                            Control = Led.Blue,
                            Radiation = Led.Green,
                            NumberASP = nufOfRec,
                        };
                        break;
                    case NameTable.TableAnalogResFF:
                        var freq1 = rand.Next();
                        record = new TableAnalogResFF
                        {
                            Time = DateTime.Now,
                            FrequencyKHz = freq1, TypeSignal = "",
                            ResFF = new TableResFF() { FrequencyKHz = freq1, BearingOwn = rand.Next(), SignalFlag = SignalFlag.Analog , Time = DateTime.Now }
                            //ListTrack = new System.Collections.ObjectModel.ObservableCollection<TableTrackResFF>() { new TableTrackResFF() { BearingOwn = rand.Next() }, new TableTrackResFF() { BearingOwn = rand.Next() } }
                        };
                        break;
                    case NameTable.TableDigitalResFF:
                        var freq = rand.Next();
                        record = new TableDigitalResFF
                        {
                            TimeBegin = DateTime.Now,
                            // TimeEnd = DateTime.Now.AddSeconds(3),
                            FrequencyKHz = freq,
                            //ResFF = new TableResFF() { FrequencyKHz = freq, BearingOwn = rand.Next(), SignalFlag = SignalFlag.Digital }
                            //ListTrack = new System.Collections.ObjectModel.ObservableCollection<TableTrackResFF>() { new TableTrackResFF() { BearingOwn = rand.Next() }, new TableTrackResFF() { BearingOwn = rand.Next() } }
                        };
                        break;
                    case NameTable.TableResFFDistribution:
                        record = new TableResFFDistribution
                        {
                            FrequencyKHz = rand.Next(),
                            Time = DateTime.Now,
                            //ListTrack = new System.Collections.ObjectModel.ObservableCollection<TableTrackResFF>() { new TableTrackResFF() { BearingOwn = rand.Next() }, new TableTrackResFF() { BearingOwn = rand.Next() } }
                        };
                        break;
                    case NameTable.TableRoute:
                        record = new TableRoute
                        {
                            NumberRoute = (short)rand.Next(0,30000),
                            Time = DateTime.Now,
                             ListPoints = new ObservableCollection<TableRoutePoint>() { new TableRoutePoint() { NumberPoint = (short)rand.Next(0, 30000) }   }
                            //ListTrack = new System.Collections.ObjectModel.ObservableCollection<TableTrackResFF>() { new TableTrackResFF() { BearingOwn = rand.Next() }, new TableTrackResFF() { BearingOwn = rand.Next() } }
                        };
                        break;
                    case NameTable.TableFHSSExcludedJam:
                        record = new TableFHSSExcludedJam
                        {
                            FrequencyKHz = rand.Next(0, 30000),
                            Deviation = rand.Next(0, 100),
                            FhssId = nufOfRec
                        };
                        break;
                    default:
                        break;
                }
                if (record != null)
                    serviceClient?.Tables[(NameTable)Tables.SelectedItem].Add(record);
            }
            catch (GrpcClientLib.ExceptionClient exceptClient)
            {
                tbMessage.Foreground = System.Windows.Media.Brushes.Red;
                tbMessage.AppendText(exceptClient.Message);
            }
        }

        private async void ButLoad_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                tbMessage.Foreground = System.Windows.Media.Brushes.Black;
                int nufOfRec = Convert.ToInt32(RecordID.Text);

                List<AbstractCommonTable> table = new List<AbstractCommonTable>();
                NameTable nameTable = (NameTable)Tables.SelectedItem;

                //if (serviceClient.Tables[nameTable] is IDependentAsp)
                //{
                //    //dynamic tablDepenAsp = await (serviceClient.Tables[NameTable.TableFreqKnown] as IDependentAsp).LoadByFilterAsync<FreqRanges>(nufOfRec);
                //    //DispatchIfNecessary(() =>
                //    //{
                //    //    tbMessage.AppendText($"Load data from Db. {((NameTable)Tables.SelectedItem).ToString()} count records - {tablDepenAsp.Count} \n");
                //    //});
                //}
                //else
                //{
                    //table = await serviceClient?.Tables[nameTable].LoadAsync<AbstractCommonTable>();
                    table = serviceClient?.Tables[nameTable].Load<AbstractCommonTable>();
                    DispatchIfNecessary(() =>
                    {
                        tbMessage.AppendText($"Load data from Db. {((NameTable)Tables.SelectedItem).ToString()} count records - {table.Count} \n");
                    });
                //}


            }
            catch (ExceptionClient exeptClient)
            {
                tbMessage.Foreground = System.Windows.Media.Brushes.Red;
                tbMessage.AppendText(exeptClient.Message);
            }
            catch (ExceptionDatabase excpetService)
            {
                tbMessage.Foreground = System.Windows.Media.Brushes.Red;
                tbMessage.AppendText(excpetService.Message);
            }
        }

        private void butChange_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int nufOfRec = Convert.ToInt32(RecordID.Text);
                Random rand = new Random();
                object record = null;
                switch (Tables.SelectedItem)
                {

                    case NameTable.TableJammer:
                        record = new TableJammer
                        {
                            Id = nufOfRec,
                            Coordinates = new Coord
                            {
                                Altitude = (float)rand.NextDouble() * rand.Next(0, 360),
                                Latitude = rand.Next(0, 360),
                                Longitude = rand.Next(0, 360)
                            },
                            CallSign = "berezka",
                            Note = "bye local",
                            Role = StationRole.Complex,
                            IsOwn = true
                        };
                        break;
                    case NameTable.TableFreqForbidden:
                        record = new TableFreqForbidden
                        {
                            FreqMaxKHz = rand.Next(150000, 250000),
                            FreqMinKHz = rand.Next(30000, 50000),
                            NumberASP = nufOfRec
                        };
                        break;
                    case NameTable.TableFreqKnown:
                        record = new TableFreqKnown
                        {
                            FreqMaxKHz = rand.Next(150000, 250000),
                            FreqMinKHz = rand.Next(30000, 50000),
                            NumberASP = nufOfRec
                        };
                        break;
                    case NameTable.TableADSB:
                        record = new TableADSB
                        {
                            ICAO = nufOfRec.ToString(),
                            Time = DateTime.Now
                        };
                        break;
                    case NameTable.TableResFF:
                        record = new TableResFF
                        {
                            Id = nufOfRec,
                            FrequencyKHz = rand.Next(),
                            Time = DateTime.Now,
                            //ListTrack = new System.Collections.ObjectModel.ObservableCollection<TableTrackResFF>() { new TableTrackResFF() { BearingOwn = rand.Next() }, new TableTrackResFF() { BearingOwn = rand.Next() } }
                        };
                        break;
                    case NameTable.TableResFFJam:
                        record = new TableResFFJam
                        {
                            Id = nufOfRec, NumberASP = 1,
                            FrequencyKHz = rand.Next(),
                            Threshold = (short)rand.Next(0, 255)
                        };
                        break;
                    case NameTable.TableAnalogResFF:
                        record = new TableAnalogResFF
                        {
                            Id = nufOfRec,
                            FrequencyKHz = rand.Next(), TypeSignal = "",
                            BandKHz = rand.Next(),
                            Time = DateTime.Now,
                            ResFF = new TableResFF() { Id = nufOfRec, TableAnalogResFFId = nufOfRec, BearingOwn = rand.Next(), SignalFlag = SignalFlag.Analog, Time = DateTime.Now, }
                            //ListTrack = new System.Collections.ObjectModel.ObservableCollection<TableTrackResFF>() { new TableTrackResFF() { BearingOwn = rand.Next() }, new TableTrackResFF() { BearingOwn = rand.Next() } }
                        };
                        break;
                    case NameTable.TableDigitalResFF:
                        record = new TableDigitalResFF
                        {
                            Id = nufOfRec,
                            FrequencyKHz = rand.Next(),
                            TimeBegin = DateTime.Now,
                            TimeEnd = DateTime.Now,
                            ResFF = new TableResFF() { BearingOwn = rand.Next(), SignalFlag = SignalFlag.Digital, Time = DateTime.Now, }
                            //ResFF = new TableResFF() { FrequencyKHz = freq, BearingOwn = rand.Next(), SignalFlag = SignalFlag.Digital }
                            //ListTrack = new System.Collections.ObjectModel.ObservableCollection<TableTrackResFF>() { new TableTrackResFF() { BearingOwn = rand.Next() }, new TableTrackResFF() { BearingOwn = rand.Next() } }
                        };
                        break;
                    case NameTable.TableRoute:
                        var table = serviceClient?.Tables[NameTable.TableRoute].Load<TableRoute>();
                        var r = table.Last(); r.Name = rand.Next().ToString();
                        record = r;
                        
                        
                        //record = new TableRoute
                        //{
                        //    Id = nufOfRec,
                        //    NumberRoute = (short)rand.Next(0, 30000),
                        //    Time = DateTime.Now,
                        //    ListPoints = new ObservableCollection<TableRoutePoint>() { new TableRoutePoint() { NumberPoint = (short)rand.Next(0, 30000) } }
                        //    //ListTrack = new System.Collections.ObjectModel.ObservableCollection<TableTrackResFF>() { new TableTrackResFF() { BearingOwn = rand.Next() }, new TableTrackResFF() { BearingOwn = rand.Next() } }
                        //};
                        break;
                    default:
                        break;
                }
                if (record != null)
                    serviceClient?.Tables[(NameTable)Tables.SelectedItem].Change(record);
            }
            catch (GrpcClientLib.ExceptionClient exceptClient)
            {
                tbMessage.Foreground = System.Windows.Media.Brushes.Red;
                tbMessage.AppendText(exceptClient.Message);
            }
        }

        private void ButRemove_Click(object sender, RoutedEventArgs e)
        {
            int nufOfRec = Convert.ToInt32(RecordID.Text);
            dynamic listForSend = new List<AbstractCommonTable>();

            switch (Tables.SelectedItem)
            {

                case NameTable.TableJammer:
                    listForSend = new List<TableJammer>();
                    for (int i = nufOfRec; i < nufOfRec + 3; i++)
                    {
                        listForSend.Add(new TableJammer
                        {
                            Id = i
                        });
                    };
                    break;
                case NameTable.TableFreqForbidden:
                    listForSend = new List<TableFreqForbidden>();
                    for (int i = nufOfRec; i < nufOfRec + 3; i++)
                    {
                        listForSend.Add(new TableFreqForbidden
                        {
                            Id = i
                        });
                    };
                    break;

                default:
                    break;
            }

            serviceClient?.Tables[(NameTable)Tables.SelectedItem].RemoveRange(listForSend);
        }

        private void SendRange_Click(object sender, RoutedEventArgs e)
        {
            Random rand = new Random();
            int nufOfRec = Convert.ToInt32(RecordID.Text);
            dynamic listForSend = new List<AbstractCommonTable>();

            switch (Tables.SelectedItem)
            {

                case NameTable.TableJammer:
                    listForSend = new List<TableJammer>();
                    for (int i = nufOfRec; i < nufOfRec + 3; i++)
                    {
                        listForSend.Add(new TableJammer
                        {
                            Coordinates = new Coord
                            {
                                Altitude = (float)rand.NextDouble() * rand.Next(0, 360),
                                Latitude = rand.NextDouble() * rand.Next(0, 360),
                                Longitude = rand.NextDouble() * rand.Next(0, 360)
                            }
                        });
                    };
                    break;
                case NameTable.TableFreqForbidden:
                    listForSend = new List<TableFreqForbidden>();
                    for (int i = nufOfRec; i < nufOfRec + 3; i++)
                    {
                        listForSend.Add(new TableFreqForbidden
                        {
                            FreqMaxKHz = rand.Next(150000, 250000),
                            FreqMinKHz = rand.Next(30000, 50000)
                        });
                    };
                    break;
                case NameTable.TableADSB:
                    listForSend = new List<TableADSB>();
                    for (int i = nufOfRec; i < nufOfRec + 3; i++)
                    {
                        listForSend.Add(new TableADSB
                        {
                            ICAO = rand.Next().ToString(),
                            Time = DateTime.Now
                        });
                    };
                    break;
                case NameTable.TableResFF:
                    listForSend = new List<TableResFF>();
                    for (int i = nufOfRec; i < nufOfRec + 3; i++)
                    {
                        listForSend.Add(new TableResFF
                        {
                            FrequencyKHz = rand.Next(),
                            Time = DateTime.Now,
                            //ListTrack = new System.Collections.ObjectModel.ObservableCollection<TableTrackResFF>() { new TableTrackResFF() { BearingOwn = rand.Next() }, new TableTrackResFF() { BearingOwn = rand.Next() } }
                        });
                    };
                    break;
                case NameTable.TableAnalogResFF:
                    listForSend = new List<TableAnalogResFF>();
                    for (int i = nufOfRec; i < nufOfRec + 2; i++)
                    {
                        var freq1 = rand.Next();
                        listForSend.Add(new TableAnalogResFF
                                            {
                                                FrequencyKHz = freq1,
                                                TypeSignal = "",
                                                ResFF = new TableResFF()
                                                            {
                                                                FrequencyKHz = freq1,
                                                                BearingOwn = rand.Next(),
                                                                SignalFlag = SignalFlag.Analog,
                                                                Time = DateTime.Now,
                                                },
                                                Time = DateTime.Now,
                            //ListTrack = new System.Collections.ObjectModel.ObservableCollection<TableTrackResFF>() { new TableTrackResFF() { BearingOwn = rand.Next() }, new TableTrackResFF() { BearingOwn = rand.Next() } }
                        });
                    };
                    break;
                default:
                    break;
            }

            serviceClient?.Tables[(NameTable)Tables.SelectedItem].AddRange(listForSend);
        }
        
        
        private void butChangeRange_Click(object sender, RoutedEventArgs e)
        {
            Random rand = new Random();
            int nufOfRec = Convert.ToInt32(RecordID.Text);
            dynamic listForSend = new List<AbstractCommonTable>();

            switch (Tables.SelectedItem)
            {

                case NameTable.TableJammer:
                    listForSend = new List<TableJammer>();
                    for (int i = nufOfRec; i < nufOfRec + 3; i++)
                    {
                        listForSend.Add(new TableJammer
                        {
                            Id = i,
                            Coordinates = new Coord
                            {
                                Altitude = (float)rand.NextDouble() * rand.Next(0, 360),
                                Latitude = rand.NextDouble() * rand.Next(0, 360),
                                Longitude = rand.NextDouble() * rand.Next(0, 360)
                            }
                        });
                    };
                    break;
                case NameTable.TableFreqForbidden:
                    listForSend = new List<TableFreqForbidden>();
                    for (int i = nufOfRec; i < nufOfRec + 3; i++)
                    {
                        listForSend.Add(new TableFreqForbidden
                        {
                            Id = i,
                            FreqMaxKHz = rand.Next(150000, 250000),
                            FreqMinKHz = rand.Next(30000, 50000)
                        });
                    };
                    break;
                case NameTable.TableADSB:
                    listForSend = new List<TableADSB>();
                    for (int i = nufOfRec; i < nufOfRec + 3; i++)
                    {
                        listForSend.Add(new TableADSB
                        {
                            ICAO = i.ToString(),
                            Time = DateTime.Now
                        });
                    };
                    break;
                case NameTable.TableResFF:
                    listForSend = new List<TableResFF>();
                    for (int i = nufOfRec; i < nufOfRec + 3; i++)
                    {
                        listForSend.Add(new TableResFF
                        {
                            Id = i,
                            FrequencyKHz = rand.Next(),
                            Time = DateTime.Now,
                            //ListTrack = new System.Collections.ObjectModel.ObservableCollection<TableTrackResFF>() { new TableTrackResFF() { BearingOwn = rand.Next() }, new TableTrackResFF() { BearingOwn = rand.Next() } }
                        });
                    };
                    break;
                case NameTable.TableAnalogResFF:
                    listForSend = new List<TableAnalogResFF>();
                    for (int i = nufOfRec; i < nufOfRec + 2; i++)
                    {
                        var freq1 = rand.Next();
                        listForSend.Add(new TableAnalogResFF
                                            {
                                                Id = i,
                                                FrequencyKHz = freq1,
                                                TypeSignal = "",
                                                Time = DateTime.Now,
                            //ResFF = new TableResFF()
                            //            {
                            //                FrequencyKHz = freq1,
                            //                BearingOwn = rand.Next(),
                            //                SignalFlag = SignalFlag.Analog
                            //            }
                            //ListTrack = new System.Collections.ObjectModel.ObservableCollection<TableTrackResFF>() { new TableTrackResFF() { BearingOwn = rand.Next() }, new TableTrackResFF() { BearingOwn = rand.Next() } }
                        });
                    };
                    break;
                default:
                    break;
            }

            serviceClient?.Tables[(NameTable)Tables.SelectedItem].ChangeRange(listForSend);
        }

        private void butTest_Click(object sender, RoutedEventArgs e)
        {

        }


        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            serviceClient?.Dispose();
            //System.Windows.Threading.Dispatcher.ExitAllFrames();
        }
    }
}
