﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Grpc.Core;
using TransmissionPackage;
using KvetkaModelsDBLib;
using OperationTablesLib;
using InheritorsEventArgs;
using ProtoLibrary;
using System.Threading;
using System.Linq;

namespace ServerDbKvetkaLib
{
    public partial class ProtoServer : Transmission.TransmissionBase
    {
        public override Task<DefaultResponse> ClearTable(NameTableMessage request, ServerCallContext context)
        {
            var nameTable = TypesConverter.ConvertToDBModel(request.Table);
            //var IdClient = GetClientId(context);
            var IdClient = 1;

            lock (dicOperTables[nameTable])
            {
                try
                {
                    SendMessToHost(IdClient, NameTableOperation.Clear, nameTable.ToString());
                    //if (!_clients.ContainsKey(IdClient))
                    //{
                    //    SendErrorOfClient(Errors.EnumServerError.ClientAbsent, "");
                    //    return Task.FromResult(new DefaultResponse() { IsSucceeded = false });
                    //}

                    Dictionary<NameTable, int> keyValuePairs = new Dictionary<NameTable, int>();

                    foreach (var table in dicOperTables)
                    {
                        if (table.Key == nameTable)
                            continue;
                        keyValuePairs.Add(table.Key, table.Value.Load(IdClient).ListRecords.Count);
                    }

                    dicOperTables[nameTable].Clear(IdClient);

                    foreach (var table in keyValuePairs)
                    {
                        if (dicOperTables[table.Key].Load(IdClient).ListRecords.Count != table.Value)
                        {
                            //Task.Delay(100);
                            dicOperTables[table.Key].UpDate(IdClient);
                        }
                    }
                    SendMessToHost(IdClient, NameTableOperation.Clear, "Ok");
                    return Task.FromResult(new DefaultResponse() { IsSucceeded = true });
                }
                catch (InheritorsException.ExceptionLocalDB except)
                {
                    SendError(except, NameTableOperation.Clear);
                    throw new RpcException(new Status(StatusCode.Aborted, except.Message));
                    //return Task.FromResult(new DefaultResponse() { IsSucceeded = false });
                    //SendError(except, NameTableOperation.Clear);
                }
                catch (Exception error)
                {
                    SendErrorOfClient(Errors.EnumServerError.UnknownError, error.Message);
                    throw new RpcException(new Status(StatusCode.Aborted, error.Message));
                    //return Task.FromResult(new DefaultResponse() { IsSucceeded = false });
                }
            }

        }

        public override Task<LoadMessage> LoadTable(NameTableMessage request, ServerCallContext context)
        {
            var nameTable = TypesConverter.ConvertToDBModel(request.Table);
            var result = new LoadMessage() { Table = request.Table };

            //var IdClient = GetClientId(context);
            var IdClient = 1;
            try
            {
                SendMessToHost(IdClient, NameTableOperation.Load, nameTable.ToString());
                //if (!_clients.ContainsKey(IdClient))
                //{
                //    SendErrorOfClient(Errors.EnumServerError.ClientAbsent, "");
                //    return Task.FromResult(result); //отправлять ошибку
                //}

                ClassDataCommon data = dicOperTables[nameTable].Load(IdClient);
                result.Records.AddRange(TypesConverter.ConvertToProto(data, nameTable)); //проверить

                return Task.FromResult(result);
            }
            catch (InheritorsException.ExceptionLocalDB except)
            {
                SendError(except, NameTableOperation.Load);
                throw new RpcException(new Status(StatusCode.Aborted, except.Message));
               // return Task.FromResult(result); //отправлять ошибку

                //SendErrorToHost(new OperationTableEventArgs(NameTableOperation.Load, except.Error, except.Message));

                //throw new FaultException<InheritorsException.ExceptionWCF>(new InheritorsException.ExceptionWCF(except));
            }
            catch (Exception error)
            {
                throw new RpcException(new Status(StatusCode.Aborted, error.Message));
               // return Task.FromResult(result); //отправлять ошибку

                //SendErrorToHost(new OperationTableEventArgs(NameTableOperation.Load, error.Message));

                //throw new FaultException<InheritorsException.ExceptionWCF>(new InheritorsException.ExceptionWCF(error.Message));
            }

        }

        

        public override Task<DefaultResponse> ChangeRecord(ChangeRecordMessage request, ServerCallContext context)
        {
            var nameTable = TypesConverter.ConvertToDBModel(request.Table);
            var nameOperation = request.Operation.ConvertToDBModel();
            var record = request.Record.ConvertToDBModel(nameTable);
            NameTableOperation operation = NameTableOperation.None;
            //var IdClient = GetClientId(context);
            var IdClient = 1;
            lock (dicOperTables[nameTable])
            {
                try
                {
                    if (nameTable == NameTable.TempSuppressFF || nameTable == NameTable.TempSuppressFHSS)
                    {
                        SendMessToHost(IdClient, nameOperation, nameTable.ToString() + ", Id=" + (record as TempSuppress).Id + ", Control=" + (record as TempSuppress).Control + ", Suppress=" + (record as TempSuppress).Suppress + ", Radiation=" + (record as TempSuppress).Radiation + ", AspId=" + (record as TempSuppress).NumberASP);
                    }
                    else
                    { SendMessToHost(IdClient, nameOperation, nameTable.ToString()); }

                    //if (!_clients.ContainsKey(IdClient))
                    //{
                    //    SendErrorOfClient(Errors.EnumServerError.ClientAbsent, "");
                    //    return Task.FromResult(new DefaultResponse() { IsSucceeded = false });
                    //}

                    switch (nameOperation)
                    {
                        case NameChangeOperation.Add: 
                            
                            int countFF = 0;
                            if (nameTable == NameTable.TableAnalogResFF || nameTable == NameTable.TableDigitalResFF)
                            {
                                countFF = dicOperTables[NameTable.TableResFF].Load(IdClient).ListRecords.Count;
                            }

                            operation = NameTableOperation.Add;
                            dicOperTables[nameTable].Add(record, IdClient);

                            CheckResFF(nameTable, IdClient, countFF);
                            //if (nameTable == NameTable.TableAnalogResFF || nameTable == NameTable.TableDigitalResFF)
                            //{
                            //    Task.Delay(50);
                            //    if (dicOperTables[NameTable.TableResFF].Load(IdClient).ListRecords.Count != countFF)
                            //        dicOperTables[NameTable.TableResFF].UpDate(IdClient);
                            //}
                            break;
                        case NameChangeOperation.Change:
                            operation = NameTableOperation.Change;
                            dicOperTables[nameTable].Change(record, IdClient);
                            if (nameTable == NameTable.TableAnalogResFF || nameTable == NameTable.TableDigitalResFF)
                            {
                                //Task.Delay(100);
                                dicOperTables[NameTable.TableResFF].UpDate(IdClient);
                            }
                            break;
                        case NameChangeOperation.Delete:
                            operation = NameTableOperation.Delete;
                            Action actionDelete = () => dicOperTables[nameTable].Delete(record, IdClient);
                            if (dicOperTables[nameTable].IsTemp)
                            {
                                actionDelete();
                                break;
                            }
                            //хранит кол-во записей
                            Dictionary<NameTable, int> keyValuePairs = new Dictionary<NameTable, int>();

                            foreach (var table in dicOperTables)
                            {
                                if (table.Key == nameTable)
                                    continue;
                                keyValuePairs.Add(table.Key, table.Value.Load(IdClient).ListRecords.Count);
                            }

                            actionDelete();

                            foreach (var table in keyValuePairs)
                            {
                                if (dicOperTables[table.Key].Load(IdClient).ListRecords.Count != table.Value)
                                {
                                    //Task.Delay(100);
                                    dicOperTables[table.Key].UpDate(IdClient);
                                }
                            }

                            break;
                    }

                    Task.Run(() => SendMessToHost(IdClient, nameOperation, "Ok"));
                    return Task.FromResult(new DefaultResponse() { IsSucceeded = true });
                }
                catch (InheritorsException.ExceptionLocalDB except)
                {
                    SendError(except, operation);
                    throw new RpcException(new Status(StatusCode.Aborted, except.Message));
                    //return Task.FromResult(new DefaultResponse() { IsSucceeded = false });
                }
                catch (Exception error)
                {
                    SendErrorOfClient(Errors.EnumServerError.UnknownError, error.Message);
                    throw new RpcException(new Status(StatusCode.Aborted, error.Message));
                    //return Task.FromResult(new DefaultResponse() { IsSucceeded = false });
                }
            }

        }


        public override Task<DefaultResponse> ChangeRange(ChangeRangeMessage request, ServerCallContext context)
        {
            var nameTable = TypesConverter.ConvertToDBModel(request.Table);
            var nameOperation = request.Operation.ConvertToDBModel();
            var records = request.Records.ConvertToDBModel(nameTable);
            NameTableOperation operation = NameTableOperation.None;
            //var IdClient = GetClientId(context);
            var IdClient = 1;
            lock (dicOperTables[nameTable])
            {
                try
                {
                    if (nameTable == NameTable.TempSuppressFF || nameTable == NameTable.TempSuppressFHSS)
                    {
                        foreach (var record in records.ListRecords)
                        {
                            SendMessToHost(IdClient, nameOperation, nameTable.ToString() + ", Id=" + (record as TempSuppress).Id + ", Control=" + (record as TempSuppress).Control + ", Suppress=" + (record as TempSuppress).Suppress + ", Radiation=" + (record as TempSuppress).Radiation + ", AspId=" + (record as TempSuppress).NumberASP);
                        }
                    }
                    else
                    { SendMessToHost(IdClient, nameOperation, nameTable.ToString()); }

                    //if (!_clients.ContainsKey(IdClient))
                    //{
                    //    SendErrorOfClient(Errors.EnumServerError.ClientAbsent, "");
                    //    return Task.FromResult(new DefaultResponse() { IsSucceeded = false });
                    //}

                    switch (nameOperation)
                    {
                        case NameChangeOperation.Add:
                            int countFF = 0;
                            if (nameTable == NameTable.TableAnalogResFF || nameTable == NameTable.TableDigitalResFF)
                            {
                                countFF = dicOperTables[NameTable.TableResFF].Load(IdClient).ListRecords.Count;
                            }

                            operation = NameTableOperation.AddRange;
                            dicOperTables[nameTable].AddRange(records, IdClient);

                            CheckResFF(nameTable, IdClient, countFF);
                            //if (nameTable == NameTable.TableAnalogResFF || nameTable == NameTable.TableDigitalResFF)
                            //{
                            //    if (dicOperTables[NameTable.TableResFF].Load(IdClient).ListRecords.Count != countFF)
                            //        dicOperTables[NameTable.TableResFF].UpDate(IdClient);
                            //}
                            break;
                        case NameChangeOperation.Change:
                            operation = NameTableOperation.ChangeRange;
                            dicOperTables[nameTable].ChangeRange(records, IdClient);
                            if (nameTable == NameTable.TableAnalogResFF || nameTable == NameTable.TableDigitalResFF)
                            {
                                //Task.Delay(100);
                                dicOperTables[NameTable.TableResFF].UpDate(IdClient);
                            }
                            break;
                        case NameChangeOperation.Delete:
                            operation = NameTableOperation.RemoveRange;
                            //хранит кол-во записей
                            Dictionary<NameTable, int> keyValuePairs = new Dictionary<NameTable, int>();

                            foreach (var table in dicOperTables)
                            {
                                if (table.Key == nameTable)
                                    continue;
                                keyValuePairs.Add(table.Key, table.Value.Load(IdClient).ListRecords.Count);
                            }

                            dicOperTables[nameTable].RemoveRange(records, IdClient);

                            foreach (var table in keyValuePairs)
                            {
                                
                                if (dicOperTables[table.Key].Load(IdClient).ListRecords.Count != table.Value)
                                {
                                    //Task.Delay(100);
                                    dicOperTables[table.Key].UpDate(IdClient);
                                }
                            }
                            break;
                    }

                    Task.Run(() => SendMessToHost(IdClient, nameOperation, "Ok"));
                    return Task.FromResult(new DefaultResponse() { IsSucceeded = true });
                }
                catch (InheritorsException.ExceptionLocalDB except)
                {
                    SendError(except, operation);
                    throw new RpcException(new Status(StatusCode.Aborted, except.Message));
                    //return Task.FromResult(new DefaultResponse() { IsSucceeded = false });
                }
                catch (Exception error)
                {
                    SendErrorOfClient(Errors.EnumServerError.UnknownError, error.Message);
                    throw new RpcException(new Status(StatusCode.Aborted, error.Message));
                    //return Task.FromResult(new DefaultResponse() { IsSucceeded = false });
                }
            }

        }

        public override Task<LoadMessage> LoadByFilterAsp(AspFilterMessage request, ServerCallContext context)
        {
            var nameTable = TypesConverter.ConvertToDBModel(request.Table);
            var NumberASP = request.NumberASP;
            //var IdClient = GetClientId(context);
            var IdClient = 1;
            var result = new LoadMessage() { Table = request.Table };

            try
            {
                SendMessToHost(IdClient, NameTableOperation.LoadByFilterAsp, nameTable.ToString());
                //if (!_clients.ContainsKey(IdClient))
                //{
                //    SendErrorOfClient(Errors.EnumServerError.ClientAbsent, "");
                //    return null;
                //}
                if (!(dicOperTables[nameTable] is IDependentAsp))
                {
                    Errors.EnumDBError error = Errors.EnumDBError.NoColumnAsp;

                    SendErrorToHost(new OperationTableEventArgs(NameTableOperation.Load, error));

                    //throw new FaultException<InheritorsException.ExceptionWCF>(new InheritorsException.ExceptionWCF(Errors.EnumDBError.NoColumnAsp));
                }

                var dataDependAsp = (dicOperTables[nameTable] as IDependentAsp).LoadByFilter(IdClient, NumberASP);
                var data = ClassDataCommon.ConvertToListAbstractCommonTable(dataDependAsp.ListRecords);
                result.Records.AddRange(TypesConverter.ConvertToProto(data, nameTable)); //проверить

                return Task.FromResult(result);
            }
            catch (InheritorsException.ExceptionLocalDB except)
            {
                SendErrorToHost(new OperationTableEventArgs(NameTableOperation.Load, except.Error, except.Message));
                SendError(except, NameTableOperation.LoadByFilterAsp);
                throw new RpcException(new Status(StatusCode.Aborted, except.Message));
                //return Task.FromResult(result); //отправлять ошибку
                
            }
            catch (Exception error)
            {
                SendErrorToHost(new OperationTableEventArgs(NameTableOperation.Load, error.Message));
                throw new RpcException(new Status(StatusCode.Aborted, error.Message));
                //return Task.FromResult(result); //отправлять ошибку
            }
        }


        public override Task<DefaultResponse> ClearByFilterAsp(AspFilterMessage request, ServerCallContext context)
        {
            var nameTable = TypesConverter.ConvertToDBModel(request.Table);
            var NumberASP = request.NumberASP;
            //var IdClient = GetClientId(context);
            var IdClient = 1;

            lock (dicOperTables[nameTable])
            {
                try
                {
                    SendMessToHost(IdClient, NameTableOperation.ClearByFilter, nameTable.ToString());

                    //if (!_clients.ContainsKey(IdClient))
                    //{
                    //    SendErrorOfClient(Errors.EnumServerError.ClientAbsent, "");
                    //    return null;
                    //}

                    Dictionary<NameTable, int> keyValuePairs = new Dictionary<NameTable, int>();
                    foreach (var table in dicOperTables)
                    {
                        if (table.Key == nameTable)
                            continue;
                        keyValuePairs.Add(table.Key, table.Value.Load(IdClient).ListRecords.Count);
                    }

                    if ((dicOperTables[nameTable] is IDependentAsp))
                    {
                        (dicOperTables[nameTable] as IDependentAsp).ClearByFilter(IdClient, NumberASP);
                    }

                    foreach (var table in keyValuePairs)
                    {
                        if (dicOperTables[table.Key].Load(IdClient).ListRecords.Count != table.Value)
                        {
                            //Task.Delay(100);
                            dicOperTables[table.Key].UpDate(IdClient);
                        }
                    }
                    SendMessToHost(IdClient, NameTableOperation.ClearByFilter, "Ok");
                    return Task.FromResult(new DefaultResponse() { IsSucceeded = true });
                }
                catch (InheritorsException.ExceptionLocalDB except)
                {
                    SendError(except, NameTableOperation.ClearByFilter);
                    throw new RpcException(new Status(StatusCode.Aborted, except.Message));
                   // return Task.FromResult(new DefaultResponse() { IsSucceeded = false });
                }
                catch (Exception error)
                {
                    SendErrorOfClient(Errors.EnumServerError.UnknownError, error.Message);
                    throw new RpcException(new Status(StatusCode.Aborted, error.Message));
                    //return Task.FromResult(new DefaultResponse() { IsSucceeded = false });
                }
            }
        }


        private EventHandler<DataEventArgs> UpDataHandler = null;
        
        public override async Task Subscribe(DefaultRequest request, IServerStreamWriter<LoadMessage> responseStream, ServerCallContext context)
        {
            try
            {
                UpDataHandler = async (sender, args) => await HandlerUpData(responseStream, args);
                Operation.OnReceiveData += UpDataHandler;

                await AwaitCancellation(context.CancellationToken, UpDataHandler);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception on Server");
            }
        }

        private static Task AwaitCancellation(CancellationToken token, EventHandler<DataEventArgs> func)
        {
            var completion = new TaskCompletionSource<object>();
            token.Register(() => {
                completion.SetResult(null);
                Operation.OnReceiveData -= func;
            });
            return completion.Task;
        }

        private Semaphore _SendLock = new Semaphore(1, 1);
        private async Task HandlerUpData(IServerStreamWriter<LoadMessage> responseStream, DataEventArgs eventArgs)
        {
            try
            {
                var result = new LoadMessage() { Table = eventArgs.Name.ConvertToProto() };
                result.Records.AddRange(TypesConverter.ConvertToProto(eventArgs.AbstractData, eventArgs.Name));
                _SendLock.WaitOne();
                await responseStream.WriteAsync(result);
                _SendLock.Release();
            }
            catch (Exception excp)
            {
                SendErrorToHost(new OperationTableEventArgs(NameTableOperation.Update, excp.Message + $" {excp.InnerException?.Message} "));
            }
        }


        private int GetClientId(ServerCallContext context)
        {
            var metadataValue = context.RequestHeaders.FirstOrDefault(e => e.Key == "clientid").Value;
            var id = metadataValue != null ? Convert.ToInt32(metadataValue) : 0;
            return id;
        }

        private void CheckResFF(NameTable nameTable, int IdClient, int countFF)
        {
            if (nameTable == NameTable.TableAnalogResFF || nameTable == NameTable.TableDigitalResFF)
            {
                //Task.Delay(100);
                if (dicOperTables[NameTable.TableResFF].Load(IdClient).ListRecords.Count != countFF)
                    dicOperTables[NameTable.TableResFF].UpDate(IdClient);
            }
        }
    }
}
