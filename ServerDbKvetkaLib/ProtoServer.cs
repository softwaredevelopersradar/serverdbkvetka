﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using TransmissionPackage;
using KvetkaModelsDBLib;
using OperationTablesLib;
using InheritorsEventArgs;
using ProtoLibrary;
using System.Threading;

namespace ServerDbKvetkaLib
{
    public partial class ProtoServer : Transmission.TransmissionBase
    {
        #region Clients
        private static ConcurrentDictionary<int, ClientService> _clients = new ConcurrentDictionary<int, ClientService>();
        private static object _clientLocker = new object();
        private static int _nextId = 1;
        #endregion

        /// <summary>
        /// словарь, который хранит String - имя таблицы, 
        /// ITableAction - объект класса, который реализует интерфейс(ITableAction - действия над таблицей) 
        /// </summary>
        private static Dictionary<NameTable, ITableAction> dicOperTables;


        public ProtoServer()
        {
            //_clients = new Dictionary<string, DateTime>();
            if (dicOperTables != null) return;
            //Operation.OnReceiveData += HandlerUpData;
            //Operation.OnAddRange += HandlerAddRange;                
            //Operation.OnReceiveRecord += HandlerUpRecord;
            //Operation.OnSendMessToHost += HandlerMessToHostOperation;

            dicOperTables = new Dictionary<NameTable, ITableAction>
            {
                { NameTable.TableJammer, new OperationTableJammer()},
                { NameTable.TableFreqForbidden, new OperationTableDependAsp<TableFreqForbidden>()},
                { NameTable.TableFreqKnown, new OperationTableDependAsp<TableFreqKnown>()},
                { NameTable.TableFreqImportant, new OperationTableDependAsp<TableFreqImportant>()},
                { NameTable.TableFreqRangesElint, new OperationTableDependAsp<TableFreqRangesElint>()},
                { NameTable.TableFreqRangesJamming, new OperationTableDependAsp<TableFreqRangesJamming>()},
                { NameTable.TableSectorsElint, new OperationTableDependAsp<TableSectorsElint>()},
                { NameTable.TableADSB, new OperationTableADSB()},
                { NameTable.TableDigitalResFF, new OperationTableHasFF<TableDigitalResFF>()}, 
                { NameTable.TableAnalogResFF, new OperationTableHasFF<TableAnalogResFF>()},
                { NameTable.TableResFF, new OperationTableResFF()},
                { NameTable.TableTrackResFF, new OperationTable<TableTrackResFF>()},
                { NameTable.TableResFFJam, new OperationTableDependAsp<TableResFFJam>()},

                { NameTable.TableResFHSS, new OperationTableResFHSS()},
                { NameTable.TableSubscriberResFHSS, new OperationTable<TableSubscriberResFHSS>()},
                { NameTable.TableTrackSubscriberResFHSS, new OperationTable<TableTrackSubscriberResFHSS>()},
                { NameTable.TableResFHSSJam, new OperationTableDependAsp<TableResFHSSJam>()},
                { NameTable.GlobalProperties, new OperationGlobalProperties()},
                { NameTable.TempSuppressFF, new OperationTempDependASP<TempSuppressFF>()},
                { NameTable.TempSuppressFHSS, new OperationTempDependASP<TempSuppressFHSS>()},
                { NameTable.TableResFFDistribution, new OperationTable<TableResFFDistribution>()},
                { NameTable.TableRoutePoint, new OperationTable<TableRoutePoint>()},
                { NameTable.TableRoute, new OperationTableRoute()},
                { NameTable.TableFHSSExcludedJam, new OperationTable<TableFHSSExcludedJam>()}
            };
        }

        public override Task<IdMessage> Connect(PingRequest name, ServerCallContext context)
        {
            lock (_clientLocker)
            {
                ClientService client = new ClientService()
                {
                    ID = _nextId,
                    Name = name.ClientName
                };
                _nextId++;
                _clients.TryAdd(client.ID, client);

                SendStateClient(client.Name, ServerEventArgs.ActClient.Connect);

                return Task.FromResult(new IdMessage() { Id = client.ID });
            }
        }

        public override Task<PingResponse> Ping(IdMessage request, ServerCallContext context)
        {
            if (_clients.ContainsKey(request.Id))
            {
                return Task.FromResult(new PingResponse() { IsFirstPingRequest = true });
            }
            return Task.FromResult(new PingResponse() { IsFirstPingRequest = false });
        }

        public override Task<DefaultResponse> Disconnect(IdMessage message, ServerCallContext context)
        {
            if (_clients.ContainsKey(message.Id))
            {
                var result = _clients.TryRemove(message.Id, out ClientService removedClient);
                if (result)
                {
                    SendStateClient(removedClient.Name, ServerEventArgs.ActClient.Disconnect);
                    Operation.OnReceiveData -= UpDataHandler;
                }
                return Task.FromResult(new DefaultResponse() { IsSucceeded = true }); 
            }
            return Task.FromResult(new DefaultResponse() { IsSucceeded = true });
        }
        

        #region Events
        public static event EventHandler<OperationTableEventArgs> OnReceiveMsgDB = (object obj, OperationTableEventArgs eventArgs) => { };
        public static event EventHandler<OperationTableEventArgs> OnReceiveErrorDB = (object obj, OperationTableEventArgs eventArgs) => { };

        public static event EventHandler<ServerEventArgs> OnStateClient = (object obj, ServerEventArgs eventArgs) => { };
        public static event EventHandler<ServerEventArgs> OnErrorClient = (object obj, ServerEventArgs eventArgs) => { };

        public static event EventHandler<string> OnMessToHost = (obj, eventArgs) => { };
        #endregion

        private void SendStateClient(string ClientName, ServerEventArgs.ActClient act)
        {
            OnStateClient(this, new ServerEventArgs(DateTime.Now.ToShortTimeString(), ClientName, act));
        }

        private void SendErrorOfClient(Errors.EnumServerError error, string Mess)
        {
            OnErrorClient(this, new ServerEventArgs(DateTime.Now.ToShortTimeString(), error, Mess));
        }
        private void SendMessToHost(int idClient, NameTableOperation act, string Mess)
        {
            OnReceiveMsgDB(this, new OperationTableEventArgs(act, Errors.EnumDBError.None, Mess));
        }

        private void SendMessToHost(int idClient, NameChangeOperation act, string Mess)
        {
            OnReceiveMsgDB(this, new OperationTableEventArgs(act, Errors.EnumDBError.None, Mess));
        }
        private void SendErrorToHost(OperationTableEventArgs eventArgs)
        {
            OnReceiveErrorDB(this, eventArgs);
        }

        private void SendError(InheritorsException.ExceptionLocalDB error, NameTableOperation operation)
        {
            OperationTableEventArgs eventArgs = new OperationTableEventArgs(operation, error.Error, error.Message);

            SendErrorToHost(eventArgs);

            //if (_clients.ContainsKey(error.IdClient))
            //{
            //    _clients[error.IdClient].OperContext.GetCallbackChannel<IServerCallback>().ErrorCallback(eventArgs);
            //    return;
            //}
        }
    }
}
