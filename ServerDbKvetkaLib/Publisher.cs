﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Grpc.Core;
using System.Collections.Concurrent;
using TransmissionPackage;

namespace ServerDbKvetkaLib
{
    public class Publisher
    {
        private ConcurrentDictionary<string, IServerStreamWriter<LoadMessage>> users = new ConcurrentDictionary<string, IServerStreamWriter<LoadMessage>>();

        public void Join(string name, IServerStreamWriter<LoadMessage> response) => users.TryAdd(name, response);

        public void Remove(string name) => users.TryRemove(name, out var s);

        public async Task BroadcastMessageAsync(LoadMessage message) => await BroadcastMessages(message);

        private async Task BroadcastMessages(LoadMessage message)
        {
            foreach (var user in users)
            {
                var item = await SendMessageToSubscriber(user, message);
                if (item != null)
                {
                    Remove(item?.Key);
                };
            }
        }

        private async Task<KeyValuePair<string, IServerStreamWriter<LoadMessage>>?> SendMessageToSubscriber(KeyValuePair<string, IServerStreamWriter<LoadMessage>> user, LoadMessage message)
        {
            try
            {
                await user.Value.WriteAsync(message);
                return null;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return user;
            }
        }
    }
}
