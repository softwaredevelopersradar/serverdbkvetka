﻿using System;
using TransmissionPackage;

namespace GrpcClientLib
{
    internal class ClassTable : IDisposable
    {
        public static Transmission.TransmissionClient ClientServiceDB { get; internal set; } = null;

        public static int Id { get; internal set; } = 0;

        public ClassTable(Transmission.TransmissionClient clientServiceDB, int ID)
        {
            if (ClientServiceDB != null)
                return;

            ClientServiceDB = clientServiceDB;
            Id = ID;
        }

        public ClassTable()
        { }

        public void Dispose()
        {
            if (ClientServiceDB == null)
                return;

            //ClientServiceDB.Abort();
            ClientServiceDB = null;
        }
    }
}
