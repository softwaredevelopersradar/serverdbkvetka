﻿using System;
using TransmissionPackage;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using System.Threading.Tasks;
using System.Collections.Generic;
using KvetkaModelsDBLib;
using InheritorsEventArgs;
using System.Text.RegularExpressions;
using System.Threading;
using ProtoLibrary;

namespace GrpcClientLib
{
    public class ServiceClient : IDisposable
    {
        private int _ID;
        private string _name = "";
        private string _serverIp = "127.0.0.1";
        private int _serverPort = 30051;        
        private Transmission.TransmissionClient _client;

        public ServiceClient(string name, string serverIp, int serverPort)
        {
            try { 
                _name = name;
                _serverIp = serverIp;
                _serverPort = serverPort;

            }
            catch (Exception ex)
            { }
        }


        /// <summary>
        /// словарь, который хранит NameTable - имя таблицы, 
        /// ITableAction - объект класса, который реализует интерфейс(ITableAction - действия над таблицей) 
        /// </summary>
        public readonly Dictionary<NameTable, IClassTables> Tables = new Dictionary<NameTable, IClassTables>
        {
            { NameTable.TableJammer, new ClassTable<TableJammer>() },
            { NameTable.TableFreqForbidden, new ClassInheritTableAsp<TableFreqForbidden>() },
            { NameTable.TableFreqKnown, new ClassInheritTableAsp<TableFreqKnown>() },
            { NameTable.TableFreqImportant, new ClassInheritTableAsp<TableFreqImportant>() },
            { NameTable.TableFreqRangesElint, new ClassInheritTableAsp<TableFreqRangesElint>() },
            { NameTable.TableFreqRangesJamming, new ClassInheritTableAsp<TableFreqRangesJamming>() },
            { NameTable.TableSectorsElint, new ClassInheritTableAsp<TableSectorsElint>() },
            { NameTable.TableADSB, new ClassTable<TableADSB>() },

            { NameTable.TableDigitalResFF, new ClassTable<TableDigitalResFF>() },
            { NameTable.TableAnalogResFF, new ClassTable<TableAnalogResFF>() },
            { NameTable.TableResFF, new ClassTable<TableResFF>() },
            { NameTable.TableTrackResFF, new ClassTable<TableTrackResFF>()},
            { NameTable.TableResFFJam, new ClassInheritTableAsp<TableResFFJam>()},

            { NameTable.TableResFHSS, new ClassTable<TableResFHSS>() },
            { NameTable.TableSubscriberResFHSS, new ClassTable<TableSubscriberResFHSS>()},
            { NameTable.TableTrackSubscriberResFHSS, new ClassTable<TableTrackSubscriberResFHSS>()},
            { NameTable.TableResFHSSJam, new ClassInheritTableAsp<TableResFHSSJam>()},
            { NameTable.GlobalProperties, new ClassTable<GlobalProperties>()},
            { NameTable.TempSuppressFF, new ClassInheritTableAsp<TempSuppressFF>()},
            { NameTable.TempSuppressFHSS, new ClassInheritTableAsp<TempSuppressFHSS>()},
            { NameTable.TableResFFDistribution, new ClassTable<TableResFFDistribution>()},
            { NameTable.TableRoute, new ClassTable<TableRoute>()},
            { NameTable.TableRoutePoint, new ClassTable<TableRoutePoint>()},
            { NameTable.TableFHSSExcludedJam, new ClassTable<TableFHSSExcludedJam>()}
        };

        #region Events
        public event EventHandler<ClientEventArgs> OnConnect = (object obj, ClientEventArgs evenArgs) => { };
        public event EventHandler<ClientEventArgs> OnDisconnect = (object obj, ClientEventArgs evenArgs) => { };
        public event EventHandler<DataEventArgs> OnUpData = (object obj, DataEventArgs evenArgs) => { };
        public event EventHandler<OperationTableEventArgs> OnErrorDataBase = (object obj, OperationTableEventArgs evenArgs) => { };
        #endregion

        private bool ValidEndPoint(string endpointAddress)
        {
            string ValidEndpointRegex = @"\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b";    // IP validation 
            Regex r = new Regex(ValidEndpointRegex, RegexOptions.IgnoreCase | RegexOptions.Singleline);
            Match m = r.Match(endpointAddress);
            return m.Success;
        }

        public void Abort()
        {
           
              //(Transmission.TransmissionClient)_client.).Dispose(); //check
            _client = null;

            foreach (ClassTable table in Tables.Values)
                table.Dispose();
            OnDisconnect(null, new ClientEventArgs(ClientEventArgs.ActServer.Disconnect));
        }

        public bool IsConnected()
        {
            if (_client == null)
                return false;
            try
            {
                return _client.Ping(new IdMessage { Id = _ID }).IsFirstPingRequest;
            }
            catch (Exception except)
            {
                OnErrorDataBase(this, new OperationTableEventArgs(NameTableOperation.Update, except.Message));
                return false;
            }
        }

        public void Connect()
        {
            try
            {
                if (IsConnected())
                {
                    OnConnect(this, new ClientEventArgs(ClientEventArgs.ActServer.Connect));
                    return;
                }

                _client = new Transmission.TransmissionClient(
                    new Channel(_serverIp + ":" + _serverPort, ChannelCredentials.Insecure));

                //ID = ClientServiceDB.Connect(name);
                //_ID = 1;
                Task.Run(async () => await Subscribe());

                _ID = _client.Connect(new PingRequest() {ClientName = _name }).Id;

                ClassTable.Id = _ID;
                ClassTable.ClientServiceDB = _client;
                //Ping();
                OnConnect(this, new ClientEventArgs(ClientEventArgs.ActServer.Connect, $"Id = {_ID.ToString()}"));
                //LoadAllTables();
            }
            catch (Exception error)
            {
                OnDisconnect(this, new ClientEventArgs(ClientEventArgs.ActServer.Disconnect, error.Message));
            }
        }

        public void Disconnect()
        {
            try
            {
                if (_client != null)
                {
                    _client.Disconnect(new IdMessage() { Id = _ID });
                    Abort();
                }
            }
            catch (Exception error)
            {
                Abort();
                throw new ExceptionClient(error.Message);
            }
        }


        public bool Ping()
        {
               var result = _client.Ping(new IdMessage {Id = _ID });
            return result.IsFirstPingRequest;
        }

        private async Task Subscribe()
        {
            using (var stream = _client.Subscribe(new DefaultRequest()))
            {
                var tokenSource = new CancellationTokenSource();
                await DisplayAsync(stream.ResponseStream, tokenSource.Token);
               
                tokenSource.Cancel();
            }
        }

        private async Task DisplayAsync(IAsyncStreamReader<LoadMessage> stream, CancellationToken token)
        {
            try
            {
                while (await stream.MoveNext(cancellationToken: token))
                {
                    var responce = stream.Current;
                    var nameTable = TypesConverter.ConvertToDBModel(responce.Table);
                    var records = responce.Records.ConvertToDBModel(nameTable); 

                    OnUpData(this, new DataEventArgs(nameTable, records));
                    (Tables[nameTable] as IClickUpData).ClickUpTable(records);
                }
            }
            catch (RpcException e)
            {
                if (e.StatusCode == StatusCode.Cancelled)
                {
                    return;
                }

                Disconnect();
            }
            catch (OperationCanceledException)
            {
                Console.WriteLine("Finished.");
            }
            catch (Exception excp)
            {
                OnErrorDataBase(this, new OperationTableEventArgs(NameTableOperation.Update, excp.Message));
                Disconnect();
                return;
            }
        }


        public void Dispose()
        {
            try { Disconnect(); }
            catch { }
        }
    }
}
