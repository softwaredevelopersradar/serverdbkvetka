﻿using Errors;

namespace GrpcClientLib
{
    public interface IExceptionClient
    {
        EnumClientError Error { get; }

        string Message { get; }
    }
}
