﻿using InheritorsEventArgs;
using System;

namespace GrpcClientLib
{
    public interface ITableAddRange<T> where T : KvetkaModelsDBLib.AbstractCommonTable
    {
        event EventHandler<TableEventArgs<T>> OnAddRange;
    }
}
