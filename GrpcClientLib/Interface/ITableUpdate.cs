﻿using InheritorsEventArgs;
using System;

namespace GrpcClientLib
{
    public interface ITableUpdate<T> where T : KvetkaModelsDBLib.AbstractCommonTable
    {
        event EventHandler<TableEventArgs<T>> OnUpTable;
    }
}
