﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GrpcClientLib
{
    public interface IClassTables
    {
        void Add(object obj);

        Task AddAsync(object obj);

        void AddRange(object rangeObj);

        Task AddRangeAsync(object rangeObj);

        void RemoveRange(object rangeObj);

        Task RemoveRangeAsync(object rangeObj);

        void Delete(object obj);

        Task DeleteAsync(object obj);

        void Clear();

        Task CLearAsync();

        void Change(object obj);

        Task ChangeAsync(object obj);

        void ChangeRange(object rangeObj);

        Task ChangeRangeAsync(object rangeObj);

        List<T> Load<T>() where T : KvetkaModelsDBLib.AbstractCommonTable;

        Task<List<T>> LoadAsync<T>() where T : KvetkaModelsDBLib.AbstractCommonTable;

        void ClickUpTable(KvetkaModelsDBLib.ClassDataCommon data);
    }
}
