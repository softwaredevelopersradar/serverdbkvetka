﻿using System;

namespace GrpcClientLib
{
    public interface ITableUpRecord<T> where T : KvetkaModelsDBLib.AbstractCommonTable
    {
        event EventHandler<T> OnAddRecord;

        event EventHandler<T> OnDeleteRecord;

        event EventHandler<T> OnChangeRecord;
    }
}
