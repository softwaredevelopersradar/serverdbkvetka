﻿using InheritorsEventArgs;

namespace GrpcClientLib
{
    internal interface IClickUpRecord
    {
        void ClickUpRecord(RecordEventArgs eventArgs);
    }
}
