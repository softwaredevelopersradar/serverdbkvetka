﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrpcClientLib
{
    internal interface IClickUpData
    {
        void ClickUpTable(KvetkaModelsDBLib.ClassDataCommon dataCommon);
    }
}
