﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GrpcClientLib
{
    public interface IDependentAsp
    {
        List<T> LoadByFilter<T>(int NumberASP) where T : KvetkaModelsDBLib.AbstractDependentASP;
        Task<List<T>> LoadByFilterAsync<T>(int NumberASP) where T : KvetkaModelsDBLib.AbstractDependentASP;
        void ClearByFilter(int NumberASP);
    }
}
