﻿using Errors;
using KvetkaModelsDBLib;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationTablesLib
{
    public class OperationTableHasFF<T> : OperationTable<T> where T : AbstractHasFFTable
    {
        public override void Change(AbstractCommonTable record, int idClient)
        {
            try
            {
                T rec;
                lock (DataBase)
                {
                    using (TablesContext db = new TablesContext())
                    {
                        DbSet<T> Table = DataBase.GetTable<T>(Name);
                        //rec = Table.Find(record.GetKey());
                        rec = Table.Include(t => t.ResFF).AsNoTracking().Where(r => r.Id == record.Id).FirstOrDefault();
                        if (rec == null)
                            throw new InheritorsException.ExceptionLocalDB(idClient, EnumDBError.RecordNotFound);

                        var forDelete = rec.ResFF;

                        (rec as AbstractCommonTable).Update(record);

                        
                        db.Entry(rec).State = EntityState.Modified;

                        if (forDelete != null && (record as AbstractHasFFTable).ResFF == null)
                        {   
                            db.TResFF.Remove(forDelete);
                            db.SaveChanges();
                        }

                        if (rec.ResFF != null)
                        {
                            db.Entry(rec.ResFF).State =
                                rec.ResFF.Id == 0 ? EntityState.Added : EntityState.Modified;
                        }

                        db.SaveChanges();
                        db.Entry(rec).State = EntityState.Detached;
                    }
                }

                UpDate(idClient);
                return;
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public override void ChangeRange(ClassDataCommon data, int idClient)
        {
            try
            {  
                foreach (var record in data.ListRecords)
                {
                    T rec; 
                    lock (DataBase)
                    {
                        using (TablesContext db = new TablesContext())
                        {
                            DbSet<T> Table = DataBase.GetTable<T>(Name);
                            rec = Table.Include(t => t.ResFF).AsNoTracking().Where(r => r.Id == record.Id).FirstOrDefault();
                            if (rec != null)
                            {
                                var forDelete = rec.ResFF;

                                rec.Update(record);
                                
                                db.Entry(rec).State = EntityState.Modified;

                                if (forDelete != null && (record as AbstractHasFFTable).ResFF == null)
                                {
                                    db.TResFF.Remove(forDelete);
                                    db.SaveChanges();
                                }

                                if (rec.ResFF != null)
                                {
                                    db.Entry(rec.ResFF).State =
                                        rec.ResFF.Id == 0 ? EntityState.Added : EntityState.Modified;
                                }
                                db.SaveChanges();
                                db.Entry(rec).State = EntityState.Detached;
                            }
                                //rangeUpdate.Add(rec);
                        }

                    }
                }
                    //Table.UpdateRange(rangeUpdate);
                    //DataBase.SaveChanges();
                
                UpDate(idClient);
                return;
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public override void Clear(int idClient)
        {
            try
            {
                lock (DataBase)
                {
                    using (TablesContext db = new TablesContext())
                    {
                        DbSet<T> Table = db.GetTable<T>(Name);

                        Table.RemoveRange(Table.Include(t => t.ResFF).ToList());
                        db.SaveChanges();
                    }
                }
                UpDate(idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public override ClassDataCommon Load(int idClient)
        {
            ClassDataCommon data = new ClassDataCommon();
            try
            {
                using (TablesContext db = new TablesContext())
                {
                    var Table = db.GetTable<T>(Name);
                    data = ClassDataCommon.ConvertToListAbstractCommonTable(Table.Include(t => t.ResFF).ToList());
                }


                return data;
                //lock (DataBase)
                //{
                //    var Table = DataBase.GetTable<TableOwnUAV>(Name);

                //    var temp = ClassDataCommon.ConvertToListAbstractCommonTable(Table.Include(t => t.Frequencies).ToList());
                //    return temp;
                //}
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }
        //public override void Change(AbstractCommonTable record, int idClient)
        //{
        //    try
        //    {
        //        List<TableReconFWS> ChangeRange = new List<TableReconFWS>();

        //        lock (DataBase)
        //        {
        //            DbSet<TableReconFWS> Table = DataBase.GetTable<TableReconFWS>(Name);

        //            TableReconFWS rec = Table.Find(record.GetKey());
        //            if (rec == null)
        //                throw new InheritorsException.ExceptionLocalDB(idClient, EnumDBError.RecordNotFound);

        //            (rec as AbstractCommonTable).Update(record);
        //            ChangeRange.Add(rec);
        //            Table.Update(rec);
        //            if (rec.ListJamDirect != null)
        //            {
        //                DbSet<TableJamDirect> tableJamDirects = DataBase.TJamDirects;
        //                tableJamDirects.UpdateRange(rec.ListJamDirect);
        //            }
        //            DataBase.SaveChanges();
        //        }
        //        SendRange(this, new DataEventArgs(Name, ClassDataCommon.ConvertToListAbstractCommonTable(ChangeRange)));
        //        UpDate(idClient);
        //        return;
        //    }
        //    catch (InheritorsException.ExceptionLocalDB exception)
        //    {
        //        throw exception;
        //    }
        //    catch (DbUpdateException exDb)
        //    {
        //        throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
        //    }
        //}


        //public override void AddRange(ClassDataCommon data, int idClient)
        //{
        //    try
        //    {
        //        lock (DataBase)
        //        {
        //            DbSet<TableResFHSS> Table = DataBase.GetTable<TableResFHSS>(Name);
        //            //List<TableResFF> AddRange = new List<TableResFF>();
        //            foreach (var record in data.ListRecords)
        //            {
        //                var rec = Table.Find(record.GetKey());
        //                if (rec != null)
        //                {
        //                    rec.Update(record);
        //                    Table.Update(rec);
        //                    if ((record as TableResFHSS).ListSubscribers != null)
        //                    {
        //                        DbSet<TableSubscriberResFHSS> tableSubscribers = DataBase.TSubscriberResFHSS;
        //                        tableSubscribers.UpdateRange(rec.ListSubscribers);
        //                    }
        //                    //AddRange.Add(rec);
        //                }
        //                else
        //                {
        //                    Table.Add(record as TableResFHSS);
        //                    //AddRange.Add(record as TableResFF);
        //                }
        //            }
        //            DataBase.SaveChanges();
        //        }
        //        UpDate(idClient);
        //    }
        //    catch (InheritorsException.ExceptionLocalDB exception)
        //    {
        //        throw exception;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
        //    }
        //}

        //public override void ChangeRange(ClassDataCommon data, int idClient)
        //{
        //    try
        //    {
        //        lock (DataBase)
        //        {
        //            DbSet<TableResFHSS> Table = DataBase.GetTable<TableResFHSS>(Name);
        //            List<TableResFHSS> rangeUpdate = new List<TableResFHSS>();

        //            foreach (var record in data.ListRecords)
        //            {
        //                var rec = Table.Find(record.GetKey());
        //                if (rec != null)
        //                {
        //                    rec.Update(record);
        //                    Table.Update(rec);
        //                    if ((record as TableResFHSS).ListSubscribers != null)
        //                    {
        //                        DbSet<TableSubscriberResFHSS> tableSubscribers = DataBase.TSubscriberResFHSS;
        //                        tableSubscribers.RemoveRange(tableSubscribers.Where(c => c.TableResFHSSId == rec.Id).ToList());
        //                        DataBase.SaveChanges();
        //                    }
        //                    rangeUpdate.Add(rec);
        //                }
        //            }
        //            Table.UpdateRange(rangeUpdate);
        //            DataBase.SaveChanges();
        //        }
        //        UpDate(idClient);
        //        return;
        //    }
        //    catch (InheritorsException.ExceptionLocalDB exception)
        //    {
        //        throw exception;
        //    }
        //    catch (DbUpdateException exDb)
        //    {
        //        throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
        //    }
        //}

        //public override void Change(AbstractCommonTable record, int idClient)
        //{
        //    try
        //    {
        //        List<TableResFHSS> ChangeRange = new List<TableResFHSS>();
        //        lock (DataBase)
        //        {
        //            DbSet<TableResFHSS> Table = DataBase.GetTable<TableResFHSS>(Name);

        //            TableResFHSS rec = Table.Find(record.GetKey());
        //            if (rec == null)
        //                throw new InheritorsException.ExceptionLocalDB(idClient, EnumDBError.RecordNotFound);

        //            (rec as AbstractCommonTable).Update(record);

        //            if (rec.ListSubscribers != null)
        //            {
        //                DbSet<TableSubscriberResFHSS> tableSubscribers = DataBase.TSubscriberResFHSS;
        //                tableSubscribers.RemoveRange(tableSubscribers.Where(c => c.TableResFHSSId == rec.Id).ToList());
        //                DataBase.SaveChanges();
        //            }
        //            Table.Update(rec);
        //            DataBase.SaveChanges();
        //        }
        //        UpDate(idClient);

        //        return;
        //    }
        //    catch (InheritorsException.ExceptionLocalDB exception)
        //    {
        //        throw exception;
        //    }
        //    catch (DbUpdateException exDb)
        //    {
        //        throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
        //    }
        //}

        //public override ClassDataCommon Load(int idClient)
        //{
        //    ClassDataCommon data = new ClassDataCommon();
        //    try
        //    {
        //        lock (DataBase)
        //        {
        //            var Table = DataBase.GetTable<TableResFHSS>(Name);

        //            //испльзуем жадную загрузку связных таблиц
        //            //return ClassDataCommon.ConvertToListAbstractCommonTable(Table.Include(t => t.Track.Select(o => o.Bearing)).ToList());
        //            return ClassDataCommon.ConvertToListAbstractCommonTable(Table.Include(t => t.ListSubscribers).ThenInclude(o => o.Position).ToList());
        //        }
        //    }
        //    catch (InheritorsException.ExceptionLocalDB exception)
        //    {
        //        throw exception;
        //    }
        //    catch (DbUpdateException exDb)
        //    {
        //        throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
        //    }
        //}
    }
}
