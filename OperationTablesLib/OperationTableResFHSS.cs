﻿using System;
using System.Collections.Generic;
using System.Linq;
using Errors;
using KvetkaModelsDBLib;
using Microsoft.EntityFrameworkCore;

namespace OperationTablesLib
{
    public class OperationTableResFHSS : OperationTable<TableResFHSS>
    {
        public override void AddRange(ClassDataCommon data, int idClient)
        {
            try
            {
                lock (DataBase)
                {
                    DbSet<TableResFHSS> Table = DataBase.GetTable<TableResFHSS>(Name);
                    //List<TableResFF> AddRange = new List<TableResFF>();
                    foreach (var record in data.ListRecords)
                    {
                        var rec = Table.Find(record.GetKey());
                        if (rec != null)
                        {
                            rec.Update(record);
                            Table.Update(rec);
                            if ((record as TableResFHSS).ListSubscribers != null)
                            {
                                DbSet<TableSubscriberResFHSS> tableSubscribers = DataBase.TSubscriberResFHSS;
                                tableSubscribers.UpdateRange(rec.ListSubscribers);
                            }
                            //AddRange.Add(rec);
                        }
                        else
                        {
                            Table.Add(record as TableResFHSS);
                            //AddRange.Add(record as TableResFF);
                        }
                    }
                    DataBase.SaveChanges();
                }
                UpDate(idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public override void ChangeRange(ClassDataCommon data, int idClient)
        {
            try
            {
                lock (DataBase)
                {
                    DbSet<TableResFHSS> Table = DataBase.GetTable<TableResFHSS>(Name);
                    List<TableResFHSS> rangeUpdate = new List<TableResFHSS>();

                    foreach (var record in data.ListRecords)
                    {
                        var rec = Table.Find(record.GetKey());
                        if (rec != null)
                        {
                            rec.Update(record);
                            Table.Update(rec);
                            if ((record as TableResFHSS).ListSubscribers != null)
                            {
                                DbSet<TableSubscriberResFHSS> tableSubscribers = DataBase.TSubscriberResFHSS;
                                tableSubscribers.RemoveRange(tableSubscribers.Where(c => c.TableResFHSSId == rec.Id).ToList());
                                DataBase.SaveChanges();
                            }
                            rangeUpdate.Add(rec);
                        }
                    }
                    Table.UpdateRange(rangeUpdate);
                    DataBase.SaveChanges();
                }
                UpDate(idClient);
                return;
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public override void Change(AbstractCommonTable record, int idClient)
        {
            try
            {
                List<TableResFHSS> ChangeRange = new List<TableResFHSS>();
                lock (DataBase)
                {
                    DbSet<TableResFHSS> Table = DataBase.GetTable<TableResFHSS>(Name);

                    TableResFHSS rec = Table.Find(record.GetKey());
                    if (rec == null)
                        throw new InheritorsException.ExceptionLocalDB(idClient, EnumDBError.RecordNotFound);

                    (rec as AbstractCommonTable).Update(record);

                    if (rec.ListSubscribers != null)
                    {
                        DbSet<TableSubscriberResFHSS> tableSubscribers = DataBase.TSubscriberResFHSS;
                        tableSubscribers.RemoveRange(tableSubscribers.Where(c => c.TableResFHSSId == rec.Id).ToList());
                        DataBase.SaveChanges();
                    }
                    Table.Update(rec);
                    DataBase.SaveChanges();
                }
                UpDate(idClient);

                return;
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public override ClassDataCommon Load(int idClient)
        {
            ClassDataCommon data = new ClassDataCommon();
            try
            {
                lock (DataBase)
                {
                    var Table = DataBase.GetTable<TableResFHSS>(Name);

                    //испльзуем жадную загрузку связных таблиц
                    //return ClassDataCommon.ConvertToListAbstractCommonTable(Table.Include(t => t.Track.Select(o => o.Bearing)).ToList());
                    return ClassDataCommon.ConvertToListAbstractCommonTable(Table.Include(t => t.ListSubscribers).ThenInclude(o => o.Position).ToList());
                }
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }
    }
}
