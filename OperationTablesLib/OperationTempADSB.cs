﻿using System;
using System.Collections.Generic;
using System.Linq;
using InheritorsEventArgs;
using KvetkaModelsDBLib;

namespace OperationTablesLib
{
    public class OperationTableADSB : Operation, ITableAction
    {
        static Dictionary<int, Queue<TableADSB>> TempTable = new Dictionary<int, Queue<TableADSB>>();
        object locker = new object();

        public OperationTableADSB() : base()
        {
            Name = NameTable.TableADSB;
        }

        public void Add(AbstractCommonTable record, int idClient)
        {
            try
            {
                lock (locker)
                {
                    (record as TableADSB).Id = (record as TableADSB).ICAO.GetHashCode();

                    if (!TempTable.ContainsKey((int)record.GetKey().FirstOrDefault()))
                    {
                        TempTable.Add((int)record.GetKey().First(), new Queue<TableADSB>());
                    }
                    TempTable[(int)record.GetKey().First()].Enqueue(record as TableADSB);

                    if (TempTable[(int)record.GetKey().First()].Count > 50)
                        TempTable[(int)record.GetKey().First()].Dequeue();
                    //UpRecord(idClient, record, NameChangeOperation.Add);
                    UpDate(idClient, TempTable[(int)record.GetKey().First()]);
                }
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public void AddRange(ClassDataCommon data, int idClient)
        {
            throw new NotImplementedException();
        }

        public void RemoveRange(ClassDataCommon data, int idClient)
        {
            throw new NotImplementedException();
        }

        public void Change(AbstractCommonTable record, int idClient)
        {
            throw new NotImplementedException();
        }

        public void ChangeRange(ClassDataCommon data, int idClient)
        {
            throw new NotImplementedException();
        }

        public void Clear(int idClient)
        {
            try
            {
                TempTable.Clear();
                UpDate(idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public void Delete(AbstractCommonTable record, int idClient)
        {
            try
            {
                int key = (int)record.GetKey().FirstOrDefault();
                if (!TempTable.ContainsKey(key))
                {
                    throw new InheritorsException.ExceptionLocalDB(idClient, Errors.EnumDBError.RecordNotFound);
                }
                TempTable.Remove(key);
                UpDate(idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public ClassDataCommon Load(int idClient)
        {
            ClassDataCommon data = new ClassDataCommon();
            try
            {
                List<TableADSB> FullTable = new List<TableADSB>();
                foreach (var temps in TempTable.Values)
                {
                    FullTable.AddRange(temps.ToList());

                }
                return ClassDataCommon.ConvertToListAbstractCommonTable(FullTable);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public void UpDate(int idClient)
        {
            try
            {
                List<TableADSB> FullTable = new List<TableADSB>();
                foreach (var temps in TempTable.Values)
                {
                    FullTable.AddRange(temps.ToList());

                }
                base.SendUpData(this, new DataEventArgs(Name, ClassDataCommon.ConvertToListAbstractCommonTable(FullTable)));//, idClient));
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public void UpDate(int idClient, Queue<TableADSB> TableADSBs)
        {
            try
            {
                base.SendUpData(this, new DataEventArgs(Name, ClassDataCommon.ConvertToListAbstractCommonTable(TableADSBs.ToList())));//, idClient));
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public void UpRecord(int idClient, AbstractCommonTable record, NameChangeOperation action)
        {
            try
            {
                base.SendUpRecord(this, new RecordEventArgs(Name, record, action));
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }


    }
}
