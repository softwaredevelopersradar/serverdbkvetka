﻿using System;
using System.Collections.Generic;
using System.Linq;
using Errors;
using KvetkaModelsDBLib;
using Microsoft.EntityFrameworkCore;

namespace OperationTablesLib
{
    public class OperationTableRoute : OperationTable<TableRoute>
    {
        public override void AddRange(ClassDataCommon data, int idClient)
        {
            try
            {
                List<TableRoute> AddRange = new List<TableRoute>();
                lock (DataBase)
                {
                    DbSet<TableRoute> Table = DataBase.GetTable<TableRoute>(Name);
                    foreach (var record in data.ListRecords)
                    {
                        var rec = Table.Find(record.GetKey());
                        if (rec != null)
                        {
                            rec.Update(record);
                            Table.Update(rec);
                            if ((record as TableRoute).ListPoints != null)
                            {
                                DbSet<TableRoutePoint> tableJamBearing = DataBase.TRoutePoint;
                                tableJamBearing.UpdateRange(rec.ListPoints);
                            }
                            AddRange.Add(rec);
                        }
                        else
                        {
                            Table.Add(record as TableRoute);
                            AddRange.Add(record as TableRoute);
                        }
                    }
                    DataBase.SaveChanges();
                }
                UpDate(idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public override void Change(AbstractCommonTable record, int idClient)
        {
            try
            {
                List<TableRoute> ChangeRange = new List<TableRoute>();
                lock (DataBase)
                {
                    DbSet<TableRoute> Table = DataBase.GetTable<TableRoute>(Name);

                    TableRoute rec = Table.Find(record.GetKey());
                    if (rec == null)
                        throw new InheritorsException.ExceptionLocalDB(idClient, EnumDBError.RecordNotFound);

                    Table.Remove(rec);
                    DataBase.SaveChanges();
                    (rec as AbstractCommonTable).Update(record);
                    Table.Add(rec);
                    DataBase.SaveChanges();
                    //(rec as AbstractCommonTable).Update(record);
                    //if (rec.ListPoints != null)
                    //{
                    //    DbSet<TableRoutePoint> tableJamBearing = DataBase.TRoutePoint;
                    //    tableJamBearing.RemoveRange(tableJamBearing.Where(c => c.RouteId == rec.Id).ToList());
                    //    DataBase.SaveChanges();
                    //}
                    //Table.Update(rec);
                    //DataBase.SaveChanges();
                    ////ChangeRange.Add(rec);
                    ////Table.Update(rec);
                    ////if (rec.Bearing != null)
                    ////{
                    ////    DbSet<TableJamBearing> tableJamBearing = DataBase.TJamBearing;
                    ////    tableJamBearing.UpdateRange(rec.Bearing);
                    ////}
                    ////DataBase.SaveChanges();
                }
                UpDate(idClient);

                return;
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        

    }
}
