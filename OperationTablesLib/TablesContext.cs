﻿using System;
using KvetkaModelsDBLib;
using Microsoft.EntityFrameworkCore;

namespace OperationTablesLib
{
    public class TablesContext : DbContext
    {
        #region DbSet
        public DbSet<TableJammer> TJammer { get; set; }
        public DbSet<TableFreqKnown> TFreqKnown { get; set; }
        public DbSet<TableFreqForbidden> TFreqForbidden { get; set; }
        public DbSet<TableFreqImportant> TFreqImportant { get; set; }
        public DbSet<TableFreqRangesElint> TFreqRangesElint { get; set; }
        public DbSet<TableFreqRangesJamming> TFreqRangesJamming { get; set; }
        public DbSet<TableSectorsElint> TSectorsElint { get; set; }
        public DbSet<TableResFF> TResFF { get; set; }
        public DbSet<TableTrackResFF> TTrackResFF { get; set; }
        public DbSet<TableResFFJam> TResFFJam { get; set; }
        public DbSet<TableDigitalResFF> TDigitalResFF { get; set; }
        public DbSet<TableAnalogResFF> TAnalogResFF { get; set; }

        public DbSet<TableResFHSS> TResFHSS { get; set; }
        public DbSet<TableSubscriberResFHSS> TSubscriberResFHSS { get; set; }
        public DbSet<TableTrackSubscriberResFHSS> TTrackSubscriberResFHSS { get; set; }
        public DbSet<TableResFHSSJam> TResFHSSJam { get; set; }
        public DbSet<GlobalProperties> TGlobalProperties { get; set; }
        public DbSet<TableResFFDistribution> TResFFDistribution { get; set; }
        public DbSet<TableRoute> TRoute { get; set; }
        public DbSet<TableRoutePoint> TRoutePoint { get; set; }
        public DbSet<TableFHSSExcludedJam> TFHSSExcludedJam { get; set; }
        #endregion

        public DbSet<T> GetTable<T>(NameTable name) where T : class
        {
            switch (name)
            {
                case NameTable.TableJammer:
                    return TJammer as DbSet<T>;
                case NameTable.TableFreqForbidden:
                    return TFreqForbidden as DbSet<T>;
                case NameTable.TableFreqKnown:
                    return TFreqKnown as DbSet<T>;
                case NameTable.TableFreqImportant:
                    return TFreqImportant as DbSet<T>;
                case NameTable.TableFreqRangesElint:
                    return TFreqRangesElint as DbSet<T>;
                case NameTable.TableFreqRangesJamming:
                    return TFreqRangesJamming as DbSet<T>;
                case NameTable.TableSectorsElint:
                    return TSectorsElint as DbSet<T>;
                case NameTable.TableResFF:
                    return TResFF as DbSet<T>;
                case NameTable.TableTrackResFF:
                    return TTrackResFF as DbSet<T>;
                case NameTable.TableResFFJam:
                    return TResFFJam as DbSet<T>;
                case NameTable.TableDigitalResFF:
                    return TDigitalResFF as DbSet<T>;
                case NameTable.TableAnalogResFF:
                    return TAnalogResFF as DbSet<T>;

                case NameTable.TableResFHSS:
                    return TResFHSS as DbSet<T>;
                case NameTable.TableSubscriberResFHSS:
                    return TSubscriberResFHSS as DbSet<T>;
                case NameTable.TableTrackSubscriberResFHSS:
                    return TTrackSubscriberResFHSS as DbSet<T>;
                case NameTable.TableResFHSSJam:
                    return TResFHSSJam as DbSet<T>;
                case NameTable.GlobalProperties:
                    return TGlobalProperties as DbSet<T>;
                case NameTable.TableResFFDistribution:
                    return TResFFDistribution as DbSet<T>;
                case NameTable.TableRoute:
                    return TRoute as DbSet<T>;
                case NameTable.TableRoutePoint:
                    return TRoutePoint as DbSet<T>;
                case NameTable.TableFHSSExcludedJam:
                    return TFHSSExcludedJam as DbSet<T>;
                default:
                    return null;
            }
        }

        public TablesContext()
        {
            try
            {
                SQLitePCL.Batteries.Init();
            }
            catch (Exception ex)
            {
                var a = ex;
                Console.Write($"Error: {ex.Message}");
            }

            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.EnableSensitiveDataLogging(true);
            var temp = System.IO.Directory.GetCurrentDirectory();
            var location = System.Reflection.Assembly.GetExecutingAssembly();
            string path = "Filename="  + ".//KvetkaDB.db"; //+ System.IO.Directory.GetCurrentDirectory()
            optionsBuilder.UseSqlite(path);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TableJammer>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TableJammer>().OwnsOne(t => t.Coordinates);

            modelBuilder.Entity<TableFreqForbidden>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TableFreqImportant>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TableFreqKnown>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TableFreqRangesElint>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TableFreqRangesJamming>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TableSectorsElint>().Property(rec => rec.Id).ValueGeneratedOnAdd();

            modelBuilder.Entity<TableFreqImportant>().HasOne<TableJammer>().WithMany().HasForeignKey(rec => rec.NumberASP).OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<TableFreqForbidden>().HasOne<TableJammer>().WithMany().HasForeignKey(rec => rec.NumberASP).OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<TableFreqKnown>().HasOne<TableJammer>().WithMany().HasForeignKey(rec => rec.NumberASP).OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<TableFreqRangesElint>().HasOne<TableJammer>().WithMany().HasForeignKey(rec => rec.NumberASP).OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<TableFreqRangesJamming>().HasOne<TableJammer>().WithMany().HasForeignKey(rec => rec.NumberASP).OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<TableSectorsElint>().HasOne<TableJammer>().WithMany().HasForeignKey(rec => rec.NumberASP).OnDelete(DeleteBehavior.Cascade);


            modelBuilder.Entity<TableDigitalResFF>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TableDigitalResFF>().OwnsOne(t => t.Coordinates);
            modelBuilder.Entity<TableAnalogResFF>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TableAnalogResFF>().OwnsOne(t => t.Coordinates);
            modelBuilder.Entity<TableResFF>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TableResFF>().OwnsOne(t => t.Coordinates);
            modelBuilder.Entity<TableTrackResFF>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TableResFFJam>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TableResFFJam>().HasOne<TableJammer>().WithMany().HasForeignKey(rec => rec.NumberASP).OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<TableResFFJam>().OwnsOne(t => t.InterferenceParameters);
            modelBuilder.Entity<TableResFFJam>().OwnsOne(t => t.Coordinates);
            //modelBuilder.Entity<TableResFF>().HasMany(rec => rec.ListTrack).WithOne().HasForeignKey(rec => rec.TableResFFId)
            //    .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<TableTrackResFF>().OwnsOne(t => t.Coordinates);

            modelBuilder.Entity<TableAnalogResFF>().HasOne(rec => rec.ResFF).WithOne().HasForeignKey<TableResFF>(rec => rec.TableAnalogResFFId).OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<TableDigitalResFF>().HasOne(rec => rec.ResFF).WithOne().HasForeignKey<TableResFF>(rec => rec.TableDigitalResFFId).OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<TableResFFDistribution>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TableResFFDistribution>().OwnsOne(t => t.Coordinates);

            modelBuilder.Entity<TableResFHSS>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TableSubscriberResFHSS>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TableTrackSubscriberResFHSS>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TableResFHSSJam>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TableResFHSSJam>().HasOne<TableJammer>().WithMany().HasForeignKey(rec => rec.NumberASP).OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<TableResFHSSJam>().OwnsOne(t => t.InterferenceParameters);
            modelBuilder.Entity<TableResFHSS>().HasMany(rec => rec.ListSubscribers).WithOne().HasForeignKey(rec => rec.TableResFHSSId)
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<TableSubscriberResFHSS>().HasOne(rec => rec.Position).WithOne().HasForeignKey<TableTrackSubscriberResFHSS>(rec => new { rec.TableSubscriberResFHSSId, rec.TableResFHSSId })
                .HasPrincipalKey<TableSubscriberResFHSS>(rec => new { rec.Id, rec.TableResFHSSId })
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<TableTrackSubscriberResFHSS>().OwnsOne(t => t.Coordinates);

            modelBuilder.Entity<TableFHSSExcludedJam>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TableFHSSExcludedJam>().HasOne<TableResFHSSJam>().WithMany().HasForeignKey(rec => rec.FhssId).HasPrincipalKey(t=>t.Id).OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<TableRoute>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TableRoutePoint>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TableRoutePoint>().OwnsOne(t => t.Coordinates);
            modelBuilder.Entity<TableRoute>().HasMany(rec => rec.ListPoints).WithOne().HasForeignKey(rec => rec.RouteId).OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<GlobalProperties>().OwnsOne(t => t.Gnss);
            modelBuilder.Entity<GlobalProperties>().Property(rec => rec.TimeJamming).HasDefaultValue(8);
            modelBuilder.Entity<GlobalProperties>().Property(rec => rec.NumberOfPhasesAveragings).HasDefaultValue(1);
            modelBuilder.Entity<GlobalProperties>().Property(rec => rec.NumberOfBearingAveragings).HasDefaultValue(1);
            modelBuilder.Entity<GlobalProperties>().Property(rec => rec.CourseAngle).HasDefaultValue(-1);
            modelBuilder.Entity<GlobalProperties>().Property(rec => rec.AdaptiveThresholdFhss).HasDefaultValue(0);
            modelBuilder.Entity<GlobalProperties>().Property(rec => rec.NumberOfResInLetter).HasDefaultValue(4);
            modelBuilder.Entity<GlobalProperties>().Property(rec => rec.NumberOfChannelsInLetter).HasDefaultValue(2);
            modelBuilder.Entity<GlobalProperties>().Property(rec => rec.LongWorkingSignalDurationMs).HasDefaultValue(1);
            modelBuilder.Entity<GlobalProperties>().Property(rec => rec.Priority).HasDefaultValue(1);
            modelBuilder.Entity<GlobalProperties>().Property(rec => rec.Threshold).HasDefaultValue(0);
            modelBuilder.Entity<GlobalProperties>().Property(rec => rec.TimeRadiationFF).HasDefaultValue(500);
            modelBuilder.Entity<GlobalProperties>().Property(rec => rec.IsTest).HasDefaultValue(false);
            modelBuilder.Entity<GlobalProperties>().Property(rec => rec.FhssFftResolution).HasDefaultValue(FftResolution.N16384);
            modelBuilder.Entity<GlobalProperties>().Property(rec => rec.TimeRadiationFHSS).HasDefaultValue(300);
            modelBuilder.Entity<GlobalProperties>().Property(rec => rec.SearchSector).HasDefaultValue(2);
            modelBuilder.Entity<GlobalProperties>().Property(rec => rec.SearchTime).HasDefaultValue(1);
            modelBuilder.Entity<GlobalProperties>().Property(rec => rec.JsgAmPollInterval).HasDefaultValue(20);
            modelBuilder.Entity<GlobalProperties>().Property(rec => rec.JsgAmPower).HasDefaultValue(PowerPercent.N25);


            modelBuilder.Entity<GlobalProperties>(b =>
            {
                b.HasData(new
                {
                    Id = 1,
                    TimeJamming = 8,
                    NumberOfPhasesAveragings = 1,
                    NumberOfBearingAveragings = 1,
                    CourseAngle = -1,
                    AdaptiveThresholdFhss = 0,
                    NumberOfResInLetter = 4,
                    NumberOfChannelsInLetter = 2,
                    LongWorkingSignalDurationMs = 1,
                    Priority = (byte)1,
                    Threshold = 0,
                    TimeRadiationFF = 500,
                    IsTest = false,
                    FhssFftResolution = FftResolution.N16384,
                    TimeRadiationFHSS = 300,
                    SearchSector = (double)2,
                    SearchTime = 1,
                    JsgAmPollInterval = 20,
                    JsgAmPower = PowerPercent.N25
                });

                b.OwnsOne(e => e.Gnss).HasData(new
                {
                    GlobalPropertiesId = 1,
                    Altitude = (float)-1,
                    Latitude = (double)500,
                    Longitude = (double)500
                });

                //b.OwnsOne(e => e.Spoofing).HasData(new
                //{
                //    GlobalPropertiesId = 1,
                //    Altitude = (float)-1,
                //    Latitude = (double)-1,
                //    Longitude = (double)-1
                //});
            });
        }
    }
}
