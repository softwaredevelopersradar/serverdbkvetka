﻿using System;
using System.Linq;
using KvetkaModelsDBLib;
using Microsoft.EntityFrameworkCore;

namespace OperationTablesLib
{
    public class OperationTableResFF : OperationTable<TableResFF>
    {
        public override ClassDataCommon Load(int idClient)
        {
            ClassDataCommon data = new ClassDataCommon();
            try
            {
                using (TablesContext db = new TablesContext())
                {
                    var Table = db.GetTable<TableResFF>(Name);
                    data = ClassDataCommon.ConvertToListAbstractCommonTable(Table.ToList());
                }

                return data;
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }
        //public override void AddRange(ClassDataCommon data, int idClient)
        //{
        //    try
        //    {
        //        lock (DataBase)
        //        {
        //            DbSet<TableResFF> Table = DataBase.GetTable<TableResFF>(Name);
        //            //List<TableResFF> AddRange = new List<TableResFF>();
        //            foreach (var record in data.ListRecords)
        //            {
        //                var rec = Table.Find(record.GetKey());
        //                if (rec != null)
        //                {
        //                    rec.Update(record);
        //                    Table.Update(rec);
        //                    if ((record as TableResFF).ListTrack != null)
        //                    {
        //                        DbSet<TableTrackResFF> tableTrackResFF = DataBase.TTrackResFF;
        //                        tableTrackResFF.UpdateRange(rec.ListTrack);
        //                    }
        //                    //AddRange.Add(rec);
        //                }
        //                else
        //                {
        //                    Table.Add(record as TableResFF);
        //                    //AddRange.Add(record as TableResFF);
        //                }
        //            }
        //            DataBase.SaveChanges();
        //        }
        //        UpDate(idClient);
        //    }
        //    catch (InheritorsException.ExceptionLocalDB exception)
        //    {
        //        throw exception;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
        //    }
        //}

        //public override void ChangeRange(ClassDataCommon data, int idClient)
        //{
        //    try
        //    {
        //        lock (DataBase)
        //        {
        //            DbSet<TableResFF> Table = DataBase.GetTable<TableResFF>(Name);
        //            List<TableResFF> rangeUpdate = new List<TableResFF>();

        //            foreach (var record in data.ListRecords)
        //            {
        //                var rec = Table.Find(record.GetKey());
        //                if (rec != null)
        //                {
        //                    rec.Update(record);
        //                    Table.Update(rec);
        //                    if ((record as TableResFF).ListTrack != null)
        //                    {
        //                        DbSet<TableTrackResFF> tableTrackResFF = DataBase.TTrackResFF;
        //                        tableTrackResFF.RemoveRange(tableTrackResFF.Where(c => c.TableResFFId == rec.Id).ToList());
        //                        DataBase.SaveChanges();
        //                    }
        //                    rangeUpdate.Add(rec);
        //                }
        //            }
        //            Table.UpdateRange(rangeUpdate);
        //            DataBase.SaveChanges();
        //        }
        //        UpDate(idClient);
        //        return;
        //    }
        //    catch (InheritorsException.ExceptionLocalDB exception)
        //    {
        //        throw exception;
        //    }
        //    catch (DbUpdateException exDb)
        //    {
        //        throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
        //    }
        //}

        //public override void Change(AbstractCommonTable record, int idClient)
        //{
        //    try
        //    {
        //        List<TableResFF> ChangeRange = new List<TableResFF>();
        //        lock (DataBase)
        //        {
        //            DbSet<TableResFF> Table = DataBase.GetTable<TableResFF>(Name);

        //            TableResFF rec = Table.Find(record.GetKey());
        //            if (rec == null)
        //                throw new InheritorsException.ExceptionLocalDB(idClient, EnumDBError.RecordNotFound);

        //            (rec as AbstractCommonTable).Update(record);

        //            if (rec.ListTrack != null)
        //            {
        //                DbSet<TableTrackResFF> tableTrackResFF = DataBase.TTrackResFF;
        //                tableTrackResFF.RemoveRange(tableTrackResFF.Where(c => c.TableResFFId == rec.Id).ToList());
        //                DataBase.SaveChanges();
        //            }
        //            Table.Update(rec);
        //            DataBase.SaveChanges();
        //        }
        //        UpDate(idClient);

        //        return;
        //    }
        //    catch (InheritorsException.ExceptionLocalDB exception)
        //    {
        //        throw exception;
        //    }
        //    catch (DbUpdateException exDb)
        //    {
        //        throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
        //    }
        //}
    }
}
