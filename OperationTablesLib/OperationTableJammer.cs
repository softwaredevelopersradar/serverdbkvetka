﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using KvetkaModelsDBLib;
using Errors;

namespace OperationTablesLib
{
    public class OperationTableJammer : OperationTable<TableJammer>
    {
        public override void Add(AbstractCommonTable record, int idClient)
        {
            try
            {
                lock (DataBase)
                {
                    var Table = DataBase.GetTable<TableJammer>(Name);
                    if (Table.Find(record.GetKey()) != null)
                    {
                        throw new InheritorsException.ExceptionLocalDB(idClient, EnumDBError.RecordExist);
                    }

                    if ((record as TableJammer).IsOwn)
                    {
                        var OtherRecs = Table.Where(rec => (rec as TableJammer).IsOwn == true);
                        foreach (var otherRecs in OtherRecs)
                            (otherRecs as TableJammer).IsOwn = false;
                    }
                    DataBase.SaveChanges();
                }
                base.Add(record, idClient);
                    //if ((record as TableJammer).Role == StationRole.Own)
                    //{
                    //    var OtherRecs = Table.Where(rec => (rec as TableJammer).Role == StationRole.Own);
                    //    foreach (var otherRecs in OtherRecs)
                    //        (otherRecs as TableJammer).Role = StationRole.Complex;
                    //}

                    //if ((record as TableJammer).Role == StationRole.Linked)
                    //{
                    //    var OtherRecs = Table.Where(rec => (rec as TableJammer).Role == StationRole.Linked);
                    //    foreach (var otherRecs in OtherRecs)
                    //        (otherRecs as TableJammer).Role = StationRole.Complex;
                    //}
                    //Table.Add(record as TableJammer);
                    //DataBase.SaveChanges();
                //UpDate(idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                if (exception.InnerException != null)
                    throw exception.InnerException;
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public override void Change(AbstractCommonTable record, int idClient)
        {
            try
            {
                bool hasChanged = false;

                lock (DataBase)
                {
                    var Table = DataBase.GetTable<TableJammer>(Name);

                    TableJammer rec = Table.Find(record.GetKey());

                    if (rec == null)
                    {
                        throw new InheritorsException.ExceptionLocalDB(idClient, Errors.EnumDBError.RecordNotFound);
                    }

                    hasChanged = (record as TableJammer).IsOwn && !rec.IsOwn;

                    if (hasChanged)
                    {
                        var OtherRecs = Table.Where(t => (t as TableJammer).IsOwn == true);
                        foreach (var otherRecs in OtherRecs)
                            (otherRecs as TableJammer).IsOwn = false;
                    }

                    DataBase.SaveChanges();
                }
                base.Change(record, idClient);

                return;
                //bool hasChangedIsOwn = false;
                //bool hasChangedIsLincked = false;

                //lock (DataBase)
                //{
                //    var Table = DataBase.GetTable<TableJammer>(Name);

                //    TableJammer rec = Table.Find(record.GetKey());
                //    if (rec == null)
                //        throw new InheritorsException.ExceptionLocalDB(idClient, EnumDBError.RecordNotFound);

                //    hasChangedIsOwn = (record as TableJammer).Role == StationRole.Own && rec.Role != StationRole.Own;

                //    if (hasChangedIsOwn)
                //    {
                //        var OtherRecs = Table.Where(t => (t as TableJammer).Role == StationRole.Own);
                //        foreach (var otherRecs in OtherRecs)
                //            (otherRecs as TableJammer).Role = StationRole.Complex;
                //    }

                //    hasChangedIsLincked = (record as TableJammer).Role == StationRole.Linked && rec.Role != StationRole.Linked;

                //    if (hasChangedIsLincked)
                //    {
                //        var OtherRecs = Table.Where(t => (t as TableJammer).Role == StationRole.Linked);
                //        foreach (var otherRecs in OtherRecs)
                //            (otherRecs as TableJammer).Role = StationRole.Complex;
                //    }
                //    (rec as AbstractCommonTable).Update(record);
                //    Table.Update(rec);
                //    DataBase.SaveChanges();



                //}
                //UpDate(idClient);
                //return;
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }
    }
}
