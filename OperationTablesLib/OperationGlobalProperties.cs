﻿using KvetkaModelsDBLib;
using Microsoft.EntityFrameworkCore;

namespace OperationTablesLib
{
    public class OperationGlobalProperties : OperationTable<GlobalProperties>
    {
        public override void Add(AbstractCommonTable record, int idClient)
        {
            try
            {
                record.Id = 1;
                GlobalProperties recFind;
                lock (DataBase)
                {
                    DbSet<GlobalProperties> Table = DataBase.GetTable<GlobalProperties>(Name);
                    recFind = Table.Find(record.GetKey());
                }

                if (recFind != null)
                    base.Change(record, idClient);
                else
                    base.Add(record, idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
        }
    }
}
