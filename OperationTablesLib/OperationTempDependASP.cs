﻿using System;
using System.Linq;
using KvetkaModelsDBLib;

namespace OperationTablesLib
{
    public class OperationTempDependASP<T> : OperationTempTable<T>, IDependentAsp where T : AbstractDependentASP
    {
        public ClassDataDependASP LoadByFilter(int idClient, int NumberASP)
        {
            try
            {
                return ClassDataDependASP.ConvertToDataDependASP(TempTable.Values.Where(t => t.NumberASP == NumberASP).ToList());
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public void ClearByFilter(int idClient, int NumberASP)
        {
            try
            {
                var listRemove = TempTable.Values.Where(t => t.NumberASP == NumberASP).ToList();
                foreach (var rec in listRemove)
                {
                    TempTable.Remove((int)rec.GetKey().FirstOrDefault());
                }
                UpDate(idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

    }
}
