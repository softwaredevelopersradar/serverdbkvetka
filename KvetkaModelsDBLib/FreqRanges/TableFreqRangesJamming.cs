﻿using System.Runtime.Serialization;

namespace KvetkaModelsDBLib
{
    /// <summary>
    /// Диапазоны радиоразведки (ДРР)
    /// </summary>
    [DataContract]
    [KnownType(typeof(FreqRanges))]
    [InfoTable(NameTable.TableFreqRangesJamming)]

    public class TableFreqRangesJamming : FreqRanges
    {
    }
}
