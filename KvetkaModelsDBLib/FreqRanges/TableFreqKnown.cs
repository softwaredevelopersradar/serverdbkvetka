﻿using System.Runtime.Serialization;

namespace KvetkaModelsDBLib
{
    /// <summary>
    /// Известные частоты (ИЧ)
    /// </summary>
    [DataContract]
    [KnownType(typeof(FreqRanges))]
    [InfoTable(NameTable.TableFreqKnown)]

    public class TableFreqKnown : FreqRanges
    {
    }
}
