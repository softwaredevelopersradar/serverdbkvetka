﻿using System.Runtime.Serialization;

namespace KvetkaModelsDBLib
{
    /// <summary>
    /// Важные частоты (ВЧ)
    /// </summary>
    [DataContract]
    [KnownType(typeof(FreqRanges))]
    [InfoTable(NameTable.TableFreqImportant)]
    public class TableFreqImportant : FreqRanges
    {
    }
}
