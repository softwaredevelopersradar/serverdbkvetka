﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace KvetkaModelsDBLib
{
    /// <summary>
    /// Диапазоны частот
    /// </summary>
    [DataContract]
    [KnownType(typeof(AbstractDependentASP))]
    [KnownType(typeof(TableFreqForbidden))]
    [KnownType(typeof(TableFreqRangesElint))]
    [KnownType(typeof(TableFreqKnown))]
    [KnownType(typeof(TableFreqImportant))]
    [KnownType(typeof(TableFreqRangesJamming))]

    public class FreqRanges : AbstractDependentASP
    {
        [DataMember]
        [DisplayName(nameof(Id)), Browsable(false)]
        public override int Id { get; set; }

        [DataMember]
        [DisplayName(nameof(NumberASP)), ReadOnly(true), Browsable(true)]
        public override int NumberASP { get; set; }  // адрес станции

        [DataMember]
        [DisplayName(nameof(FreqMinKHz))]
        public double FreqMinKHz { get; set; } 

        [DataMember]
        [DisplayName(nameof(FreqMaxKHz))]
        public double FreqMaxKHz { get; set; }  

        [DataMember]
        [DisplayName(nameof(Note))]
        public String Note { get; set; } = String.Empty;

        [DataMember]
        [DisplayName(nameof(IsActive)), Browsable(false)]
        public bool IsActive { get; set; }


        public FreqRanges() { }
        public FreqRanges(int numberASP, double freqMinKHz, double freqMaxKHz, string note, bool isActive, int id = 0)
        {
            NumberASP = numberASP;
            FreqMinKHz = freqMinKHz;
            FreqMaxKHz = freqMaxKHz;
            Note = note;
            IsActive = isActive;
            Id = id;
        }


        public FreqRanges Clone()
        {
            return new FreqRanges(NumberASP, FreqMinKHz, FreqMaxKHz, Note, IsActive, Id);
        }

        public override object[] GetKey()
        {
            return new object[] { Id };
        }

        public TableFreqRangesElint ToFreqRangesElint()
        {
            TableFreqRangesElint table = new TableFreqRangesElint()
            {
                Id = Id,
                NumberASP = NumberASP,
                FreqMaxKHz = FreqMaxKHz,
                FreqMinKHz = FreqMinKHz,
                Note = Note,
                IsActive = IsActive
            };
            return table;
        }

        public TableFreqRangesJamming ToFreqRangesJamming()
        {
            TableFreqRangesJamming table = new TableFreqRangesJamming()
            {
                Id = Id,
                NumberASP = NumberASP,
                FreqMaxKHz = FreqMaxKHz,
                FreqMinKHz = FreqMinKHz,
                Note = Note,
                IsActive = IsActive
            };
            return table;
        }

        public TableFreqImportant ToFreqImportant()
        {
            TableFreqImportant table = new TableFreqImportant()
            {
                Id = Id,
                NumberASP = NumberASP,
                FreqMaxKHz = FreqMaxKHz,
                FreqMinKHz = FreqMinKHz,
                Note = Note,
                IsActive = IsActive
            };
            return table;
        }

        public TableFreqKnown ToFreqKnown()
        {
            TableFreqKnown table = new TableFreqKnown()
            {
                Id = Id,
                NumberASP = NumberASP,
                FreqMaxKHz = FreqMaxKHz,
                FreqMinKHz = FreqMinKHz,
                Note = Note,
                IsActive = IsActive
            };
            return table;
        }

        public TableFreqForbidden ToFreqForbidden()
        {
            TableFreqForbidden table = new TableFreqForbidden()
            {
                Id = Id,
                NumberASP = NumberASP,
                FreqMaxKHz = FreqMaxKHz,
                FreqMinKHz = FreqMinKHz,
                Note = Note,
                IsActive = IsActive
            };
            return table;
        }

        public override void Update(AbstractCommonTable record)
        {
            var newRec = (FreqRanges)record;

            FreqMaxKHz = newRec.FreqMaxKHz;
            FreqMinKHz = newRec.FreqMinKHz;
            Note = newRec.Note;
            IsActive = newRec.IsActive;
        }
    }
}
