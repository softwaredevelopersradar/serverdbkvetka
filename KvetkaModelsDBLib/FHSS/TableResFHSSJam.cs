﻿
using System;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace KvetkaModelsDBLib
{
    /// <summary>
    /// ИРИ ППРЧ РП
    /// </summary>
    [DataContract]
    [KnownType(typeof(AbstractDependentASP))]
    [InfoTable(NameTable.TableResFHSSJam)]

    public class TableResFHSSJam : AbstractDependentASP
    {
        [DataMember]
        public override int Id { get; set; }

        [DataMember]
        public override int NumberASP { get; set; }  

        [DataMember]
        public bool IsActive { get; set; }

        [DataMember]
        public byte Sign { get; set; }

        [DataMember]
        public double FrequencyMinKHz { get; set; }

        [DataMember]
        public double FrequencyMaxKHz { get; set; }

        [DataMember]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public InterferenceParameters InterferenceParameters { get; set; } = new InterferenceParameters();

        [DataMember]
        public short Threshold { get; set; }

        [DataMember]
        public byte[] Letters { get; set; } = new byte[] { };

        [DataMember]
        public byte Priority { get; set; }


        public override object[] GetKey()
        {
            return new object[] { Id };
        }

        public TableResFHSSJam() { }
        public TableResFHSSJam(int numberAsp, bool isActive, byte sign, double frequencyMinKHz, double frequencyMaxKHz, InterferenceParameters interferenceParameters, short threshold, byte[] letters, byte priority, int id = 0)
        {
            NumberASP = numberAsp;
            IsActive = isActive;
            Sign = sign;
            FrequencyMinKHz = frequencyMinKHz;
            FrequencyMaxKHz = frequencyMaxKHz;
            InterferenceParameters = interferenceParameters.Clone();
            Threshold = threshold;
            Letters = letters; //TODO: check
            Priority = priority;
            Id = id;
        }

        public override void Update(AbstractCommonTable record)
        {
            var newRecord = (TableResFHSSJam)record;

            IsActive = newRecord.IsActive;
            Sign = newRecord.Sign;
            FrequencyMinKHz = newRecord.FrequencyMinKHz;
            FrequencyMaxKHz = newRecord.FrequencyMaxKHz;
            InterferenceParameters.Update(newRecord.InterferenceParameters);
            Threshold = newRecord.Threshold;
            Letters = new byte[newRecord.Letters.Length]; 
            Array.Copy(newRecord.Letters, Letters, newRecord.Letters.Length);
            Priority = newRecord.Priority;
        }

        public TableResFHSSJam Clone() => new TableResFHSSJam( NumberASP, IsActive, Sign, FrequencyMinKHz, FrequencyMaxKHz, InterferenceParameters, Threshold, Letters, Priority, Id);
    }
}
