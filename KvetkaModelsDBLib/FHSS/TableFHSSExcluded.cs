﻿
using System.Runtime.Serialization;

namespace KvetkaModelsDBLib
{
    /// <summary>
    /// ППРЧ исключенные
    /// </summary>
    [DataContract]
    [KnownType(typeof(AbstractDependentFHSS))]
    [KnownType(typeof(TableFHSSExcludedJam))]
    public class TableFHSSExcluded : AbstractDependentFHSS
    {
        [DataMember]
        public override int Id { get; set; }

        [DataMember]
        public override int FhssId { get; set; } 

        [DataMember]
        public double FrequencyKHz { get; set; }

        [DataMember]
        public float Deviation { get; set; }

        
        public TableFHSSExcluded() { }
        public TableFHSSExcluded(int fhssId, double frequencyKHz, float deviation, int id = 0)
        {
            FrequencyKHz = frequencyKHz;
            Deviation = deviation;
            FhssId = fhssId;
            Id = id;
        }


        public TableFHSSExcluded Clone()
        {
            return new TableFHSSExcluded(FhssId, FrequencyKHz, Deviation, Id);
        }

        public override object[] GetKey()
        {
            return new object[] { Id };
        }

        public TableFHSSExcludedJam ToFHSSExcludedJam()
        {
            TableFHSSExcludedJam table = new TableFHSSExcludedJam()
            {
                Id = Id,
                FrequencyKHz = FrequencyKHz,
                Deviation = Deviation,
                FhssId = FhssId
            };
            return table;
        }

        public override void Update(AbstractCommonTable record)
        {
            var newRec = (TableFHSSExcluded)record;

            Deviation = newRec.Deviation;
            FrequencyKHz = newRec.FrequencyKHz;
        }
    }
}
