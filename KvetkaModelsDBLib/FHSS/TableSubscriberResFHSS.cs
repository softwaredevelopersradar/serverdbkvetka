﻿using System;
using System.Runtime.Serialization;

namespace KvetkaModelsDBLib
{
    /// <summary>
    /// Абоненты ИРИ ППРЧ
    /// </summary>
    [DataContract]
    [KnownType(typeof(AbstractCommonTable))]
    [KnownType(typeof(TableTrackSubscriberResFHSS))]
    [InfoTable(NameTable.TableSubscriberResFHSS)]
    public class TableSubscriberResFHSS : AbstractCommonTable
    {
        [DataMember]
        public override int Id { get; set; }

        [DataMember]
        public int TableResFHSSId { get; set; }

        [DataMember]
        public DateTime Time { get; set; }

        [DataMember]
        //public ObservableCollection<TableTrackSubscriberResFHSS> ListTrack { get; set; }
        public TableTrackSubscriberResFHSS Position { get; set; } = new TableTrackSubscriberResFHSS();

        public override object[] GetKey()
        {
            return new object[] { Id };
        }

        public TableSubscriberResFHSS() { }
        public TableSubscriberResFHSS(int tableResFHSSId, DateTime time, TableTrackSubscriberResFHSS position, int id = 0)
        {
            TableResFHSSId = tableResFHSSId;
            Time = time;
            Position = position; //возможно сделать  Position = position?.Clone();
            Id = id;
        }

        public TableSubscriberResFHSS Clone() => new TableSubscriberResFHSS(TableResFHSSId, Time, Position, Id);


        public override void Update(AbstractCommonTable record)
        {
            var newRecord = (TableSubscriberResFHSS)record;

            Time = newRecord.Time;
            Position = newRecord.Position;
            //возможно сделать:
            //if (ResFF == null)
            //{ ResFF = newRecord.ResFF; }
            //else
            //{ ResFF.Update(newRecord.ResFF); }




            //Position = Position ?? newRecord.Position;
            //if (Position != newRecord.Position)
            //{
            //    Position.Clear();
            //    foreach (var point in newRecord.Position)
            //        Position.Add(point);
            //}
        }
    }
}
