﻿
using System.Runtime.Serialization;

namespace KvetkaModelsDBLib
{
    /// <summary>
    /// ППРЧ РП исключенные.
    /// </summary>

    [DataContract]
    [KnownType(typeof(TableFHSSExcluded))]
    [InfoTable(NameTable.TableFHSSExcludedJam)]
    public class TableFHSSExcludedJam : TableFHSSExcluded
    {

    }
}
