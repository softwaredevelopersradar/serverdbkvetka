﻿using System.Runtime.Serialization;
using System.ComponentModel;

namespace KvetkaModelsDBLib
{
    /// <summary>
    /// Траектория абонента ИРИ ППРЧ
    /// </summary>
    [DataContract]
    [KnownType(typeof(AbstractCommonTable))]
    [InfoTable(NameTable.TableTrackSubscriberResFHSS)]
    public class TableTrackSubscriberResFHSS : AbstractCommonTable
    {
        [DataMember]
        public override int Id { get; set; }

        [DataMember]
        public int TableSubscriberResFHSSId { get; set; }

        [DataMember]
        public int TableResFHSSId { get; set; }

        [DataMember]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Coord Coordinates { get; set; } = new Coord();

        [DataMember]
        public float BearingOwn { get; set; }

        [DataMember]
        public float BearingLinked { get; set; }

        [DataMember]
        public float DistanceOwn { get; set; }

        [DataMember]
        public float DistanceLinked { get; set; }

        public override object[] GetKey()
        {
            return new object[] { Id };
        }

        public TableTrackSubscriberResFHSS() { }
        public TableTrackSubscriberResFHSS(int tableSubscriberResFHSSId, int tableResFHSSId, Coord coord, float bearingOwn, float bearingLinked, float distanceOwn, float distanceLinked, int id = 0)
        {
            TableSubscriberResFHSSId = tableSubscriberResFHSSId;
            TableResFHSSId = tableResFHSSId;
            Coordinates = new Coord(coord.Latitude, coord.Longitude, coord.Altitude);
            BearingOwn = bearingOwn;
            BearingLinked = bearingLinked;
            DistanceOwn = distanceOwn;
            DistanceLinked = distanceLinked;
            Id = id;
        }

        public TableTrackSubscriberResFHSS Clone() => new TableTrackSubscriberResFHSS(TableSubscriberResFHSSId, TableResFHSSId, Coordinates, BearingOwn, BearingLinked, DistanceOwn, DistanceLinked, Id);

        public override void Update(AbstractCommonTable record)
        {
            var newRecord = (TableTrackSubscriberResFHSS)record;

            Coordinates.Update(newRecord.Coordinates);
            BearingOwn = newRecord.BearingOwn;
            BearingLinked = newRecord.BearingLinked;
            DistanceOwn = newRecord.DistanceOwn;
            DistanceLinked = newRecord.DistanceLinked;
        }
    }
}
