﻿
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace KvetkaModelsDBLib
{
    /// <summary>
    /// ИРИ ППРЧ
    /// </summary>
    [DataContract]
    [KnownType(typeof(AbstractCommonTable))]
    [KnownType(typeof(TableSubscriberResFHSS))]
    [InfoTable(NameTable.TableResFHSS)]

    public class TableResFHSS : AbstractCommonTable
    {

        [DataMember]
        public override int Id { get; set; }

        [DataMember]
        public double FrequencyMinKHz { get; set; }

        [DataMember]
        public double FrequencyMaxKHz { get; set; }

        [DataMember]
        public float BandKHz { get; set; }

        [DataMember]
        public byte Modulation { get; set; }

        [DataMember]
        public short ManipulationVelocity { get; set; }

        [DataMember]
        public short StepKHz { get; set; }

        [DataMember]
        public short ImpulseDuration { get; set; }

        [DataMember]
        public ObservableCollection<TableSubscriberResFHSS> ListSubscribers { get; set; } = new ObservableCollection<TableSubscriberResFHSS>();
        
        [DataMember]
        public int IdBearingDb { get; set; }
        

        public override object[] GetKey()
        {
            return new object[] { Id };
        }
        

        public TableResFHSS() { }
        public TableResFHSS(double frequencyMinKHz, double frequencyMaxKHz, float bandKHz, byte modulation, short manipulationVelocity, short stepKHz, short impulseDuration, ObservableCollection<TableSubscriberResFHSS> listSubscribers, int idBearingDb,  int id = 0)
        {
            FrequencyMinKHz = frequencyMinKHz;
            FrequencyMaxKHz = frequencyMaxKHz;
            BandKHz = bandKHz;
            Modulation = modulation;
            ManipulationVelocity = manipulationVelocity;
            StepKHz = stepKHz;
            ImpulseDuration = impulseDuration;
            ListSubscribers = new ObservableCollection<TableSubscriberResFHSS>(listSubscribers);
            Id = id;
            IdBearingDb = idBearingDb;
        }

        public TableResFHSS Clone() => new TableResFHSS(FrequencyMinKHz, FrequencyMaxKHz, BandKHz, Modulation, ManipulationVelocity, StepKHz, ImpulseDuration, ListSubscribers, IdBearingDb, Id);

        public override void Update(AbstractCommonTable record)
        {
            var newRecord = (TableResFHSS)record;

            FrequencyMinKHz = newRecord.FrequencyMinKHz;
            FrequencyMaxKHz = newRecord.FrequencyMaxKHz;
            BandKHz = newRecord.BandKHz;
            Modulation = newRecord.Modulation;
            ManipulationVelocity = newRecord.ManipulationVelocity;
            StepKHz = newRecord.StepKHz;
            ImpulseDuration = newRecord.ImpulseDuration;
            IdBearingDb = newRecord.IdBearingDb;

            ListSubscribers = ListSubscribers ?? newRecord.ListSubscribers;
            if (ListSubscribers != newRecord.ListSubscribers)
            {
                ListSubscribers.Clear();
                foreach (var subscriber in newRecord.ListSubscribers)
                    ListSubscribers.Add(subscriber);
            }

        }
    }
}
