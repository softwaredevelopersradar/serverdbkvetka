﻿
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace KvetkaModelsDBLib
{
    [DataContract]
    public class ClassDataCommon
    {
        [DataMember]
        public List<AbstractCommonTable> ListRecords { get; set; }

        public ClassDataCommon()
        {
            ListRecords = new List<AbstractCommonTable>();
        }

        public List<T> ToList<T>() where T : class
        {
            return (from t in ListRecords let c = t as T select c).ToList();
        }

        public static ClassDataCommon ConvertToListAbstractCommonTable<T>(List<T> listRecords) where T : class
        {
            ClassDataCommon objListAbstractData = new ClassDataCommon();
            if (listRecords == null)
                return null;
            if (listRecords.Count == 0)
                return objListAbstractData;
            objListAbstractData.ListRecords = (from t in listRecords let c = t as AbstractCommonTable select c).ToList();
            return objListAbstractData;
        }
    }
}
