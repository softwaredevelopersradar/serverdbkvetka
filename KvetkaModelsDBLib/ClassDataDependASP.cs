﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace KvetkaModelsDBLib
{
    [DataContract]
    public class ClassDataDependASP
    {
        [DataMember]
        public List<AbstractDependentASP> ListRecords { get; set; }

        public ClassDataDependASP()
        {
            ListRecords = new List<AbstractDependentASP>();
        }

        public List<T> ToList<T>() where T : AbstractDependentASP
        {
            return (from t in ListRecords let c = t as T select c).ToList();
        }

        public static ClassDataDependASP ConvertToDataDependASP<T>(List<T> listRecords) where T : class
        {
            ClassDataDependASP objListAbstractData = new ClassDataDependASP();
            if (listRecords == null)
                return null;
            if (listRecords.Count == 0)
                return objListAbstractData;
            objListAbstractData.ListRecords = (from t in listRecords let c = t as AbstractDependentASP select c).ToList();
            return objListAbstractData;
        }
    }
}
