﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace KvetkaModelsDBLib
{
    /// <summary>
    /// Станция помех
    /// </summary>
    [DataContract]
    [KnownType(typeof(AbstractCommonTable))]
    [InfoTable(NameTable.TableJammer)]
    public class TableJammer : AbstractCommonTable
    {
        [DataMember]
        [DisplayName(nameof(Id)), ReadOnly(false)]
        public override int Id { get; set; }

        [DataMember]
        [DisplayName(" ")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Coord Coordinates { get; set; } = new Coord();

        [DataMember]
        [DisplayName(nameof(CallSign))]
        public string CallSign { get; set; } = String.Empty;

        [DataMember]
        [DisplayName(nameof(Note))]
        public String Note { get; set; } = String.Empty;

        [DataMember]
        [DisplayName(nameof(Role))]
        public StationRole Role { get; set; }

        [DataMember]
        [DisplayName(nameof(IsOwn))]
        public bool IsOwn { get; set; }

        [DataMember]
        [DisplayName(nameof(AntennaAngle))]
        public int AntennaAngle { get; set; }

        [DataMember]
        [DisplayName(nameof(CommunicationType))]
        public CommunicationType CommunicationType { get; set; }

        public TableJammer()
        { }

        public TableJammer(Coord coord, string callSign, string note, StationRole role, bool isOwn, int antennaAngle, CommunicationType communicationType, int id = 0)
        {
            Id = id;
            Coordinates = new Coord(coord.Latitude, coord.Longitude, coord.Altitude);
            CallSign = callSign;
            Note = note;
            Role = role;
            IsOwn = isOwn;
            AntennaAngle = antennaAngle;
            CommunicationType = communicationType;
        }
       

        public override object[] GetKey()
        {
            return new object[] { Id };
        }


        public override void Update(AbstractCommonTable record)
        {
            var newRecord = (TableJammer)record;

            Coordinates.Update(newRecord.Coordinates);
            CallSign = newRecord.CallSign;
            Note = newRecord.Note;
            Role = newRecord.Role;
            IsOwn = newRecord.IsOwn;
            AntennaAngle = newRecord.AntennaAngle;
            CommunicationType = newRecord.CommunicationType;
        }


        public TableJammer Clone() => new TableJammer(Coordinates, CallSign, Note, Role, IsOwn, AntennaAngle, CommunicationType, Id);
    }
}
