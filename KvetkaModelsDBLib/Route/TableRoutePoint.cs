﻿using System.Runtime.Serialization;
using System.ComponentModel;

namespace KvetkaModelsDBLib
{
    /// <summary>
    /// Таблица Точки маршрута
    /// </summary>
    [DataContract]
    [KnownType(typeof(AbstractCommonTable))]
    [InfoTable(NameTable.TableRoutePoint)]

    public class TableRoutePoint : AbstractCommonTable
    {
        [DataMember]
        public override int Id { get; set; }

        [DataMember]
        public short NumberPoint { get; set; }

        [DataMember]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Coord Coordinates { get; set; } = new Coord();

        [DataMember]
        public double SegmentLength { get; set; }

        [DataMember]
        public int RouteId { get; set; }

        public override object[] GetKey()
        {
            return new object[] { Id };
        }

        public TableRoutePoint() { }
        public TableRoutePoint(int routeId, short numberPoint, Coord coord, double segmentLength, int id = 0)
        {
            Id = id;
            NumberPoint = numberPoint;
            Coordinates = coord.Clone();
            SegmentLength = segmentLength;
            RouteId = routeId;
        }

        public override void Update(AbstractCommonTable record)
        {
            var newRecord = (TableRoutePoint)record;

            NumberPoint = newRecord.NumberPoint;
            Coordinates.Update(newRecord.Coordinates);
            SegmentLength = newRecord.SegmentLength;
        }

        public TableRoutePoint Clone() => new TableRoutePoint(RouteId, NumberPoint, Coordinates, SegmentLength, Id);
    }
}
