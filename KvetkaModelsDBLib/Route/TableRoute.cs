﻿using System;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;

namespace KvetkaModelsDBLib
{
    /// <summary>
    /// Таблица Маршруты
    /// </summary>
    [DataContract]
    [KnownType(typeof(AbstractCommonTable))]
    [InfoTable(NameTable.TableRoute)]

    public class TableRoute : AbstractCommonTable
    {
        [DataMember]
        public override int Id { get; set; }

        [DataMember]
        public short NumberRoute { get; set; }

        [DataMember]
        public string Name { get; set; } = string.Empty;

        [DataMember]
        public double DistanceOwn { get; set; }

        [DataMember]
        public DateTime Time { get; set; }

        [DataMember]
        public ObservableCollection<TableRoutePoint> ListPoints { get; set; } = new ObservableCollection<TableRoutePoint>();


        public override object[] GetKey()
        {
            return new object[] { Id };
        }

        public TableRoute() { }
        public TableRoute(short numberRoute, string name, double distanceOwn, DateTime time, ObservableCollection<TableRoutePoint> listPoints, int id = 0)
        {
            Id = id;
            NumberRoute = numberRoute;
            Name = name;
            DistanceOwn = distanceOwn;
            Time = time;
            ListPoints = new ObservableCollection<TableRoutePoint>(listPoints);
        }

        public override void Update(AbstractCommonTable record)
        {
            var newRecord = (TableRoute)record;

            NumberRoute = newRecord.NumberRoute;
            Name = newRecord.Name;
            DistanceOwn = newRecord.DistanceOwn;
            Time = newRecord.Time;

            ListPoints = ListPoints ?? newRecord.ListPoints;
            if (ListPoints != newRecord.ListPoints)
            {
                ListPoints.Clear();
                foreach (var subscriber in newRecord.ListPoints)
                    ListPoints.Add(subscriber);
            }
        }

        public TableRoute Clone() => new TableRoute(NumberRoute, Name, DistanceOwn, Time, ListPoints, Id);
    }
}
