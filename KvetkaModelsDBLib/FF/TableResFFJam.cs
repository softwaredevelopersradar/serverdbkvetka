﻿
using System;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace KvetkaModelsDBLib
{
    /// <summary>
    /// ИРИ ФРЧ РП
    /// </summary>
    [DataContract]
    [KnownType(typeof(AbstractDependentASP))]
    [InfoTable(NameTable.TableResFFJam)]

    public class TableResFFJam : AbstractDependentASP
    {
        [DataMember]
        public override int Id { get; set; }

        [DataMember]
        public override int NumberASP { get; set; }

        [DataMember]
        public bool IsActive { get; set; }

        [DataMember]
        public byte Sign { get; set; } 

        [DataMember]
        public double FrequencyKHz { get; set; }

        [DataMember]
        public float Bearing { get; set; } 

        [DataMember]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public InterferenceParameters InterferenceParameters { get; set; } = new InterferenceParameters();

        [DataMember]
        public short Threshold { get; set; }

        [DataMember]
        public byte Letter { get; set; }

        [DataMember]
        public byte Priority { get; set; }

        [DataMember]
        public Coord Coordinates { get; set; } = new Coord();

        //[DataMember]
        //public string Note { get; set; } = String.Empty;

        public override object[] GetKey()
        {
            return new object[] { Id };
        }

        public TableResFFJam() { }
        public TableResFFJam(int numberAsp, bool isActive, byte sign, double frequencyKHz, float bearing, InterferenceParameters interferenceParameters, short threshold, byte letter, byte priority, Coord coord, int id = 0)
        {
            NumberASP = numberAsp;
            IsActive = isActive;
            Sign = sign;
            FrequencyKHz = frequencyKHz;
            Bearing = bearing;
            InterferenceParameters = interferenceParameters.Clone();
            Threshold = threshold;
            Letter = letter;
            Priority = priority;
            Coordinates = coord.Clone();
            
            Id = id;
        }

        public override void Update(AbstractCommonTable record)
        {
            var newRecord = (TableResFFJam)record;

            IsActive = newRecord.IsActive;
            Sign = newRecord.Sign;
            FrequencyKHz = newRecord.FrequencyKHz;
            Bearing = newRecord.Bearing;
            InterferenceParameters.Update(newRecord.InterferenceParameters);
            Threshold = newRecord.Threshold;
            Letter = newRecord.Letter;
            Priority = newRecord.Priority;
            Coordinates.Update(newRecord.Coordinates);
            
        }

        public TableResFFJam Clone() => new TableResFFJam(NumberASP, IsActive, Sign, FrequencyKHz, Bearing, InterferenceParameters, Threshold, Letter, Priority, Coordinates, Id);
       
    }
}
