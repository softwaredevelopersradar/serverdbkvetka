﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel;

namespace KvetkaModelsDBLib
{
    /// <summary>
    /// Таблица ИРИ ФРЧ ЦР
    /// </summary>
    [DataContract]
    [KnownType(typeof(AbstractCommonTable))]
    [InfoTable(NameTable.TableResFFDistribution)]

    public class TableResFFDistribution : AbstractCommonTable
    {
        [DataMember]
        public override int Id { get; set; }

        [DataMember]
        public double FrequencyKHz { get; set; }

        [DataMember]
        public float BandKHz { get; set; }

        [DataMember]
        public float BearingOwn { get; set; }

        [DataMember]
        public float BearingLinked { get; set; }

        [DataMember]
        public int Level { get; set; }

        [DataMember]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Coord Coordinates { get; set; } = new Coord();

        [DataMember]
        public DateTime Time { get; set; }

        [DataMember]
        public float DistanceOwn { get; set; }

        [DataMember]
        public bool IsActive { get; set; }

        [DataMember]
        public bool IsRecording { get; set; }

        [DataMember]
        public string Demodulation { get; set; } = string.Empty;

        [DataMember]
        public double BandwidthKHz { get; set; }

        [DataMember]
        public int SampleRate { get; set; }

        [DataMember]
        public ModeSound Mode { get; set; }

        [DataMember]
        public double AgcLevel { get; set; }

        [DataMember]
        public double MgcCoefficient { get; set; }


        public override object[] GetKey()
        {
            return new object[] { Id };
        }

        public TableResFFDistribution() { }
        public TableResFFDistribution(double frequencyKHz, float bandKHz, float bearingOwn, float bearingLinked, float distanceOwn, Coord coord, int level, DateTime time, bool isActive, 
            bool isRecording, string demodulation, double bandwidthKHz, int sampleRate, ModeSound mode, double agcLevel, double mgcCoefficient, int id = 0)
        {
            Id = id;
            FrequencyKHz = frequencyKHz;
            BandKHz = bandKHz;
            BearingOwn = bearingOwn;
            BearingLinked = bearingLinked;
            DistanceOwn = distanceOwn;
            Level = level;
            Coordinates = coord.Clone();
            Time = time;
            IsActive = isActive;
            IsRecording = isRecording;
            Demodulation = demodulation;
            BandwidthKHz = bandwidthKHz;
            SampleRate = sampleRate;
            Mode = mode;
            AgcLevel = agcLevel;
            MgcCoefficient = mgcCoefficient;
        }

        public override void Update(AbstractCommonTable record)
        {
            var newRecord = (TableResFFDistribution)record;

            FrequencyKHz = newRecord.FrequencyKHz;
            BandKHz = newRecord.BandKHz;
            BearingOwn = newRecord.BearingOwn;
            BearingLinked = newRecord.BearingLinked;
            DistanceOwn = newRecord.DistanceOwn;
            Level = newRecord.Level;
            Coordinates.Update(newRecord.Coordinates);
            Time = newRecord.Time;
            IsActive = newRecord.IsActive;
            IsRecording = newRecord.IsRecording;
            Demodulation = newRecord.Demodulation;
            BandwidthKHz = newRecord.BandwidthKHz;
            SampleRate = newRecord.SampleRate;
            Mode = newRecord.Mode;
            AgcLevel = newRecord.AgcLevel;
            MgcCoefficient = newRecord.MgcCoefficient;
        }
        public TableResFFDistribution Clone() => new TableResFFDistribution(FrequencyKHz, BandKHz, BearingOwn, BearingLinked, DistanceOwn, Coordinates, Level, Time, IsActive, IsRecording,
            Demodulation, BandwidthKHz, SampleRate, Mode, AgcLevel, MgcCoefficient,Id);

    }
}
