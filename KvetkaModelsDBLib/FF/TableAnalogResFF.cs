﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace KvetkaModelsDBLib
{
    /// <summary>
    /// Таблица ИРИ ФРЧ аналоговые сигналы
    /// </summary>
    [DataContract]
    [KnownType(typeof(AbstractHasFFTable))]
    [KnownType(typeof(TableResFF))]
    [InfoTable(NameTable.TableAnalogResFF)]
    public class TableAnalogResFF : AbstractHasFFTable
    {
        [DataMember]
        public override int Id { get; set; }

        [DataMember]
        public double FrequencyKHz { get; set; }

        [DataMember]
        public float BandKHz { get; set; }

        [DataMember]
        public float BearingOwn { get; set; }

        [DataMember]
        public int Level { get; set; }

        [DataMember]
        public string TypeSignal { get; set; }

        [DataMember]
        public int BandDemodulation { get; set; }

        [DataMember]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Coord Coordinates { get; set; } = new Coord();

        [DataMember]
        public DateTime Time { get; set; }
        
        [DataMember]
        public bool IsActive { get; set; }

        [DataMember]
        public override TableResFF ResFF { get; set; }

        [DataMember]
        public double BandAudio { get; set; }

        public override object[] GetKey()
        {
            return new object[] { Id };
        }

        public TableAnalogResFF() { }
        public TableAnalogResFF(double frequencyKHz, float bandKHz, float bearingOwn, int level, string signalType, int bandDemodulation, Coord coord, DateTime time, bool isActive, TableResFF resFF, double bandAudio, int id = 0)
        {
            Id = id;
            FrequencyKHz = frequencyKHz;
            BandKHz = bandKHz;
            BearingOwn = bearingOwn;
            Level = level;
            TypeSignal = signalType;
            BandDemodulation = bandDemodulation;
            Coordinates = coord?.Clone();
            Time = time;
            IsActive = isActive;
            ResFF = resFF?.Clone();
            BandAudio = bandAudio;
        }

        public override void Update(AbstractCommonTable record)
        {
            var newRecord = (TableAnalogResFF)record;

            FrequencyKHz = newRecord.FrequencyKHz;
            BandKHz = newRecord.BandKHz;
            BearingOwn = newRecord.BearingOwn;
            Level = newRecord.Level;
            TypeSignal = newRecord.TypeSignal;
            BandDemodulation = newRecord.BandDemodulation;
            Coordinates.Update(newRecord.Coordinates);
            Time = newRecord.Time;
            IsActive = newRecord.IsActive;
            BandAudio = newRecord.BandAudio;
            if (ResFF == null || newRecord.ResFF == null)
            { ResFF = newRecord.ResFF; }
            else
            { ResFF.Update(newRecord.ResFF); }
        }

        public TableAnalogResFF Clone() => new TableAnalogResFF(FrequencyKHz, BandKHz, BearingOwn, Level, TypeSignal, BandDemodulation, Coordinates, Time, IsActive, ResFF, BandAudio, Id);
    }
}
