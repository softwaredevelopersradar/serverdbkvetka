﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace KvetkaModelsDBLib
{
    /// <summary>
    /// Таблица ИРИ ФРЧ цифровые сигналы
    /// </summary>
    [DataContract]
    [KnownType(typeof(AbstractHasFFTable))]
    [KnownType(typeof(TableResFF))]
    [InfoTable(NameTable.TableDigitalResFF)]
    public class TableDigitalResFF : AbstractHasFFTable
    {
        [DataMember]
        public override int Id { get; set; }

        [DataMember]
        public DateTime TimeBegin { get; set; }

        [DataMember]
        public DateTime TimeEnd { get; set; }

        [DataMember]
        public double FrequencyKHz { get; set; }

        [DataMember]
        public DigitalSignalType SignalClass { get; set; }

        [DataMember]
        public float BearingOwn { get; set; }

        [DataMember]
        public string SubscriberInitiator { get; set; } = String.Empty;

        [DataMember]
        public string SubscriberRecipient { get; set; } = String.Empty;

        [DataMember]
        public string RadioNetwork { get; set; } = String.Empty;

        [DataMember]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Coord Coordinates { get; set; } = new Coord();

        [DataMember]
        public bool IsActive { get; set; }

        [DataMember]
        public bool HasMetadata { get; set; }

        [DataMember]
        public override TableResFF ResFF { get; set; }

        [DataMember]
        public VereskState VereskState { get; set; }

        [DataMember]
        public uint VereskDbSessionId { get; set; }


        public override object[] GetKey()
        {
            return new object[] { Id };
        }

        public TableDigitalResFF() { }
        public TableDigitalResFF(DateTime timeBegin, DateTime timeEnd, double frequencyKHz, DigitalSignalType signalType, float bearingOwn, string subscriberInitiator, string subscriberRecipient, string radionetwork, Coord coord, bool isActive, bool hasMetadata, TableResFF resFF, VereskState vereskState, uint vereskDbSessionId, int id = 0)
        {
            Id = id;
            TimeBegin = timeBegin;
            TimeEnd = timeEnd;
            FrequencyKHz = frequencyKHz;
            SignalClass = signalType;
            BearingOwn = bearingOwn;
            SubscriberInitiator = subscriberInitiator;
            SubscriberRecipient = subscriberRecipient;
            RadioNetwork = radionetwork;
            Coordinates = coord.Clone();
            IsActive = isActive;
            HasMetadata = hasMetadata;
            ResFF = resFF?.Clone();
            VereskState = vereskState;
            VereskDbSessionId= vereskDbSessionId;
        }

        public override void Update(AbstractCommonTable record)
        {
            var newRecord = (TableDigitalResFF)record;

            TimeBegin = newRecord.TimeBegin;
            TimeEnd = newRecord.TimeEnd;
            FrequencyKHz = newRecord.FrequencyKHz;
            SignalClass = newRecord.SignalClass;
            BearingOwn = newRecord.BearingOwn;
            Coordinates.Update(newRecord.Coordinates);
            SubscriberInitiator = newRecord.SubscriberInitiator;
            SubscriberRecipient = newRecord.SubscriberRecipient;
            RadioNetwork = newRecord.RadioNetwork;
            IsActive = newRecord.IsActive;
            HasMetadata = newRecord.HasMetadata;
            if (ResFF == null || newRecord.ResFF == null)
            { ResFF = newRecord.ResFF; }
            else
            { ResFF.Update(newRecord.ResFF); }
            VereskState= newRecord.VereskState;
            VereskDbSessionId= newRecord.VereskDbSessionId;
        }

        public TableDigitalResFF Clone() => new TableDigitalResFF(TimeBegin, TimeEnd, FrequencyKHz, SignalClass, BearingOwn, SubscriberInitiator, SubscriberRecipient, RadioNetwork, Coordinates, IsActive, HasMetadata, ResFF, VereskState, VereskDbSessionId, Id);

    }
}
