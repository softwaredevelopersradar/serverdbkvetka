﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel;

namespace KvetkaModelsDBLib
{
    /// <summary>
    /// Таблица ИРИ ФРЧ
    /// </summary>
    [DataContract]
    //[KnownType(typeof(TableTrackResFF))]
    [KnownType(typeof(AbstractCommonTable))]
    [InfoTable(NameTable.TableResFF)]

    public class TableResFF : AbstractCommonTable
    {
        [DataMember]
        public override int Id { get; set; }

        [DataMember]
        public Led Control { get; set; }

        [DataMember]
        public SignalFlag SignalFlag { get; set; }

        [DataMember]
        public double FrequencyKHz { get; set; }       

        //[DataMember]
        //public float BandKHz { get; set; }

        [DataMember]
        public float BearingOwn { get; set; }

        [DataMember]
        public float BearingLinked { get; set; }

        [DataMember]
        public float DistanceOwn { get; set; }

        //[DataMember]
        //public byte Modulation { get; set; }

        //[DataMember]
        //public short ManipulationVelocity { get; set; } 

        [DataMember]
        public int Level { get; set; }

        //[DataMember]
        //public byte Type { get; set; }

        //[DataMember]
        //public byte Priority { get; set; }

        [DataMember]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Coord Coordinates { get; set; } = new Coord();

        [DataMember]
        public DateTime Time { get; set; }

        //[DataMember]
        //public byte RadioNetwork { get; set; }

        [DataMember]
        public bool IsActive { get; set; }

        [DataMember]
        public int? TableAnalogResFFId { get; set; }

        [DataMember]
        public int? TableDigitalResFFId { get; set; }

        [DataMember]
        public short Quality { get; set; }
        //[DataMember]
        //public string Note { get; set; } = String.Empty;

        //[DataMember]
        //public ObservableCollection<TableTrackResFF> ListTrack { get; set; } = new ObservableCollection<TableTrackResFF>();


        public override object[] GetKey()
        {
            return new object[] { Id };
        }

        public TableResFF() { }
        public TableResFF(Led control, SignalFlag signalFlag, double frequencyKHz, float bearingOwn, float bearingLinked, float distanceOwn, Coord coord, int level, DateTime time, bool isActive, int? tableAnalogResFFId, int? tableDigitalResFFId, short quality, int id = 0)
        {
            Id = id;
            Control = control;
            SignalFlag = signalFlag;
            FrequencyKHz  = frequencyKHz;
            //BandKHz = bandKHz;
            BearingOwn = bearingOwn;
            BearingLinked = bearingLinked;
            DistanceOwn = distanceOwn;
            Level = level;
            //Type = type;
            //Priority = priority;
            Coordinates = coord.Clone();
            Time  = time;
            //RadioNetwork = radionetwork;
            IsActive = isActive;
            TableAnalogResFFId = tableAnalogResFFId;
            TableDigitalResFFId = tableDigitalResFFId;
            Quality = quality;
        }

        public override void Update(AbstractCommonTable record)
        {
            var newRecord = (TableResFF)record;

            Control = newRecord.Control;
            SignalFlag = newRecord.SignalFlag;
            FrequencyKHz = newRecord.FrequencyKHz;
            //BandKHz = newRecord.BandKHz;
            BearingOwn = newRecord.BearingOwn;
            BearingLinked = newRecord.BearingLinked;
            DistanceOwn = newRecord.DistanceOwn;
            Level = newRecord.Level;
            //Type = newRecord.Type;
            //Priority = newRecord.Priority;
            Coordinates.Update(newRecord.Coordinates);
            Time = newRecord.Time;
            //RadioNetwork = newRecord.RadioNetwork;
            IsActive = newRecord.IsActive;
            //TableAnalogResFFId = newRecord.TableAnalogResFFId;
            //TableDigitalResFFId = newRecord.TableDigitalResFFId;
            Quality = newRecord.Quality;
        }
        public TableResFF Clone() => new TableResFF(Control, SignalFlag, FrequencyKHz, BearingOwn, BearingLinked, DistanceOwn, Coordinates, Level, Time, IsActive, TableAnalogResFFId, TableDigitalResFFId, Quality, Id);
        //public TableResFF Clone() => new TableResFF(Control, SignalFlag, FrequencyKHz, BearingOwn, BearingLinked, DistanceOwn, Coordinates, Level, Time, IsActive, TableAnalogResFFId, Id);
    }
}
