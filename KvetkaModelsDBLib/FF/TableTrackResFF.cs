﻿using System.Runtime.Serialization;
using System.ComponentModel;

namespace KvetkaModelsDBLib
{
    /// <summary>
    /// Таблица Точки траектории ИРИ ФРЧ
    /// </summary>
    [DataContract]
    [KnownType(typeof(AbstractCommonTable))]
    [InfoTable(NameTable.TableTrackResFF)]
    public class TableTrackResFF : AbstractCommonTable
    {
        [DataMember]
        public override int Id { get; set; }

        [DataMember]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Coord Coordinates { get; set; } = new Coord();

        [DataMember]
        public float BearingOwn { get; set; }

        [DataMember]
        public float BearingLinked { get; set; }

        [DataMember]
        public float DistanceOwn { get; set; }

        [DataMember]
        public float DistanceLinked { get; set; }

        [DataMember]
        public int TableResFFId { get; set; }

        public override object[] GetKey()
        {
            return new object[] { Id };
        }

        public TableTrackResFF() { }
        public TableTrackResFF(int tableResFFId, Coord coord, float bearingOwn, float bearingLinked, float distanceOwn, float distanceLinked, int id = 0)
        {
            TableResFFId = tableResFFId;
            Coordinates = new Coord(coord.Latitude, coord.Longitude, coord.Altitude);
            BearingOwn = bearingOwn;
            BearingLinked = bearingLinked;
            DistanceOwn = distanceOwn;
            DistanceLinked = distanceLinked;
            Id = id;
        }

        public override void Update(AbstractCommonTable record)
        {
            var newRecord = (TableTrackResFF)record;

            Coordinates.Update(newRecord.Coordinates);
            BearingOwn = newRecord.BearingOwn;
            BearingLinked = newRecord.BearingLinked;
            DistanceOwn = newRecord.DistanceOwn;
            DistanceLinked = newRecord.DistanceLinked;
        }

        public TableTrackResFF Clone() => new TableTrackResFF(TableResFFId, Coordinates, BearingOwn, BearingLinked, DistanceOwn, DistanceLinked, Id);
    }
}
