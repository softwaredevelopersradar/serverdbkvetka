﻿using System.Runtime.Serialization;

namespace KvetkaModelsDBLib
{
    [DataContract]
    [KnownType(typeof(AbstractCommonTable))]
    public abstract class AbstractDependentASP : AbstractCommonTable
    {
        [DataMember]
        public abstract int NumberASP { get; set; }
    }
}
