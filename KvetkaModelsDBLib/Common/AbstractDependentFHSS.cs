﻿using System.Runtime.Serialization;

namespace KvetkaModelsDBLib
{
    [DataContract]
    [KnownType(typeof(AbstractCommonTable))]
    public abstract class AbstractDependentFHSS : AbstractCommonTable
    {
        [DataMember]
        public abstract int FhssId { get; set; }
    }
}
