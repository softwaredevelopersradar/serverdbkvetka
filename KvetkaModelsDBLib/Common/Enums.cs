﻿using System.ComponentModel;
using System.Runtime.Serialization;

namespace KvetkaModelsDBLib
{
    [DataContract]
    public enum NameChangeOperation : byte
    {
        [EnumMember]
        Add,      // Добавить запись 
        [EnumMember]
        Delete,   // Удалить запись
        [EnumMember]
        Change   // Изменить запись
    }

    [DataContract]
    public enum NameTableOperation
    {
        [EnumMember]
        Add,
        [EnumMember]
        AddRange,
        [EnumMember]
        RemoveRange,
        [EnumMember]
        Change,
        [EnumMember]
        ChangeRange,
        [EnumMember]
        Delete,
        [EnumMember]
        Clear,
        [EnumMember]
        Load,
        [EnumMember]
        LoadByFilterAsp,
        [EnumMember]
        ClearByFilter,
        [EnumMember]
        Update,
        [EnumMember]
        None
    }

    [DataContract]
    public enum NameTable : byte
    {
        [EnumMember]
        [Description("Станции помех")]
        TableJammer,

        [EnumMember]
        [Description("Диапазоны РР")]
        TableFreqRangesElint,

        [EnumMember]
        [Description("Диапазоны РП")]
        TableFreqRangesJamming,

        [EnumMember]
        [Description("Известные частоты")]
        TableFreqKnown,

        [EnumMember]
        [Description("Важные частоты")]
        TableFreqImportant,

        [EnumMember]
        [Description("Запрещенные частоты")]
        TableFreqForbidden,

        [EnumMember]
        [Description("Сектора РР")]
        TableSectorsElint,

        [EnumMember]
        [Description("Самолеты ADSB")]
        TableADSB,

        [EnumMember]
        [Description("ИРИ ФРЧ")]
        TableResFF,

        [EnumMember]
        [Description("Траектория ИРИ ФРЧ")]
        TableTrackResFF,

        ////[EnumMember]
        ////[Description("Пеленг ИРИ")]
        ////TableJamBearing,


        [EnumMember]
        [Description("ИРИ ФРЧ РП")]
        TableResFFJam,

        [EnumMember]
        [Description("ИРИ ППРЧ")]
        TableResFHSS,

        [EnumMember]
        [Description("Абоненты ИРИ ППРЧ")]
        TableSubscriberResFHSS,

        [EnumMember]
        [Description("Траектория абонентов ИРИ ППРЧ")]
        TableTrackSubscriberResFHSS,

        [EnumMember]
        [Description("ИРИ ППРЧ РП")]
        TableResFHSSJam,

        [EnumMember]
        [Description("Глобальные настройки")]
        GlobalProperties,

        [EnumMember]
        [Description("TempSuppressFF")]
        TempSuppressFF,

        [EnumMember]
        [Description("TempSuppressFHSS")]
        TempSuppressFHSS,

        [EnumMember]
        [Description("TableDigitalResFF")]
        TableDigitalResFF,

        [EnumMember]
        [Description("TableAnalogResFF")]
        TableAnalogResFF,

        [EnumMember]
        [Description("ИРИ ФРЧ ЦР")]
        TableResFFDistribution,

        [EnumMember]
        [Description("Маршруты")]
        TableRoute,

        [EnumMember]
        [Description("Точки маршрутов")]
        TableRoutePoint,

        [EnumMember]
        [Description("ППРЧ РП исключенные")]
        TableFHSSExcludedJam
    }

    [DataContract]
    public enum StationRole : byte
    {
        [EnumMember]
        Complex,
        [EnumMember]
        Own,
        [EnumMember]
        Linked
    }

    [DataContract]
    public enum Led : byte
    {
        [EnumMember]
        Empty,
        [EnumMember]
        Green,
        [EnumMember]
        Red,
        [EnumMember]
        Blue,
        [EnumMember]
        Yellow,
        [EnumMember]
        Gray
    }

    [DataContract]
    public enum CommunicationType : byte
    {
        [EnumMember]
        RadioModem,
        [EnumMember]
        Modem4Wire,
        [EnumMember]
        Router3G_4G
    }

    [DataContract]
    public enum DigitalSignalType : short
    {
        /// <summary>
        /// Любой обнаружитель
        /// </summary>
        ScAny = 0,
        /// <summary>
        /// ALE 2G
        /// </summary>
        ScAle2G = 938,
        /// <summary>
        ///++ ЧТ-2
        /// </summary>
        ScFsk2 = 74,
        /// <summary>
        /// ALE 3G
        /// </summary>
        ScAle3G = 932,
        /// <summary>
        /// STANAG 4285
        /// </summary>
        ScSt4285 = 939,
        /// <summary>
        /// STANAG 4529
        /// </summary>
        ScSt4529 = 937,
        /// <summary>
        /// MILSTD 188-110A
        /// </summary>
        ScMil110A = 946,
        /// <summary>
        /// MILSTD 188-110B(AppC)
        /// </summary>
        ScMil110BAppc = 933,
        /// <summary>
        /// MILSTD 188-110C(AppD) (обнаружение только в полосе 3 кГц)
        /// </summary>
        ScMil110CAppd = 1576,
        /// <summary>
        /// ASELSAN (только обнаружение, обработка не реализована)
        /// </summary>
        ScAselsan = 1547,
        /// <summary>
        /// DAIMLER&amp;CHRYSLER 7200
        /// </summary>
        ScDc7200 = 910,
        /// <summary>
        /// LINK11 (не обнаруживается непосредственно в реальном времени, классификация происходит только после обработки сигнала)
        /// </summary>
        ScLink11 = 916,
        /// <summary>
        /// Rohde &amp; Schwarz (не обнаруживается непосредственно в реальном времени, классификация происходит только после обработки сигнала)
        /// </summary>
        ScRs = 926,
        /// <summary>
        /// M39 (MILSTD 188-110B(AppB)) (не обнаруживается, обработка автоматизированная с участием оператора)
        /// </summary>
        ScM39 = 931,
        /// <summary>
        /// ИЗРАИЛЬ ВМС (не обнаруживается непосредственно в реальном времени, классификация происходит только после демодуляции сигнала, полноценная обработка не реализована)
        /// </summary>
        ScIsraelNavy = 1490,
        /// <summary>
        /// MARCONI OFDM (не обнаруживается и не обрабатывается)
        /// </summary>
        ScMarconiOfdm = 917,
        /// <summary>
        /// LINK22 (только обнаружение, обработка не реализована)
        /// </summary>
        ScLink22 = 1536,
        /// <summary>
        /// GMDSS (отдельного обнаружителя нет, обнаруживается как ЧТ-2 сигнал и обрабатывается)
        /// </summary>
        ScGmdss = 1567,
        /// <summary>
        /// NAVTEX (отдельного обнаружителя нет, обнаруживается как ЧТ-2 сигнал и обрабатывается)
        /// </summary>
        ScNavtex = 1479,
        /// <summary>
        /// HFDL (КВ-диапазон)
        /// </summary>
        ScHfdl = 914,
        /// <summary>
        /// RFSM (обнаруживается как 110A, классификация происходит только после обработки сигнала)
        /// </summary>
        ScRfsm = 1559,
        /// <summary>
        /// CLOVER2000 (не обнаруживается, обработка автоматизированная с участием оператора)
        /// </summary>
        ScClover = 936,
        /// <summary>
        /// CODAN (не обнаруживается, обработка автоматизированная с участием оператора)
        /// </summary>
        ScCodan = 934,
        /// <summary>
        /// Б-162 (Китай ВВС)
        /// </summary>
        ScB162 = 1477,
        /// <summary>
        /// PACTOR I (не обнаруживается, автоматизированная обработка до конца не реализована)
        /// </summary>
        ScPactor1 = 922,
        /// <summary>
        /// PACTOR II (не обнаруживается, автоматизированная обработка до конца не реализована)
        /// </summary>
        ScPactor2 = 923,
        /// <summary>
        /// PACTOR III (не обнаруживается, автоматизированная обработка до конца не реализована)
        /// </summary>
        ScPactor3 = 924,
        /// <summary>
        /// PACTOR IV (не обнаруживается, автоматизированная обработка до конца не реализована)
        /// </summary>
        ScPactor4 = 1538,
        /// <summary>
        /// ANDVT (обнаруживается, не обрабатывается)
        /// </summary>
        ScAndvt = 30100,
        /// <summary>
        /// LINK11(CLEW (обнаруживается, не обрабатывается)
        /// </summary>
        ScLink11Clew = 30101,
    }

    [DataContract]
    public enum AnalogSignalType : byte
    {
        [EnumMember]
        AM,
        [EnumMember]
        FM,
        [EnumMember]
        USB,
        [EnumMember]
        LSB
    }

    [DataContract]
    public enum SignalFlag : byte
    {
        [EnumMember]
        Unknown,
        [EnumMember]
        Analog,
        [EnumMember]
        Digital
    }

    [DataContract]
    public enum FftResolution : byte
    {
        N32768 = 2,
        N16384 = 3,
        N8192 = 4,
        N4096 = 5
    }

    [DataContract]
    public enum PowerPercent : byte
    {
        N100 = 0,
        N50 = 1,
        N25 = 2,
    }

    [DataContract]
    public enum VereskState : byte
    {
        [EnumMember]
        OpenedSession = 1,
        [EnumMember]
        ClosedSession = 2,
        [EnumMember]
        DetectedSignal = 3
    }

    [DataContract]
    public enum ModeSound : byte
    {
        [EnumMember]
        Auto = 0,
        [EnumMember]
        Manual = 1
    }
}
