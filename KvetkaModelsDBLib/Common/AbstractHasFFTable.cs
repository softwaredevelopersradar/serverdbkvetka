﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace KvetkaModelsDBLib
{
    [DataContract]
    [KnownType(typeof(AbstractCommonTable))]
    public abstract class AbstractHasFFTable : AbstractCommonTable
    {
        [DataMember]
        public abstract TableResFF ResFF { get; set; }
    }
}
