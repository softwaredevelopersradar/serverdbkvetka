﻿using System.Runtime.Serialization;

namespace KvetkaModelsDBLib
{
    [DataContract]
    public class InterferenceParameters
    {
        [DataMember]
        public byte Modulation { get; set; }

        [DataMember]
        public byte Deviation { get; set; }

        [DataMember]
        public byte Manipulation { get; set; }

        [DataMember]
        public byte Duration { get; set; }

        public InterferenceParameters() { }
        public InterferenceParameters(byte modulation, byte deviation, byte manipulation, byte duration)
        {
            Modulation = modulation;
            Deviation = deviation;
            Manipulation = manipulation;
            Duration = duration;
        }

        public void Update(InterferenceParameters newRecord)
        {
            Modulation = newRecord.Modulation;
            Deviation = newRecord.Deviation;
            Manipulation = newRecord.Manipulation;
            Duration = newRecord.Duration;
        }

        public InterferenceParameters Clone() => new InterferenceParameters(Modulation, Deviation, Manipulation, Duration);
    }
}
