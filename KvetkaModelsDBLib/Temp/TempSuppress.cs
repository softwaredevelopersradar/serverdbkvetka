﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;

namespace KvetkaModelsDBLib
{
    /// <summary>
    /// Temp suppress
    /// </summary>
    [DataContract]
    [KnownType(typeof(AbstractDependentASP))]
    [KnownType(typeof(TempSuppressFF))]
    [KnownType(typeof(TempSuppressFHSS))]
    public class TempSuppress : AbstractDependentASP, INotifyPropertyChanged
    {
        private Led control;
        private Led suppress;
        private Led radiation;


        [DataMember]
        public override int Id { get; set; }

        [DataMember]
        public override int NumberASP { get; set; }

        [DataMember]
        public Led Control
        {
            get => control;
            set
            {
                if (control == value) return;
                control = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        public Led Suppress
        {
            get => suppress;
            set
            {
                if (suppress == value) return;
                suppress = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        public Led Radiation
        {
            get => radiation;
            set
            {
                if (radiation == value) return;
                radiation = value;
                OnPropertyChanged();
            }
        }

        public override object[] GetKey()
        {
            return new object[] { Id };
        }

        public TempSuppress() { }
        public TempSuppress(int numberASP, Led control, Led suppress, Led radiation, int id = 0)
        {
            NumberASP = numberASP;
            Control = control;
            Suppress = suppress;
            Radiation = radiation;
            Id = id;
        }


        public TempSuppress Clone()
        {
            return new TempSuppress(NumberASP, Control, Suppress, Radiation, Id);
        }

        public override void Update(AbstractCommonTable record)
        {
            Control = ((TempSuppress)record).Control;
            Radiation = ((TempSuppress)record).Radiation;
            Suppress = ((TempSuppress)record).Suppress;
        }

        public TempSuppressFF ToTempSuppressFF()
        {
            TempSuppressFF table = new TempSuppressFF()
            {
                Id = Id,
                NumberASP = NumberASP,
                Control = Control,
                Radiation = Radiation,
                Suppress = Suppress
            };
            return table;
        }

        public TempSuppressFHSS ToTempSuppressFHSS()
        {
            TempSuppressFHSS table = new TempSuppressFHSS()
            {
                Id = Id,
                NumberASP = NumberASP,
                Control = Control,
                Radiation = Radiation,
                Suppress = Suppress
            };
            return table;
        }


        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion
    }
}
