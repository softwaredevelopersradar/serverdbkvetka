﻿using System;
using System.Runtime.Serialization;

namespace KvetkaModelsDBLib
{
    /// <summary>
     /// ADSB
     /// </summary>
    [DataContract]
    [KnownType(typeof(AbstractCommonTable))]
    [InfoTable(NameTable.TableADSB)]
    public class TableADSB : AbstractCommonTable
    {
        [DataMember]
        public override int Id { get; set; }

        [DataMember]
        public string ICAO { get; set; } = String.Empty;

        [DataMember]
        public Coord Coordinates { get; set; } = new Coord();

        [DataMember]
        public DateTime Time { get; set; }

        [DataMember]
        public byte Type { get; set; }

        [DataMember]
        public string Country { get; set; } = String.Empty;

        public override object[] GetKey()
        {
            return new object[] { Id };
        }

        public TableADSB() { }
        public TableADSB(string icao, Coord coord, DateTime time, byte type, string country, int id = 0)
        {
            ICAO = icao;
            Coordinates = new Coord(coord.Latitude, coord.Longitude, coord.Altitude);
            Time = time;
            Type = type;
            Country = country;
            Id = id;
        }


        public TableADSB Clone()
        {
            return new TableADSB(ICAO, Coordinates, Time, Type, Country, Id);
        }

        public override void Update(AbstractCommonTable record)
        {
            var newRecord = (TableADSB)record;

            Coordinates.Update(newRecord.Coordinates);
            ICAO = newRecord.ICAO;
            Type = newRecord.Type;
            Time = newRecord.Time;
            Country = newRecord.Country;
        }
    }
}
