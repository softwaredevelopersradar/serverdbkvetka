﻿using System.Runtime.Serialization;

namespace KvetkaModelsDBLib
{
    /// <summary>
    /// Временная ППРЧ подавление
    /// </summary>
    [DataContract]
    [KnownType(typeof(TempSuppress))]
    [InfoTable(NameTable.TempSuppressFHSS)]
    public class TempSuppressFHSS : TempSuppress
    {
    }
}
