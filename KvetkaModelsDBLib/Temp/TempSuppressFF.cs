﻿using System.Runtime.Serialization;

namespace KvetkaModelsDBLib
{    
    /// <summary>
    /// Временная ФРЧ подавление
    /// </summary>
    [DataContract]
    [KnownType(typeof(TempSuppress))]
    [InfoTable(NameTable.TempSuppressFF)]
    public class TempSuppressFF : TempSuppress
    {
    }
}
