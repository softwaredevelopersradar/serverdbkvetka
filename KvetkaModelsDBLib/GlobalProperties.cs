﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;

namespace KvetkaModelsDBLib
{
    [DataContract]
    [KnownType(typeof(AbstractCommonTable))]
    [InfoTable(NameTable.GlobalProperties)]
    public class GlobalProperties : AbstractCommonTable
    {
        [DataMember]
        [Browsable(false)]
        public override int Id { get; set; }


        #region Gnss
        private Coord _gnss = new Coord();

        [DataMember]
        [NotifyParentProperty(true)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Coord Gnss
        {
            get { return _gnss; }
            set
            {
                if (_gnss == value)
                    return;
                _gnss = value;
                OnPropertyChanged();
            }
        }
        #endregion


        #region General
       
        private int _timeJamming = 8;
        
        [DataMember]
        [NotifyParentProperty(true)]
        public int TimeJamming
        {
            get => _timeJamming;
            set
            {
                if (_timeJamming == value) return;
                _timeJamming = value;
                OnPropertyChanged();
            }
        }

        #endregion


        #region RI
        private int _numberOfPhasesAveragings = 1;
        private int _numberOfBearingAveragings = 1;
        private int _courseAngle = -1;
        private int _adaptiveThresholdFhss = 0;





        [DataMember]
        [NotifyParentProperty(true)]
        public int NumberOfPhasesAveragings
        {
            get => _numberOfPhasesAveragings;
            set
            {
                if (_numberOfPhasesAveragings == value) return;
                _numberOfPhasesAveragings = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public int NumberOfBearingAveragings
        {
            get => _numberOfBearingAveragings;
            set
            {
                if (_numberOfBearingAveragings == value) return;
                _numberOfBearingAveragings = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public int CourseAngle
        {
            get => _courseAngle;
            set
            {
                if (_courseAngle == value) return;
                _courseAngle = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public int AdaptiveThresholdFhss
        {
            get => _adaptiveThresholdFhss;
            set
            {
                if (_adaptiveThresholdFhss == value) return;
                _adaptiveThresholdFhss = value;
                OnPropertyChanged();
            }
        }
        #endregion


        #region FFJamming
        private int _numberOfResInLetter = 4;
        private int _numberOfChannelsInLetter = 2;
        private int _longWorkingSignalDurationMs = 1;
        private byte _priority = 1;
        private int _threshold = 0;
        private int _timeRadiationFF = 500;
        private bool _isTest = false;

        [DataMember]
        [NotifyParentProperty(true)]
        public int NumberOfResInLetter
        {
            get => _numberOfResInLetter;
            set
            {
                if (_numberOfResInLetter == value) return;
                _numberOfResInLetter = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public int NumberOfChannelsInLetter
        {
            get => _numberOfChannelsInLetter;
            set
            {
                if (_numberOfChannelsInLetter == value) return;
                _numberOfChannelsInLetter = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public int LongWorkingSignalDurationMs
        {
            get => _longWorkingSignalDurationMs;
            set
            {
                if (_longWorkingSignalDurationMs == value) return;
                _longWorkingSignalDurationMs = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public byte Priority
        {
            get => _priority;
            set
            {
                if (_priority == value) return;
                _priority = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public int Threshold
        {
            get => _threshold;
            set
            {
                if (_threshold == value) return;
                _threshold = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public int TimeRadiationFF
        {
            get => _timeRadiationFF;
            set
            {
                if (_timeRadiationFF == value) return;
                _timeRadiationFF = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public bool IsTest
        {
            get => _isTest;
            set
            {
                if (_isTest == value) return;
                _isTest = value;
                OnPropertyChanged();
            }
        }
        #endregion


        #region FHSSJamming
        private FftResolution _fhssFftResolution = FftResolution.N16384;
        private int _timeRadiationFHSS = 300;

        [DataMember]
        [NotifyParentProperty(true)]
        public FftResolution FhssFftResolution
        {
            get => _fhssFftResolution;
            set
            {
                if (_fhssFftResolution == value) return;
                _fhssFftResolution = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public int TimeRadiationFHSS
        {
            get => _timeRadiationFHSS;
            set
            {
                if (_timeRadiationFHSS == value) return;
                _timeRadiationFHSS = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region AFHJamming
        private double _searchSector = 2;
        private int _searchTime = 1;

        [DataMember]
        [NotifyParentProperty(true)]
        public double SearchSector
        {
            get => _searchSector;
            set
            {
                if (_searchSector == value) return;
                _searchSector = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public int SearchTime
        {
            get => _searchTime;
            set
            {
                if (_searchTime == value) return;
                _searchTime = value;
                OnPropertyChanged();
            }
        }

        #endregion


        #region AM
        private int _jsgAmPollInterval = 20;
        private PowerPercent _jsgAmPower = PowerPercent.N25;

        [DataMember]
        [NotifyParentProperty(true)]
        public int JsgAmPollInterval
        {
            get => _jsgAmPollInterval;
            set
            {
                if (_jsgAmPollInterval == value) return;
                _jsgAmPollInterval = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public PowerPercent JsgAmPower
        {
            get => _jsgAmPower;
            set
            {
                if (_jsgAmPower == value) return;
                _jsgAmPower = value;
                OnPropertyChanged();
            }
        }
        
        #endregion



        #region Methods
        public override object[] GetKey()
        {
            return new object[] { Id };
        }

        public GlobalProperties()
        {}

        public GlobalProperties(Coord gnss, int timeJamming,
            int numberOfPhasesAveragings, int numberOfBearingAveragings, int courseAngle, int adaptiveThresholdFhss,
            int numberOfResInLetter, int numberOfChannelsInLetter, int longWorkingSignalDurationMs, byte priority, int threshold, int timeRadiationFF, bool isTest,
            FftResolution fhssFftResolution, int timeRadiationFHSS, 
            double searchSector, int searchTime, 
            int jsgAmPollInterval, PowerPercent jsgAmPower, int id = 0)
        {
            Id = id;

            Gnss = gnss;

            TimeJamming = timeJamming;

            NumberOfPhasesAveragings = numberOfPhasesAveragings;
            NumberOfBearingAveragings = numberOfBearingAveragings;
            CourseAngle = courseAngle;
            AdaptiveThresholdFhss = adaptiveThresholdFhss;

            NumberOfResInLetter = numberOfResInLetter;
            NumberOfChannelsInLetter = numberOfChannelsInLetter;
            LongWorkingSignalDurationMs = longWorkingSignalDurationMs;
            Priority = priority;
            Threshold = threshold;
            TimeRadiationFF = timeRadiationFF;
            IsTest = isTest;

            FhssFftResolution = fhssFftResolution;
            TimeRadiationFHSS = timeRadiationFHSS;

            SearchSector = searchSector;
            SearchTime = searchTime;

            JsgAmPollInterval = jsgAmPollInterval;
            JsgAmPower = jsgAmPower;
        }



        public override void Update(AbstractCommonTable record)
        {
            var newRecord = (GlobalProperties)record;

            Gnss.Update(newRecord.Gnss);

            TimeJamming = newRecord.TimeJamming;

            NumberOfPhasesAveragings = newRecord.NumberOfPhasesAveragings;
            NumberOfBearingAveragings = newRecord.NumberOfBearingAveragings;
            CourseAngle = newRecord.CourseAngle;
            AdaptiveThresholdFhss = newRecord.AdaptiveThresholdFhss;

            NumberOfResInLetter = newRecord.NumberOfResInLetter;
            NumberOfChannelsInLetter = newRecord.NumberOfChannelsInLetter;
            LongWorkingSignalDurationMs = newRecord.LongWorkingSignalDurationMs;
            Priority = newRecord.Priority;
            Threshold = newRecord.Threshold;
            TimeRadiationFF = newRecord.TimeRadiationFF;
            IsTest = newRecord.IsTest;

            FhssFftResolution = newRecord.FhssFftResolution;
            TimeRadiationFHSS = newRecord.TimeRadiationFHSS;

            SearchSector = newRecord.SearchSector;
            SearchTime = newRecord.SearchTime;

            JsgAmPollInterval = newRecord.JsgAmPollInterval;
            JsgAmPower = newRecord.JsgAmPower;
        }


        public GlobalProperties Clone() => new GlobalProperties(Gnss, TimeJamming, 
            NumberOfPhasesAveragings, NumberOfBearingAveragings, CourseAngle, AdaptiveThresholdFhss,
            NumberOfResInLetter, NumberOfChannelsInLetter, LongWorkingSignalDurationMs, Priority, Threshold, TimeRadiationFF, IsTest,
            FhssFftResolution, TimeRadiationFHSS,
            SearchSector, SearchTime,
            JsgAmPollInterval, JsgAmPower,Id);


        public bool EqualTo(GlobalProperties data)
        {
            return Gnss == data.Gnss && TimeJamming == data.TimeJamming
                 && NumberOfPhasesAveragings == data.NumberOfPhasesAveragings && NumberOfBearingAveragings == data.NumberOfBearingAveragings && CourseAngle == data.CourseAngle && AdaptiveThresholdFhss == data.AdaptiveThresholdFhss
                && NumberOfResInLetter == data.NumberOfResInLetter && NumberOfChannelsInLetter == data.NumberOfChannelsInLetter && LongWorkingSignalDurationMs == data.LongWorkingSignalDurationMs 
                && Priority == data.Priority && Threshold == data.Threshold && TimeRadiationFF == data.TimeRadiationFF && IsTest == data.IsTest
                && FhssFftResolution == data.FhssFftResolution && TimeRadiationFHSS == data.TimeRadiationFHSS
                && SearchSector == data.SearchSector && SearchTime == data.SearchTime
                && JsgAmPollInterval == data.JsgAmPollInterval && JsgAmPower == data.JsgAmPower;
        }
        #endregion



        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion
    }
}
