﻿namespace KvetkaModelsDBLib
{
    using System;
    using System.Collections.Generic;

    public sealed class IDGenerator
    {
        private List<int> alreadyUsed = new List<int>(); 

        private Random rand = new Random();

        private IDGenerator(object arg1, object arg2) { }

        private static IDGenerator instance = null;
        private static readonly object padlock = new object();

        public static IDGenerator GetInstance(object arg1, object arg2)
        {
            if (instance == null)
            {
                lock (padlock) // now I can claim some form of thread safety...
                {
                    if (instance == null)
                    {
                        instance = new IDGenerator(arg1, arg2);
                    }
                }
            }

            return instance;
        }

        // Finally, any singleton should define some business logic, which can
        // be executed on its instance.
        public int GenerateUniqueId()
        {
            while (true)
            {
                var value = rand.Next(0, int.MaxValue);
                if (this.alreadyUsed.Contains(value))
                {
                    continue;
                }
                return value;
            }
        }
    }
}