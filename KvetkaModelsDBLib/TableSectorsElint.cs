﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace KvetkaModelsDBLib
{
    /// <summary>
    /// Сектора радиоразведки (СРР)
    /// </summary>
    [DataContract]
    [KnownType(typeof(AbstractDependentASP))]
    [InfoTable(NameTable.TableSectorsElint)]

    public class TableSectorsElint : AbstractDependentASP
    {
        [DataMember]
        [DisplayName(nameof(Id)), Browsable(false)]
        public override int Id { get; set; }

        [DataMember]
        [DisplayName(nameof(NumberASP)), ReadOnly(true), Browsable(true)]
        public override int NumberASP { get; set; }  // адрес станции

        [DataMember]
        [DisplayName(nameof(AngleMin))]
        public double AngleMin { get; set; }

        [DataMember]
        [DisplayName(nameof(AngleMax))]
        public double AngleMax { get; set; }

        [DataMember]
        [DisplayName(nameof(Note))]
        public String Note { get; set; } = String.Empty;

        [DataMember]
        [DisplayName(nameof(IsActive)), Browsable(false)]
        public bool IsActive { get; set; }


        public TableSectorsElint() { }
        public TableSectorsElint(int numberASP, double angleMin, double angleMax, string note, bool isActive, int id = 0)
        {
            NumberASP = numberASP;
            AngleMin = angleMin;
            AngleMax = angleMax;
            Note = note;
            IsActive = isActive;
            Id = id;
        }


        public TableSectorsElint Clone()
        {
            return new TableSectorsElint(NumberASP, AngleMin, AngleMax, Note, IsActive, Id);
        }

        public override object[] GetKey()
        {
            return new object[] { Id };
        }

        public override void Update(AbstractCommonTable record)
        {
            var newRec = (TableSectorsElint)record;

            AngleMin = newRec.AngleMin;
            AngleMax = newRec.AngleMax;
            Note = newRec.Note;
            IsActive = newRec.IsActive;
        }
    }
}
