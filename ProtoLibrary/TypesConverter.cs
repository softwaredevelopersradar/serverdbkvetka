﻿using System.Collections.Generic;
using KvetkaModelsDBLib;
using TransmissionPackage;
using Google.Protobuf.WellKnownTypes;
using System;
using System.Collections.ObjectModel;

namespace ProtoLibrary
{
    public static class TypesConverter
    {
        #region Enums

        public static KvetkaModelsDBLib.NameTable ConvertToDBModel(this TransmissionPackage.EnumTables args) =>
            (KvetkaModelsDBLib.NameTable)args;
        public static TransmissionPackage.EnumTables ConvertToProto(this KvetkaModelsDBLib.NameTable args) =>
            (TransmissionPackage.EnumTables)args;


        public static KvetkaModelsDBLib.NameChangeOperation ConvertToDBModel(this TransmissionPackage.ChangeRecordOperation args) =>
            (KvetkaModelsDBLib.NameChangeOperation)args;
        public static TransmissionPackage.ChangeRecordOperation ConvertToProto(this KvetkaModelsDBLib.NameChangeOperation args) =>
            (TransmissionPackage.ChangeRecordOperation)args;


        public static KvetkaModelsDBLib.Led ConvertToDBModel(this TransmissionPackage.Led args) =>
            (KvetkaModelsDBLib.Led)args;
        public static TransmissionPackage.Led ConvertToProto(this KvetkaModelsDBLib.Led args) =>
            (TransmissionPackage.Led)args;

        public static KvetkaModelsDBLib.CommunicationType ConvertToDBModel(this TransmissionPackage.EnumCommunicationType args) =>
           (KvetkaModelsDBLib.CommunicationType)args;
        public static TransmissionPackage.EnumCommunicationType ConvertToProto(this KvetkaModelsDBLib.CommunicationType args) =>
            (TransmissionPackage.EnumCommunicationType)args;

        public static KvetkaModelsDBLib.SignalFlag ConvertToDBModel(this TransmissionPackage.SignalFlag args) =>
           (KvetkaModelsDBLib.SignalFlag)args;
        public static TransmissionPackage.SignalFlag ConvertToProto(this KvetkaModelsDBLib.SignalFlag args) =>
            (TransmissionPackage.SignalFlag)args;

        public static KvetkaModelsDBLib.DigitalSignalType ConvertToDBModel(this TransmissionPackage.DigitalSignalType args) =>
           (KvetkaModelsDBLib.DigitalSignalType)args;
        public static TransmissionPackage.DigitalSignalType ConvertToProto(this KvetkaModelsDBLib.DigitalSignalType args) =>
            (TransmissionPackage.DigitalSignalType)args;

        public static KvetkaModelsDBLib.VereskState ConvertToDBModel(this TransmissionPackage.EnumVereskState args) =>
            (KvetkaModelsDBLib.VereskState)args;
        public static TransmissionPackage.EnumVereskState ConvertToProto(this KvetkaModelsDBLib.VereskState args) =>
            (TransmissionPackage.EnumVereskState)args;

        public static KvetkaModelsDBLib.AnalogSignalType ConvertToDBModel(this TransmissionPackage.AnalogSignalType args) =>
           (KvetkaModelsDBLib.AnalogSignalType)args;
        public static TransmissionPackage.AnalogSignalType ConvertToProto(this KvetkaModelsDBLib.AnalogSignalType args) =>
            (TransmissionPackage.AnalogSignalType)args;

        public static KvetkaModelsDBLib.FftResolution ConvertToDBModel(this TransmissionPackage.EnumFftResolution args) =>
          (KvetkaModelsDBLib.FftResolution)args;
        public static TransmissionPackage.EnumFftResolution ConvertToProto(this KvetkaModelsDBLib.FftResolution args) =>
            (TransmissionPackage.EnumFftResolution)args;

        public static KvetkaModelsDBLib.PowerPercent ConvertToDBModel(this TransmissionPackage.EnumPowerPercent args) =>
          (KvetkaModelsDBLib.PowerPercent)args;
        public static TransmissionPackage.EnumPowerPercent ConvertToProto(this KvetkaModelsDBLib.PowerPercent args) =>
            (TransmissionPackage.EnumPowerPercent)args;
        #endregion




        #region Models

        public static KvetkaModelsDBLib.TableJammer ConvertToDBModel(this TransmissionPackage.JammerMessage args)
        {
            return new TableJammer(ConvertToDBModel(args.Coordinates), args.Callsign, args.Note, (StationRole)args.Role, args.IsOwn, args.AntennaAngle, ConvertToDBModel(args.CommunicationType), args.Id);
        }
        public static TransmissionPackage.JammerMessage ConvertToProto(this KvetkaModelsDBLib.TableJammer args)
        {
            return new JammerMessage() { Coordinates = ConvertToProto(args.Coordinates), Callsign = args.CallSign, Note = args.Note, Role = (EnumRole)args.Role, IsOwn = args.IsOwn, AntennaAngle = args.AntennaAngle, CommunicationType = ConvertToProto(args.CommunicationType), Id = args.Id };
        }


        public static KvetkaModelsDBLib.Coord ConvertToDBModel(this TransmissionPackage.CoordMessage args)
        {
            return new Coord(args.Latitude, args.Longitude, args.Altitude);
        }
        public static TransmissionPackage.CoordMessage ConvertToProto(this KvetkaModelsDBLib.Coord args)
        {
            return new CoordMessage() { Latitude = args.Latitude, Longitude = args.Longitude, Altitude = args.Altitude };
        }

        public static KvetkaModelsDBLib.InterferenceParameters ConvertToDBModel(this TransmissionPackage.InterferenceParamsMessage args)
        {
            return new KvetkaModelsDBLib.InterferenceParameters((byte)args.Modulation, (byte)args.Deviation, (byte)args.Manipulation, (byte)args.Duration);
        }
        public static TransmissionPackage.InterferenceParamsMessage ConvertToProto(this KvetkaModelsDBLib.InterferenceParameters args)
        {
            return new TransmissionPackage.InterferenceParamsMessage() { Modulation = args.Modulation, Deviation = args.Deviation, Manipulation = args.Manipulation, Duration = args.Duration};
        }


        public static KvetkaModelsDBLib.FreqRanges ConvertToDBModel(this TransmissionPackage.FreqRangeMessage args)
        {
            return new KvetkaModelsDBLib.FreqRanges(args.JammerId, args.Fmin , args.Fmax, args.Note, args.IsActive, args.Id);
        }
        public static TransmissionPackage.FreqRangeMessage ConvertToProto(this KvetkaModelsDBLib.FreqRanges args)
        {
            return new TransmissionPackage.FreqRangeMessage() {  JammerId = args.NumberASP, Fmin = args.FreqMinKHz, Fmax = args.FreqMaxKHz, Note = args.Note, IsActive = args.IsActive, Id = args.Id };
        }

        public static KvetkaModelsDBLib.TableSectorsElint ConvertToDBModel(this TransmissionPackage.SectorMessage args)
        {
            return new KvetkaModelsDBLib.TableSectorsElint(args.JammerId, args.Amin, args.Amax, args.Note, args.IsActive, args.Id);
        }
        public static TransmissionPackage.SectorMessage ConvertToProto(this KvetkaModelsDBLib.TableSectorsElint args)
        {
            return new TransmissionPackage.SectorMessage() { JammerId = args.NumberASP, Amin = args.AngleMin, Amax = args.AngleMax, Note = args.Note, IsActive = args.IsActive, Id = args.Id };
        }


        public static KvetkaModelsDBLib.TableADSB ConvertToDBModel(this TransmissionPackage.AirplaneMessage args)
        {
            return new KvetkaModelsDBLib.TableADSB(args.ICAO, ConvertToDBModel(args.Coordinates), ConvertTime(args.Time), (byte)args.Type, args.Country, args.Id); //TODO: проверить преобразование в byte
        }
        public static TransmissionPackage.AirplaneMessage ConvertToProto(this KvetkaModelsDBLib.TableADSB args)
        {
            return new TransmissionPackage.AirplaneMessage() { ICAO = args.ICAO, Coordinates = ConvertToProto(args.Coordinates), Time = ConvertTime(args.Time), Type = args.Type, Country = args.Country, Id = args.Id };
        }


        public static KvetkaModelsDBLib.TableTrackResFF ConvertToDBModel(this TransmissionPackage.TrackResFFMessage args)
        {
            return new KvetkaModelsDBLib.TableTrackResFF(args.ResFFId, ConvertToDBModel(args.Coordinates), args.BearingOwn, args.BearingLinked, args.DistanceOwn, args.DistanceLinked, args.Id);
        }
        public static TransmissionPackage.TrackResFFMessage ConvertToProto(this KvetkaModelsDBLib.TableTrackResFF args)
        {
            return new TransmissionPackage.TrackResFFMessage() { ResFFId = args.TableResFFId, Coordinates = ConvertToProto(args.Coordinates), BearingOwn = args.BearingOwn, BearingLinked = args.BearingLinked, DistanceOwn = args.DistanceOwn, DistanceLinked = args.DistanceLinked, Id = args.Id };
        }

        //public static KvetkaModelsDBLib.TableResFF ConvertToDBModel(this TransmissionPackage.ResFFMessage args)
        //{
        //    var trackList = new ObservableCollection<TableTrackResFF>();
        //    foreach (var t in args.Track)
        //    {
        //        trackList.Add(t.ConvertToDBModel());
        //    }
        //    return new KvetkaModelsDBLib.TableResFF(args.FrequencyKhz, args.BandKhz, (byte)args.Modulation, (byte)args.VManipulation, (byte)args.Level, args.Time.ToDateTime(), args.Note, trackList, args.Id);
        //}
        //public static TransmissionPackage.ResFFMessage ConvertToProto(this KvetkaModelsDBLib.TableResFF args)
        //{
        //    var result = new TransmissionPackage.ResFFMessage() { FrequencyKhz = args.FrequencyKHz, BandKhz = args.BandKHz, Modulation = args.Modulation, VManipulation = args.ManipulationVelocity, Level = args.Level, Time = Timestamp.FromDateTime(args.Time.ToUniversalTime()), Note = args.Note, Id = args.Id };
        //    foreach (var t in args.ListTrack)
        //    {
        //        result.Track.Add(t.ConvertToProto());
        //    }
        //    return result;
        //}

        private static DateTime ConvertTime(Google.Protobuf.WellKnownTypes.Timestamp arg)
        {
            return arg.Seconds <= -62135596800 ? default(DateTime) : arg.ToDateTime();
        }

        private static Google.Protobuf.WellKnownTypes.Timestamp ConvertTime(DateTime arg)
        {
            return DateTime.SpecifyKind(arg, DateTimeKind.Utc).ToTimestamp();
        }

        public static KvetkaModelsDBLib.TableDigitalResFF ConvertToDBModel(this TransmissionPackage.DigitalResFFMessage args)
        {
            var tableResFF = args.ResFF != null ? ConvertToDBModel(args.ResFF) : null;
            return new KvetkaModelsDBLib.TableDigitalResFF(ConvertTime(args.TimeBegin), 
                ConvertTime(args.TimeEnd), args.FrequencyKhz, ConvertToDBModel(args.SignalClass), args.BearingOwn, args.SubscriberInitiator, args.SubscriberRecipient, args.RadioNetwork, ConvertToDBModel(args.Coordinates), args.IsActive, args.HasMetadata, tableResFF, ConvertToDBModel(args.VereskState) , (uint)args.VereskDbSessionId, args.Id);
        }
        public static TransmissionPackage.DigitalResFFMessage ConvertToProto(this KvetkaModelsDBLib.TableDigitalResFF args)
        {
            var tableResFF = args.ResFF != null ? ConvertToProto(args.ResFF) : null;
            return new TransmissionPackage.DigitalResFFMessage() { FrequencyKhz = args.FrequencyKHz, SignalClass = ConvertToProto(args.SignalClass), Coordinates = ConvertToProto(args.Coordinates), BearingOwn = args.BearingOwn, TimeBegin = ConvertTime(args.TimeBegin), TimeEnd = ConvertTime(args.TimeEnd), SubscriberInitiator = args.SubscriberInitiator, SubscriberRecipient = args.SubscriberRecipient, RadioNetwork = args.RadioNetwork, IsActive = args.IsActive, HasMetadata = args.HasMetadata, ResFF = tableResFF, VereskState = ConvertToProto(args.VereskState), VereskDbSessionId = args.VereskDbSessionId, Id = args.Id };
        }

        public static KvetkaModelsDBLib.TableAnalogResFF ConvertToDBModel(this TransmissionPackage.AnalogResFFMessage args)
        {
            var tableResFF = args.ResFF != null ? ConvertToDBModel(args.ResFF) : null;
            return new KvetkaModelsDBLib.TableAnalogResFF(args.FrequencyKhz, args.BandKhz, args.BearingOwn, (short)args.Level, args.SignalType, args.BandDemudulation, ConvertToDBModel(args.Coordinates), ConvertTime(args.Time), args.IsActive, tableResFF, args.BandAudio, args.Id);
        }
        public static TransmissionPackage.AnalogResFFMessage ConvertToProto(this KvetkaModelsDBLib.TableAnalogResFF args)
        {
            var tableResFF = args.ResFF != null ? ConvertToProto(args.ResFF) : null;
            return new TransmissionPackage.AnalogResFFMessage() { FrequencyKhz = args.FrequencyKHz, BandKhz = args.BandKHz, SignalType = args.TypeSignal, BandDemudulation = args.BandDemodulation, Coordinates = ConvertToProto(args.Coordinates), BearingOwn = args.BearingOwn,  Level = args.Level, Time = ConvertTime(args.Time), IsActive = args.IsActive, ResFF = tableResFF, BandAudio = args.BandAudio, Id = args.Id };
        }

        public static KvetkaModelsDBLib.TableResFF ConvertToDBModel(this TransmissionPackage.ResFFMessage args)
        {
            try
            {
                var rec = new KvetkaModelsDBLib.TableResFF(ConvertToDBModel(args.Control),
                    ConvertToDBModel(args.SignalFlag), args.FrequencyKhz, args.BearingOwn, args.BearingLinked,
                    args.DistanceOwn, ConvertToDBModel(args.Coordinates), args.Level, ConvertTime(args.Time),
                    args.IsActive, args.AnalogResFFId, args.DigitalResFFId, (short)args.Quality, args.Id);
                
                return rec;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public static TransmissionPackage.ResFFMessage ConvertToProto(this KvetkaModelsDBLib.TableResFF args)
        {
            return new TransmissionPackage.ResFFMessage() { FrequencyKhz = args.FrequencyKHz, Coordinates = ConvertToProto(args.Coordinates), BearingOwn = args.BearingOwn, BearingLinked = args.BearingLinked, DistanceOwn = args.DistanceOwn, Level = args.Level, Time = ConvertTime(args.Time), IsActive = args.IsActive, Control = ConvertToProto(args.Control), SignalFlag = ConvertToProto(args.SignalFlag), AnalogResFFId = args.TableAnalogResFFId, DigitalResFFId = args.TableDigitalResFFId, Quality = args.Quality,  Id = args.Id };
        }

        public static KvetkaModelsDBLib.TableResFFJam ConvertToDBModel(this TransmissionPackage.ResFFJamMessage args)
        {/*AnalogResFFId = args.TableAnalogResFFId, */
            return new KvetkaModelsDBLib.TableResFFJam(args.JammerId, args.IsActive, (byte)args.Sign, args.FrequencyKhz, args.Bearing, ConvertToDBModel(args.InterferenceParams), (short)args.Threshold, (byte)args.Letter, (byte)args.Priority, ConvertToDBModel(args.Coordinates), args.Id);
        }
        public static TransmissionPackage.ResFFJamMessage ConvertToProto(this KvetkaModelsDBLib.TableResFFJam args)
        {
            return new TransmissionPackage.ResFFJamMessage(){ JammerId = args.NumberASP, IsActive = args.IsActive, Sign = args.Sign, FrequencyKhz = args.FrequencyKHz, Bearing = args.Bearing, InterferenceParams = ConvertToProto(args.InterferenceParameters), Threshold = args.Threshold, Letter = args.Letter, Priority = args.Priority, Coordinates = ConvertToProto(args.Coordinates), Id = args.Id };
        }



        public static KvetkaModelsDBLib.TableTrackSubscriberResFHSS ConvertToDBModel(this TransmissionPackage.TrackAbonentResFHSSMessage args)
        {
            return new KvetkaModelsDBLib.TableTrackSubscriberResFHSS(args.AbonentResFHSSId, args.ResFHSSId, ConvertToDBModel(args.Coordinates), args.BearingOwn, args.BearingLinked, args.DistanceOwn, args.DistanceLinked, args.Id);
        }
        public static TransmissionPackage.TrackAbonentResFHSSMessage ConvertToProto(this KvetkaModelsDBLib.TableTrackSubscriberResFHSS args)
        {
            return new TransmissionPackage.TrackAbonentResFHSSMessage() { AbonentResFHSSId = args.TableSubscriberResFHSSId, ResFHSSId = args.TableResFHSSId, Coordinates = ConvertToProto(args.Coordinates), BearingOwn = args.BearingOwn, BearingLinked = args.BearingLinked, DistanceOwn = args.DistanceOwn, DistanceLinked = args.DistanceLinked, Id = args.Id };
        }

        public static KvetkaModelsDBLib.TableSubscriberResFHSS ConvertToDBModel(this TransmissionPackage.AbonentResFHSSMessage args)
        {
            return new KvetkaModelsDBLib.TableSubscriberResFHSS(args.ResFHSSId, ConvertTime(args.Time), args.Position.ConvertToDBModel(), args.Id);
        }
        public static TransmissionPackage.AbonentResFHSSMessage ConvertToProto(this KvetkaModelsDBLib.TableSubscriberResFHSS args)
        {
            return new TransmissionPackage.AbonentResFHSSMessage(){ ResFHSSId = args.TableResFHSSId, Time = ConvertTime(args.Time), Position = args.Position.ConvertToProto(), Id = args.Id };
        }

        public static KvetkaModelsDBLib.TableResFHSS ConvertToDBModel(this TransmissionPackage.ResFHSSMessage args)
        {
            var subscribersList = new ObservableCollection<TableSubscriberResFHSS>();
            foreach (var t in args.ListSubscribers)
            {
                subscribersList.Add(t.ConvertToDBModel());
            }
            return new KvetkaModelsDBLib.TableResFHSS(args.FrequencyKhzMin, args.FrequencyKhzMax, args.BandKhz, (byte)args.Modulation, (byte)args.VManipulation, (short)args.StepKHz, (short)args.ImpulseDuration, subscribersList, args.IdBearingDb, args.Id);
        }
        public static TransmissionPackage.ResFHSSMessage ConvertToProto(this KvetkaModelsDBLib.TableResFHSS args)
        {
            var result = new TransmissionPackage.ResFHSSMessage() { FrequencyKhzMin = args.FrequencyMinKHz, FrequencyKhzMax = args.FrequencyMaxKHz, BandKhz = args.BandKHz, Modulation = args.Modulation, VManipulation = args.ManipulationVelocity, StepKHz = args.StepKHz, ImpulseDuration = args.ImpulseDuration, IdBearingDb = args.IdBearingDb, Id = args.Id };
            foreach (var t in args.ListSubscribers)
            {
                result.ListSubscribers.Add(t.ConvertToProto());
            }
            return result;
        }

        public static KvetkaModelsDBLib.TableResFHSSJam ConvertToDBModel(this TransmissionPackage.ResFHSSJamMessage args) 
        {
            return new KvetkaModelsDBLib.TableResFHSSJam(args.JammerId, args.IsActive, (byte)args.Sign, args.FrequencyKhzMin, args.FrequencyKhzMax, ConvertToDBModel(args.InterferenceParams), (short)args.Threshold, args.Letters.ToByteArray(), (byte)args.Priority, args.Id); 
        }
        public static TransmissionPackage.ResFHSSJamMessage ConvertToProto(this KvetkaModelsDBLib.TableResFHSSJam args)
        {
            return new TransmissionPackage.ResFHSSJamMessage() { JammerId = args.NumberASP, IsActive = args.IsActive, Sign = args.Sign, FrequencyKhzMin = args.FrequencyMinKHz, FrequencyKhzMax = args.FrequencyMaxKHz, InterferenceParams = ConvertToProto(args.InterferenceParameters), Threshold = args.Threshold, Priority = args.Priority, Letters = Google.Protobuf.ByteString.CopyFrom(args.Letters), Id = args.Id };
        }

        public static KvetkaModelsDBLib.GlobalProperties ConvertToDBModel(this TransmissionPackage.GlobalSettingsMessage args)
        {
            return new GlobalProperties(ConvertToDBModel(args.Gnss), args.TimeJamming,
                args.NumberOfPhasesAveragings, args.NumberOfBearingAveragings, args.CourseAngle, args.AdaptiveThresholdFhss,
                args.NumberOfResInLetter, args.NumberOfChannelsInLetter, args.LongWorkingSignalDurationMs, (byte)args.Priority, args.Threshold, args.TimeRadiationFF, args.IsTest,
                ConvertToDBModel(args.FhssFftResolution), args.TimeRadiationFHSS,
                args.SearchSector, args.SearchTime, args.JsgAmPollInterval, ConvertToDBModel(args.JsgAmPower), args.Id);
        }
        public static TransmissionPackage.GlobalSettingsMessage ConvertToProto(this KvetkaModelsDBLib.GlobalProperties args)
        {
            return new GlobalSettingsMessage() { Gnss = ConvertToProto(args.Gnss),  TimeJamming = args.TimeJamming,
                NumberOfPhasesAveragings = args.NumberOfPhasesAveragings, NumberOfBearingAveragings = args.NumberOfBearingAveragings, CourseAngle = args.CourseAngle, AdaptiveThresholdFhss = args.AdaptiveThresholdFhss,
                NumberOfResInLetter = args.NumberOfResInLetter, NumberOfChannelsInLetter = args.NumberOfChannelsInLetter, LongWorkingSignalDurationMs = args.LongWorkingSignalDurationMs,
                Priority = args.Priority, Threshold = args.Threshold, TimeRadiationFF = args.TimeRadiationFF, IsTest = args.IsTest,
                FhssFftResolution = ConvertToProto(args.FhssFftResolution), TimeRadiationFHSS = args.TimeRadiationFHSS,
                SearchSector = args.SearchSector, SearchTime = args.SearchTime,
                JsgAmPollInterval = args.JsgAmPollInterval, JsgAmPower = ConvertToProto(args.JsgAmPower),
                Id = args.Id };
        }

        public static KvetkaModelsDBLib.TempSuppress ConvertToDBModel(this TransmissionPackage.TempSuppressMessage args)
        {
            return new KvetkaModelsDBLib.TempSuppress(args.JammerId, ConvertToDBModel(args.Control), ConvertToDBModel(args.Suppress), ConvertToDBModel(args.Radiation), args.Id);
        }
        public static TransmissionPackage.TempSuppressMessage ConvertToProto(this KvetkaModelsDBLib.TempSuppress args)
        {
            return new TransmissionPackage.TempSuppressMessage() { JammerId = args.NumberASP, Control = ConvertToProto(args.Control), Suppress = ConvertToProto(args.Suppress), Radiation = ConvertToProto(args.Radiation), Id = args.Id };
        }


        public static KvetkaModelsDBLib.TableResFFDistribution ConvertToDBModel(this TransmissionPackage.ResFFDistributMessage args)
        {
            return new KvetkaModelsDBLib.TableResFFDistribution(args.FrequencyKhz, args.BandKhz, args.BearingOwn, args.BearingLinked, args.DistanceOwn, ConvertToDBModel(args.Coordinates), args.Level, ConvertTime(args.Time), args.IsActive, args.IsRecording, args.Demodulation, args.BandwidthKHz, args.SampleRate, (ModeSound)args.Mode, args.AgcLevel, args.MgcCoefficient, args.Id);
        }
        public static TransmissionPackage.ResFFDistributMessage ConvertToProto(this KvetkaModelsDBLib.TableResFFDistribution args)
        {
            return new TransmissionPackage.ResFFDistributMessage() { FrequencyKhz = args.FrequencyKHz, Coordinates = ConvertToProto(args.Coordinates), BandKhz = args.BandKHz, BearingOwn = args.BearingOwn, BearingLinked = args.BearingLinked, DistanceOwn = args.DistanceOwn, Level = args.Level, Time = ConvertTime(args.Time), IsActive = args.IsActive, IsRecording = args.IsRecording,
                Demodulation = args.Demodulation, BandwidthKHz = args.BandwidthKHz, SampleRate = args.SampleRate, Mode = (ResFFDistributMessage.Types.EnumMode)args.Mode, AgcLevel = args.AgcLevel, MgcCoefficient = args.MgcCoefficient,Id = args.Id };
        }


        public static KvetkaModelsDBLib.TableRoutePoint ConvertToDBModel(this TransmissionPackage.RoutePointMessage args)
        {
            return new KvetkaModelsDBLib.TableRoutePoint(args.RouteId, (short)args.NumPoint, args.Coordinates.ConvertToDBModel(), args.SegmentLength, args.Id);
        }
        public static TransmissionPackage.RoutePointMessage ConvertToProto(this KvetkaModelsDBLib.TableRoutePoint args)
        {
            return new TransmissionPackage.RoutePointMessage(){ RouteId = args.RouteId, NumPoint = args.NumberPoint, Coordinates = args.Coordinates.ConvertToProto(), SegmentLength = args.SegmentLength, Id = args.Id };
        }

        public static KvetkaModelsDBLib.TableRoute ConvertToDBModel(this TransmissionPackage.RouteMessage args)
        {
            var pointList = new ObservableCollection<TableRoutePoint>();
            foreach (var t in args.ListPoints)
            {
                pointList.Add(t.ConvertToDBModel());
            }
            return new KvetkaModelsDBLib.TableRoute((short)args.NumRoute, args.Name, args.DistanceOwn, ConvertTime(args.Time), pointList, args.Id);
        }
        public static TransmissionPackage.RouteMessage ConvertToProto(this KvetkaModelsDBLib.TableRoute args)
        {
            var result = new TransmissionPackage.RouteMessage() { NumRoute = args.NumberRoute, Name = args.Name, DistanceOwn = args.DistanceOwn, Time = ConvertTime(args.Time), Id = args.Id };
            foreach (var t in args.ListPoints)
            {
                result.ListPoints.Add(t.ConvertToProto());
            }
            return result;
        }

        public static KvetkaModelsDBLib.TableFHSSExcluded ConvertToDBModel(this TransmissionPackage.DependentFHSSMessage args)
        {
            return new KvetkaModelsDBLib.TableFHSSExcluded(args.FhssId, args.FrequencyKhz, args.Deviation, args.Id);
        }
        public static TransmissionPackage.DependentFHSSMessage ConvertToProto(this KvetkaModelsDBLib.TableFHSSExcluded args)
        {
            return new TransmissionPackage.DependentFHSSMessage() { FhssId = args.FhssId, FrequencyKhz = args.FrequencyKHz, Deviation = args.Deviation, Id = args.Id };
        }
        #endregion




        #region General

        public static KvetkaModelsDBLib.AbstractCommonTable ConvertToDBModel(this Google.Protobuf.WellKnownTypes.Any args, NameTable nameTable)
        {
            switch (nameTable)
            {
                case NameTable.TableJammer:
                    return ConvertToDBModel(args.Unpack<TransmissionPackage.JammerMessage>());
                case NameTable.TableFreqForbidden:
                    return ConvertToDBModel(args.Unpack<TransmissionPackage.FreqRangeMessage>()).ToFreqForbidden();
                case NameTable.TableFreqImportant:
                    return ConvertToDBModel(args.Unpack<TransmissionPackage.FreqRangeMessage>()).ToFreqImportant();
                case NameTable.TableFreqKnown:
                    return ConvertToDBModel(args.Unpack<TransmissionPackage.FreqRangeMessage>()).ToFreqKnown();
                case NameTable.TableFreqRangesElint:
                    return ConvertToDBModel(args.Unpack<TransmissionPackage.FreqRangeMessage>()).ToFreqRangesElint();
                case NameTable.TableFreqRangesJamming:
                    return ConvertToDBModel(args.Unpack<TransmissionPackage.FreqRangeMessage>()).ToFreqRangesJamming();
                case NameTable.TableSectorsElint:
                    return ConvertToDBModel(args.Unpack<TransmissionPackage.SectorMessage>());
                case NameTable.TableADSB:
                    return ConvertToDBModel(args.Unpack<TransmissionPackage.AirplaneMessage>());
                case NameTable.TableResFF:
                    return ConvertToDBModel(args.Unpack<TransmissionPackage.ResFFMessage>());
                case NameTable.TableTrackResFF:
                    return ConvertToDBModel(args.Unpack<TransmissionPackage.TrackResFFMessage>());
                case NameTable.TableResFFJam:
                    return ConvertToDBModel(args.Unpack<TransmissionPackage.ResFFJamMessage>());
                case NameTable.TableResFHSS:
                    return ConvertToDBModel(args.Unpack<TransmissionPackage.ResFHSSMessage>());
                case NameTable.TableSubscriberResFHSS:
                    return ConvertToDBModel(args.Unpack<TransmissionPackage.AbonentResFHSSMessage>());
                case NameTable.TableTrackSubscriberResFHSS:
                    return ConvertToDBModel(args.Unpack<TransmissionPackage.TrackAbonentResFHSSMessage>());
                case NameTable.TableResFHSSJam:
                    return ConvertToDBModel(args.Unpack<TransmissionPackage.ResFHSSJamMessage>());
                case NameTable.GlobalProperties:
                    return ConvertToDBModel(args.Unpack<TransmissionPackage.GlobalSettingsMessage>());
                case NameTable.TempSuppressFF:
                    return ConvertToDBModel(args.Unpack<TransmissionPackage.TempSuppressMessage>()).ToTempSuppressFF();
                case NameTable.TempSuppressFHSS:
                    return ConvertToDBModel(args.Unpack<TransmissionPackage.TempSuppressMessage>()).ToTempSuppressFHSS();
                case NameTable.TableDigitalResFF:
                    return ConvertToDBModel(args.Unpack<TransmissionPackage.DigitalResFFMessage>());
                case NameTable.TableAnalogResFF:
                    return ConvertToDBModel(args.Unpack<TransmissionPackage.AnalogResFFMessage>());
                case NameTable.TableResFFDistribution:
                    return ConvertToDBModel(args.Unpack<TransmissionPackage.ResFFDistributMessage>());
                case NameTable.TableRoute:
                    return ConvertToDBModel(args.Unpack<TransmissionPackage.RouteMessage>());
                case NameTable.TableRoutePoint:
                    return ConvertToDBModel(args.Unpack<TransmissionPackage.RoutePointMessage>());
                case NameTable.TableFHSSExcludedJam:
                    return ConvertToDBModel(args.Unpack<TransmissionPackage.DependentFHSSMessage>()).ToFHSSExcludedJam();
                default:
                    return null;

            }
        }
        public static Google.Protobuf.WellKnownTypes.Any ConvertToProto(this KvetkaModelsDBLib.AbstractCommonTable args, NameTable nameTable)
        {
            switch (nameTable)
            {
                case NameTable.TableJammer:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as TableJammer));
                case NameTable.TableFreqForbidden:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as TableFreqForbidden));
                case NameTable.TableFreqImportant:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as TableFreqImportant));
                case NameTable.TableFreqKnown:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as TableFreqKnown));
                case NameTable.TableFreqRangesElint:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as TableFreqRangesElint));
                case NameTable.TableFreqRangesJamming:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as TableFreqRangesJamming));
                case NameTable.TableSectorsElint:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as TableSectorsElint));
                case NameTable.TableADSB:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as TableADSB));
                case NameTable.TableResFF:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as TableResFF));
                case NameTable.TableTrackResFF:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as TableTrackResFF));
                case NameTable.TableResFFJam:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as TableResFFJam));
                case NameTable.TableResFHSS:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as TableResFHSS));
                case NameTable.TableSubscriberResFHSS:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as TableSubscriberResFHSS));
                case NameTable.TableTrackSubscriberResFHSS:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as TableTrackSubscriberResFHSS));
                case NameTable.TableResFHSSJam:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as TableResFHSSJam));
                case NameTable.GlobalProperties:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as GlobalProperties));
                case NameTable.TempSuppressFF:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as TempSuppressFF));
                case NameTable.TempSuppressFHSS:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as TempSuppressFHSS));
                case NameTable.TableDigitalResFF:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as TableDigitalResFF));
                case NameTable.TableAnalogResFF:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as TableAnalogResFF));
                case NameTable.TableResFFDistribution:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as TableResFFDistribution));
                case NameTable.TableRoute:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as TableRoute));
                case NameTable.TableRoutePoint:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as TableRoutePoint));
                case NameTable.TableFHSSExcludedJam:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as TableFHSSExcludedJam));
                default:
                    return null;
            }
        }



        public static KvetkaModelsDBLib.ClassDataCommon ConvertToDBModel(this Google.Protobuf.Collections.RepeatedField<Google.Protobuf.WellKnownTypes.Any> args, NameTable nameTable)
        {
            var result = new List<AbstractCommonTable>();

            foreach (var t in args)
            {
                result.Add(t.ConvertToDBModel(nameTable));
            }
            return ClassDataCommon.ConvertToListAbstractCommonTable(result);
        }

        public static Google.Protobuf.Collections.RepeatedField<Google.Protobuf.WellKnownTypes.Any> ConvertToProto(this KvetkaModelsDBLib.ClassDataCommon args, NameTable nameTable)
        {
            var result = new Google.Protobuf.Collections.RepeatedField<Google.Protobuf.WellKnownTypes.Any>();

            foreach (var t in args.ListRecords)
            {
                result.Add(t.ConvertToProto(nameTable));
            }
            return result;
        }

        #endregion
    }
}
